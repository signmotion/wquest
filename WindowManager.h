#pragma once

#include "WorldWindow.h"
#include "QDWindow.h"
#include <QTranslator>


namespace wquest {


class WindowManager {
protected:
    WindowManager();

    ~WindowManager();


public:
    static WindowManager &instance();

    WorldWindow::ptr_t world( uid_t );

    QDWindow::ptr_t qd( uid_t, QWidget *parent = nullptr );

    void closeAllExcludeWorld();

    /// Set a language by the 2-symbol code.
    void setLanguageUI(const QString &needLocale);

    /// Loads and set a language by the next which we have.
    void nextLanguageUI();


private:
    WorldWindow::ptr_t worldWindow_;
    QDWindow::ptr_t qdWindow_;

    QString currentLanguageFile_;
    QTranslator *translator_;
};


} // wquest
