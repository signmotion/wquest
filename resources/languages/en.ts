<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>ApronData</name>
    <message>
        <location filename="../../models/ApronData.h" line="28"/>
        <source>Apron</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChapterData</name>
    <message>
        <location filename="../../models/ChapterData.h" line="29"/>
        <source>Chapter</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImageLoaderModel</name>
    <message>
        <location filename="../../external/nodeeditor/examples/images/ImageLoaderModel.cpp" line="63"/>
        <source>Open Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../external/nodeeditor/examples/images/ImageLoaderModel.cpp" line="65"/>
        <source>Image Files (*.png *.jpg *.bmp)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../MainWindow.ui" line="23"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Model</name>
    <message>
        <location filename="../../models/Model.h" line="143"/>
        <source>Good </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProvisoData</name>
    <message>
        <location filename="../../models/ProvisoData.h" line="27"/>
        <source>Proviso</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProvisoWhenDistance2Model</name>
    <message>
        <location filename="../../models/ProvisoWhenDistance2Model.cpp" line="34"/>
        <source>When distance between [instance A] and [instance B] is [compare] than [distance] m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../models/ProvisoWhenDistance2Model.cpp" line="99"/>
        <source>Instance A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../models/ProvisoWhenDistance2Model.cpp" line="101"/>
        <source>Instance B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../models/ProvisoWhenDistance2Model.cpp" line="103"/>
        <source>Compare</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../models/ProvisoWhenDistance2Model.cpp" line="105"/>
        <source>Distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../models/ProvisoWhenDistance2Model.cpp" line="286"/>
        <source>Good!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../models/ProvisoWhenDistance2Model.cpp" line="294"/>
        <source> Need something...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../models/ProvisoWhenDistance2Model.cpp" line="300"/>
        <source> Need something more...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProvisosData</name>
    <message>
        <location filename="../../models/ProvisosData.h" line="28"/>
        <source>Provisos</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QtNodes::FlowScene</name>
    <message>
        <location filename="../../external/nodeeditor/src/FlowScene.cpp" line="447"/>
        <location filename="../../external/nodeeditor/src/FlowScene.cpp" line="475"/>
        <source>Open Flow Scene</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../external/nodeeditor/src/FlowScene.cpp" line="449"/>
        <location filename="../../external/nodeeditor/src/FlowScene.cpp" line="477"/>
        <source>Flow Scene Files (*.flow)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RiverListData</name>
    <message>
        <location filename="../../external/nodeeditor/examples/dynamic_node/RiverListData.h" line="22"/>
        <source>RiverList</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RiverListModel</name>
    <message>
        <location filename="../../external/nodeeditor/examples/dynamic_node/RiverListModel.h" line="29"/>
        <source>RiverList</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RiverModel</name>
    <message>
        <location filename="../../external/nodeeditor/examples/dynamic_node/RiverModel.h" line="30"/>
        <source>River</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SceneData</name>
    <message>
        <location filename="../../models/SceneData.h" line="27"/>
        <source>Scene</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScenesData</name>
    <message>
        <location filename="../../models/ScenesData.h" line="28"/>
        <source>Scenes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TowerData</name>
    <message>
        <location filename="../../models/TowerData.h" line="28"/>
        <source>Tower</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>wquest::MainWindow</name>
    <message>
        <location filename="../../MainWindow.cpp" line="12"/>
        <source>Add quest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MainWindow.h" line="28"/>
        <source>WQuest</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>wquest::QDWindow</name>
    <message>
        <location filename="../../QDWindow.cpp" line="161"/>
        <location filename="../../QDWindow.cpp" line="162"/>
        <location filename="../../QDWindow.cpp" line="163"/>
        <location filename="../../QDWindow.cpp" line="164"/>
        <location filename="../../QDWindow.cpp" line="165"/>
        <source>Base elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QDWindow.cpp" line="168"/>
        <source>Provisos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QDWindow.h" line="31"/>
        <source>Quest</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
