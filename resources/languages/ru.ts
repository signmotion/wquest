<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru" sourcelanguage="en">
<context>
    <name>ApronData</name>
    <message>
        <location filename="../../models/ApronData.h" line="28"/>
        <source>Apron</source>
        <translation>Авансцена</translation>
    </message>
</context>
<context>
    <name>ChapterData</name>
    <message>
        <location filename="../../models/ChapterData.h" line="29"/>
        <source>Chapter</source>
        <translation>Глава</translation>
    </message>
</context>
<context>
    <name>ImageLoaderModel</name>
    <message>
        <location filename="../../external/nodeeditor/examples/images/ImageLoaderModel.cpp" line="63"/>
        <source>Open Image</source>
        <translation>Открыть изображение</translation>
    </message>
    <message>
        <location filename="../../external/nodeeditor/examples/images/ImageLoaderModel.cpp" line="65"/>
        <source>Image Files (*.png *.jpg *.bmp)</source>
        <translation>Файлы изображений</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../MainWindow.ui" line="23"/>
        <source>MainWindow</source>
        <translation>Главное окно</translation>
    </message>
</context>
<context>
    <name>Model</name>
    <message>
        <location filename="../../models/Model.h" line="143"/>
        <source>Good </source>
        <translation>Хорошо</translation>
    </message>
</context>
<context>
    <name>ProvisoData</name>
    <message>
        <location filename="../../models/ProvisoData.h" line="27"/>
        <source>Proviso</source>
        <translation>Условие</translation>
    </message>
</context>
<context>
    <name>ProvisoWhenDistance2Model</name>
    <message>
        <location filename="../../models/ProvisoWhenDistance2Model.cpp" line="34"/>
        <source>When distance between [instance A] and [instance B] is [compare] than [distance] m</source>
        <translation>Когда расстояние между [instance A] и [instance B] [compare] чем [distance] м</translation>
    </message>
    <message>
        <location filename="../../models/ProvisoWhenDistance2Model.cpp" line="99"/>
        <source>Instance A</source>
        <translation>А</translation>
    </message>
    <message>
        <location filename="../../models/ProvisoWhenDistance2Model.cpp" line="101"/>
        <source>Instance B</source>
        <translation>Б</translation>
    </message>
    <message>
        <location filename="../../models/ProvisoWhenDistance2Model.cpp" line="103"/>
        <source>Compare</source>
        <translation>Сравнение</translation>
    </message>
    <message>
        <location filename="../../models/ProvisoWhenDistance2Model.cpp" line="105"/>
        <source>Distance</source>
        <translation>Расстояние</translation>
    </message>
    <message>
        <location filename="../../models/ProvisoWhenDistance2Model.cpp" line="286"/>
        <source>Good!</source>
        <translation>Хорошо!</translation>
    </message>
    <message>
        <location filename="../../models/ProvisoWhenDistance2Model.cpp" line="294"/>
        <source> Need something...</source>
        <translation>Нужно что-то</translation>
    </message>
    <message>
        <location filename="../../models/ProvisoWhenDistance2Model.cpp" line="300"/>
        <source> Need something more...</source>
        <translation>Нужно ещё кое-что</translation>
    </message>
</context>
<context>
    <name>ProvisosData</name>
    <message>
        <location filename="../../models/ProvisosData.h" line="28"/>
        <source>Provisos</source>
        <translation>Условия</translation>
    </message>
</context>
<context>
    <name>QtNodes::FlowScene</name>
    <message>
        <location filename="../../external/nodeeditor/src/FlowScene.cpp" line="447"/>
        <location filename="../../external/nodeeditor/src/FlowScene.cpp" line="475"/>
        <source>Open Flow Scene</source>
        <translation>Открыть сцену</translation>
    </message>
    <message>
        <location filename="../../external/nodeeditor/src/FlowScene.cpp" line="449"/>
        <location filename="../../external/nodeeditor/src/FlowScene.cpp" line="477"/>
        <source>Flow Scene Files (*.flow)</source>
        <translation>Файлы сцены</translation>
    </message>
</context>
<context>
    <name>RiverListData</name>
    <message>
        <location filename="../../external/nodeeditor/examples/dynamic_node/RiverListData.h" line="22"/>
        <source>RiverList</source>
        <translation>Список рек</translation>
    </message>
</context>
<context>
    <name>RiverListModel</name>
    <message>
        <location filename="../../external/nodeeditor/examples/dynamic_node/RiverListModel.h" line="29"/>
        <source>RiverList</source>
        <translation>Список рек</translation>
    </message>
</context>
<context>
    <name>RiverModel</name>
    <message>
        <location filename="../../external/nodeeditor/examples/dynamic_node/RiverModel.h" line="30"/>
        <source>River</source>
        <translation>Река</translation>
    </message>
</context>
<context>
    <name>SceneData</name>
    <message>
        <location filename="../../models/SceneData.h" line="27"/>
        <source>Scene</source>
        <translation>Сцена</translation>
    </message>
</context>
<context>
    <name>ScenesData</name>
    <message>
        <location filename="../../models/ScenesData.h" line="28"/>
        <source>Scenes</source>
        <translation>Сцены</translation>
    </message>
</context>
<context>
    <name>TowerData</name>
    <message>
        <location filename="../../models/TowerData.h" line="28"/>
        <source>Tower</source>
        <translation>Башня</translation>
    </message>
</context>
<context>
    <name>wquest::MainWindow</name>
    <message>
        <location filename="../../MainWindow.cpp" line="12"/>
        <source>Add quest</source>
        <translation>Добавить квест</translation>
    </message>
    <message>
        <location filename="../../MainWindow.h" line="28"/>
        <source>WQuest</source>
        <translation>WQuest</translation>
    </message>
</context>
<context>
    <name>wquest::ProvisosModel</name>
    <message>
        <source>Provisos</source>
        <translation type="vanished">Условия</translation>
    </message>
</context>
<context>
    <name>wquest::QDWindow</name>
    <message>
        <location filename="../../QDWindow.cpp" line="161"/>
        <location filename="../../QDWindow.cpp" line="162"/>
        <location filename="../../QDWindow.cpp" line="163"/>
        <location filename="../../QDWindow.cpp" line="164"/>
        <location filename="../../QDWindow.cpp" line="165"/>
        <source>Base elements</source>
        <translation>Базовые элементы</translation>
    </message>
    <message>
        <location filename="../../QDWindow.cpp" line="168"/>
        <source>Provisos</source>
        <translation>Условия</translation>
    </message>
    <message>
        <location filename="../../QDWindow.h" line="31"/>
        <source>Quest</source>
        <translation>Квест</translation>
    </message>
</context>
<context>
    <name>wquest::SceneModel</name>
    <message>
        <source>Scene</source>
        <translation type="vanished">Сцена</translation>
    </message>
</context>
<context>
    <name>wquest::TowerModel</name>
    <message>
        <source>Tower</source>
        <translation type="vanished">Башня</translation>
    </message>
</context>
<context>
    <name>wquest::ValueModel</name>
    <message>
        <source>Value</source>
        <translation type="vanished">Значение</translation>
    </message>
</context>
</TS>
