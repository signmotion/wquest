#pragma once

#include "FlowWindow.h"


namespace wquest {


class WorldWindow : public FlowWindow {
    Q_OBJECT


public:
    WQUEST_DECL_PTR_T(WorldWindow)

    WorldWindow( uid_t, QWidget *parent = nullptr );

    ~WorldWindow() override;


protected:
    virtual QString nameTitle() const override {
        return tr( "WQuest" );
    }

    virtual int quantWidth() const override {
        return 250;
    }

    void closeEvent( QCloseEvent * );

    virtual void updateUI() override;

    void setModel();

    virtual void putNodes() override;


private:
    uids_t qdUIDs_;
};


} // wquest
