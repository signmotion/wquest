#include <wquest/dbgMacros.h>
#include <wquest/common.h>
#include <QTranslator>

#ifdef WQUEST_TESTS

#define CATCH_CONFIG_MAIN
//#define CATCH_CONFIG_NOSTDOUT
#include <catch2/all.hpp>


static int runTest( int argc, char *argv[] ) {

    Catch::Session s;
    s.applyCommandLine( argc, argv );
    return s.run();
}

#else

// develop the global method for own storage
#define WQUEST_OWN_STORE
#include <wquest/StoreFile.h>


namespace wquest {

Store &store() {
    static const std::string path = "data/my/";
    static StoreFile inst( path );
    return inst;
}

} // wquest

#endif


#include "WindowManager.h"
#include <QApplication>
#include <QFont>




int main( int argc, char *argv[] ) {

    using namespace wquest;

#ifndef WQUEST_TESTS
    QApplication a( argc, argv );
    const QFont font( "", 14 );
    QApplication::setFont( font );
    WindowManager::instance().world( 1 );

    return a.exec();

#else
    return runTest( argc, argv );
#endif
}
