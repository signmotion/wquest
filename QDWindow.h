#pragma once

#include "FlowWindow.h"
#include <wquest/QD.h>
#include <nodes/FlowScene>


namespace wquest {


class QDWindow : public FlowWindow {
    Q_OBJECT


public:
    WQUEST_DECL_PTR_T( QDWindow )

    QDWindow( uid_t uid, QWidget *parent );

    virtual ~QDWindow() override;

    QD::ptr_t qd() const {
        return data< QD >();
    }


protected:
    virtual QString nameTitle() const override {
        return tr( "Quest" );
    }

    virtual int quantWidth() const override {
        return 300;
    }

    virtual void updateUI() override;

    void setModel();

#if 0
    void setExampleModelCalculator();
    void setExampleModelConnectionColors();
    void setExampleModelDynamicNode();
    void setExampleModelImages();
    void setExampleModelStyles();
    void setExampleModelText();
#endif

    virtual void putNodes() override;
};


} // wquest
