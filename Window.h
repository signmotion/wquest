#pragma once

#include <wquest/D.h>

#include <QWidget>
#include <QSize>
#include <QString>
#include <QPalette>
#include <QKeyEvent>


namespace wquest {


class Window : public QWidget {
    Q_OBJECT


public:
    WQUEST_DECL_PTR_T(Window)

    Window( const D::ptr_t, QWidget *parent );

    virtual ~Window() override;

    template< class T >
    std::shared_ptr< T > data() const {
        return data_->cast< T >();
    }

    const D::ptr_t &data() const {
        return data_;
    }

    virtual QString nameTitle() const = 0;

    virtual int quantWidth() const = 0;

    /**
     * @brief Size of element in this window for holistic view.
     * @details
     * Use sizes with golden ratio in this app.
     * Calc by `quantWidth()`.
     * @param landscape Set to `false` for portraite orientation.
     */
    QSize quantSize( bool landscape = true ) const {
        const int w = quantWidth();
        const int h = (int)(w * math::goldenRatio62);
        return QSize{ landscape ? w : h, landscape ? h : w };
    }

    /**
     * @brief Set a palette for parent windows palette.
     */
    void setPalette();


protected:
    virtual void keyPressEvent( QKeyEvent * ) override;

    virtual void closeEvent( QCloseEvent * ) override;

    virtual void updateUI() = 0;


private:
    D::ptr_t data_;
};


} // wquest
