#include "NaiveDataModel.h"


NaiveDataModel::~NaiveDataModel() {
}




QString
NaiveDataModel::caption() const {
    return QString( "Naive Data Model" );
}




QString
NaiveDataModel::name() const {
    return QString( "NaiveDataModel" );
}




unsigned int
NaiveDataModel::nPorts( PortType portType ) const {

    unsigned int result = 1;

    switch ( portType ) {
        case PortType::In:
            result = 2;
            break;

        case PortType::Out:
            result = 2;
            break;

        case PortType::None:
            break;
    }

    return result;
}




NodeDataType
NaiveDataModel::dataType( PortType portType, PortIndex portIndex ) const {

    switch ( portType ) {
        case PortType::In :
            switch ( portIndex ) {
                case 0:
                    return MyNodeData().type();
                case 1:
                    return SimpleNodeData().type();
            }
            break;

        case PortType::Out :
            switch ( portIndex ) {
                case 0:
                    return MyNodeData().type();
                case 1:
                    return SimpleNodeData().type();
            }
            break;

        case PortType::None:
            break;
    }

    return NodeDataType();
}




std::shared_ptr< NodeData >
NaiveDataModel::outData( PortIndex port ) {

    if (port < 1) {
        return std::make_shared< MyNodeData >();
    }

    return std::make_shared< SimpleNodeData >();
}




void
NaiveDataModel::setInData( std::shared_ptr< NodeData >, int ) {
}




QWidget *
NaiveDataModel::embeddedWidget() {
    return nullptr;
}
