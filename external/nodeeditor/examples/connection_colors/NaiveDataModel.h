#pragma once

#include <QtCore/QObject>
#include <nodes/NodeData>
#include <nodes/NodeDataModel>
#include <memory>


using QtNodes::NodeData;
using QtNodes::NodeDataModel;
using QtNodes::NodeDataType;
using QtNodes::PortIndex;
using QtNodes::PortType;


class MyNodeData : public NodeData {
public:
    NodeDataType type() const override {
        return NodeDataType{
            "MyNodeData",
            "My Node Data"
        };
    }
};




class SimpleNodeData : public NodeData {
public:
    NodeDataType type() const override {
        return NodeDataType{
            "SimpleData",
            "Simple Data"
        };
    }
};




class NaiveDataModel : public NodeDataModel {
    Q_OBJECT


public:
    virtual ~NaiveDataModel() override;


public:
    QString caption() const override;

    QString name() const override;

    unsigned int nPorts( PortType ) const override;

    NodeDataType dataType( PortType, PortIndex ) const override;

    std::shared_ptr< NodeData > outData( PortIndex ) override;

    void setInData( std::shared_ptr< NodeData >, int ) override;

    QWidget *embeddedWidget() override;
};
