﻿#include <wquest/dbgMacros.h>

#ifdef WQUEST_TESTS

#include <catch2/all.hpp>
#include <wquest/common.h>
#include <cwctype>


using namespace wquest;


// English
static const std::string abcEnglishLower =
        "abcdefghijklmnopqrstuvwxyz";
static const std::string abcEnglishUpper =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

// Russian
static const std::wstring abcRussianLower =
        L"абвгдеёжзийклмнопрстуфхцчшщъыьэюя    ";
static const std::wstring abcRussianUpper =
        L"АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ    ";

// Japan, Hiragana
// \source https://en.wikipedia.org/wiki/Hiragana_(Unicode_block)
static const std::wstring abcJapanLower =
        L"ぁあぃいぅうぇえぉおかがきぎくぐけげこごさざしじすずせぜそぞただちぢっつづ"
        "てでとどなにぬねのはばぱひびぴふぶぷへべぺほぼぽまみむめもゃやゅゆょよら"
        "りるれろゎわゐゑをんゔゕゖ゙゚ゝゞゟ";
static const std::wstring abcJapanUpper =
        abcJapanLower;




// Helper foreach by ABC with `std::string` and `std::wstring`.
template< class String >
inline void
abc(
    const String &lower,
    const String &upper
) {
    REQUIRE( lower.size() == upper.size() );
    for ( auto itLower = lower.cbegin(),
          itUpper = upper.cbegin();
          itLower != lower.cend();
          ++itLower, ++itUpper
    ) {
        /*
        const auto chLower = *itLower;
        std::cout << "\n" << chLower;
        const auto chUpper = std::towupper( *itLower );
        std::cout << "\n" << (chLower == chUpper);
        const String s( 1, *itLower );
        std::cout << "\n" << is::capitalizeFirst( s );
        const String r = make::capitalizeFirstCopy( s );
        std::cout << "\n" << r;
        std::cout << std::endl;
        continue;
        */
        // with lowercase
        {
            int i = 0;
            INFO( "i " << i++ );
            const String s( 1, *itLower );
            REQUIRE_FALSE( is::capitalizeFirst( s ) );
            const String e( 1, *itUpper );
            const String r = make::capitalizeFirstCopy( s );
            REQUIRE( e == r );
        }
        // with uppercase
        {
            const String s( 1, *itUpper );
            REQUIRE( is::capitalizeFirst( s ) );
            const String r = make::capitalizeFirstCopy( s );
            REQUIRE( s == r );
        }
    }
};




TEST_CASE( "capitalizeFirst()", "[common][is][make]" ) {

    SECTION( "Empty" ) {
        const std::string s = "";
        CHECK_FALSE( is::capitalizeFirst( s ) );
        const std::string r = make::capitalizeFirstCopy( s );
        CHECK( s == r );
    }

    SECTION( "With space starts" ) {
        const std::string s = "     World quest";
        CHECK_FALSE( is::capitalizeFirst( s ) );
        const std::string r = make::capitalizeFirstCopy( s );
        CHECK( s == r );
    }


    SECTION( "English" ) {
        abc( abcEnglishLower, abcEnglishUpper );
    }

    /* - TODO
    SECTION( "Russian" ) {
        abc( abcRussianLower, abcRussianUpper );
    }
    */
}




TEST_CASE( "shorter() on the non empty string", "[common][make]" ) {

    const std::string o = "Some not very long string";
    const int lo = o.length();
    REQUIRE( lo > 0 );
    const std::string tail = "...";
    const int lt = tail.length();
    REQUIRE( lt > 0 );

    SECTION( "String is shorter then expected" ) {
        const std::string p = make::shorterCopy( o, lo + 100 );
        CHECK( p == o );
    }

    SECTION( "String is equals to expected" ) {
        const std::string p = make::shorterCopy( o, lo );
        CHECK( p == o );
    }

    SECTION( "String is longer then expected by 1" ) {
        const std::string p = make::shorterCopy( o, lo - 1 );
        CHECK( p == "Some not very long str..." );
    }

    SECTION( "String is longer then expected by length of tail" ) {
        const std::string p = make::shorterCopy( o, lo - lt );
        CHECK( p == "Some not very long str..." );
    }

    SECTION( "String is longer then expected by more then length of tail" ) {
        const std::string p = make::shorterCopy( o, lo - lt - 1 );
        CHECK( p == "Some not very long st..." );
    }

    SECTION( "Expected length of string is equals to length of tail" ) {
        const std::string p = make::shorterCopy( o, lt );
        CHECK( p == tail );
    }

    SECTION( "Expected length of string is shorter then length of tail" ) {
        const std::string p = make::shorterCopy( o, lt - 1 );
        CHECK( p == tail );
    }

    SECTION( "Expected length of string is zero" ) {
        const std::string p = make::shorterCopy( o, 0 );
        CHECK( p == tail );
    }
}




TEST_CASE( "shorter() on the empty string", "[common][make]" ) {

    const std::string o = "";
    const int lo = o.length();
    REQUIRE( lo == 0 );
    const std::string tail = "...";
    const int lt = tail.length();
    REQUIRE( lt > 0 );

    SECTION( "String is shorter then expected" ) {
        const std::string p = make::shorterCopy( o, lo + 100 );
        CHECK( p == o );
    }

    SECTION( "String is equals to expected" ) {
        const std::string p = make::shorterCopy( o, lo );
        CHECK( p == o );
    }
}




TEST_CASE( "shorter() on the empty tail", "[common][make]" ) {

    const std::string o = "Some string but tail is empty";
    const int lo = o.length();
    REQUIRE( lo > 0 );
    const std::string tail = "";
    const int lt = tail.length();
    REQUIRE( lt == 0 );

    SECTION( "String is shorter then expected" ) {
        const std::string p = make::shorterCopy( o, lo + 100, tail );
        CHECK( p == o );
    }

    SECTION( "String is equals to expected" ) {
        const std::string p = make::shorterCopy( o, lo, tail );
        CHECK( p == o );
    }

    SECTION( "String is longer then expected by 1" ) {
        const std::string p = make::shorterCopy( o, lo - 1, tail );
        CHECK( p == "Some string but tail is empt" );
    }

    SECTION( "String is longer then expected" ) {
        const std::string p = make::shorterCopy( o, 11, tail );
        CHECK( p == "Some string" );
    }
}




TEST_CASE( "emptyBraces()", "[common][make]" ) {

    SECTION( "Non empty string" ) {
        const std::string o =
            "When distance between [instance A] and [instance B]"
            " is [compare] than [distance] m";
        const int lo = o.length();
        REQUIRE( lo > 0 );
        const std::string p = make::emptyBraces( o );
        CHECK( p.length() < lo );
        CHECK( p == "When distance between {} and {} is {} than {} m" );
    }

    SECTION( "Empty string" ) {
        const std::string o = "";
        const int lo = o.length();
        REQUIRE( lo == 0 );
        const std::string p = make::emptyBraces( o );
        CHECK( p.empty() );
        CHECK( p == "" );
    }

    SECTION( "Non correct braces" ) {
        const std::string o =
            "When distance between [instance A and [instance B]"
            " is less than 100 m";
        const int lo = o.length();
        REQUIRE( lo > 0 );
        const std::string p = make::emptyBraces( o );
        CHECK( is::undefined( p ) );
    }
}


#endif
