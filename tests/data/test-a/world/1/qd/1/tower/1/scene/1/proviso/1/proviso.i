{
  "uid": 1,
  "type": "Proviso",
  "declare": "When distance between [instance A] and [instance B] is [compare] than [distance] m",
  "morphers": {
    "instance A": "Игрок",
    "instance B": "Драгоценность",
    "compare": "less",
    "distance": 50
  }
}
