#include <wquest/dbgMacros.h>

#ifdef WQUEST_TESTS

#include <catch2/all.hpp>
#include <wquest/LazyList.h>
#include <wquest/DM.h>

#include "DefineTestStore.h"


using namespace wquest;




std::shared_ptr< D >
ownJSONParser( const json_t &, const std::shared_ptr< D > parent );




class Item : public D {
public:
    WQUEST_DECL_CAST_AND_PTR( Item )

    Item() = delete;

    Item( uid_t uid, const std::string &name, const parent_t parent ) :
        D( uid, parent )
    {
        ASSERT( !name.empty() );
        Item::name( name );
    }

    virtual Type type() const override {
        return etypeStatic();
    }

    virtual std::string stype() const override {
        return stypeStatic();
    }

    static std::string stypeStatic() {
        return "Item";
    }

    static Type etypeStatic() {
        return (Type)((int)Type::lowerBarUserType + 2);
    }

    bool operator==( const Item &b ) const {
        return (uid() == b.uid()) &&
               (name() == b.name());
    }

    const std::string &name() const {
        return name_;
    }

    void name( const std::string &name ) {
        initField( &name_, name );
    }


    virtual json_t toJSON( rapidjson::Document *outer ) const override {

        using namespace rapidjson;

        Document d( kObjectType );
        Document::AllocatorType &a =
                outer ? outer->GetAllocator() : d.GetAllocator();
        d.AddMember( "uid", uid(), a );
        d.AddMember( "type", stype(), a );
        d.AddMember( "name", name(), a );

        return d;
    }


    /**
     * @warning Not uses `DM::emit()`.
     */
    static Item::ptr_t fromJSON( const json_t &json, parent_t parent ) {

        using namespace rapidjson;

        ASSERT( json.IsObject() );

        INFO( "Item::fromJSON()\n" << std::to_string( json ) );

        ASSERT( json.HasMember( "type" ) );
        const std::string type = json[ "type" ].GetString();
        if ( type != stypeStatic() ) {
            return nullptr;
        }

        ASSERT( json.HasMember( "uid" ) );
        const uid_t uid = json[ "uid" ].GetInt();
        if ( !is::correct( uid ) ) {
            return nullptr;
        }

        const std::string name =
                json.HasMember( "name" ) ? json[ "name" ].GetString() : "";

        const Item::ptr_t item =
                std::make_shared< Item >( uid, name, parent );

        return item;
    }


private:
    std::string name_;
};




class Backpack : public D {
public:
    WQUEST_DECL_CAST_AND_PTR( Backpack )

    Backpack() = delete;

    Backpack( uid_t uid, const parent_t parent ) :
        D( uid, parent )
    {
    }

    virtual Type type() const override {
        return etypeStatic();
    }

    virtual std::string stype() const override {
        return stypeStatic();
    }

    static std::string stypeStatic() {
        return "Backpack";
    }

    static Type etypeStatic() {
        return (Type)((int)Type::lowerBarUserType + 1);
    }

    bool operator==( const Backpack &b ) const {
        return (uid() == b.uid());
    }

    int countItem() {
        initItems();
        return items_->count();
    }

    Item::ptr_t itemFront( bool load = true ) {
        return item( 0, load );
    }

    Item::ptr_t itemBack( bool load = true ) {
        initItems();
        return item( items_->count() - 1, load );
    }

    Item::ptr_t item( index_t i, bool load = true ) {
        initItems();
        return items_->getI( i, load );
    }

    void addItem( const Item::ptr_t item ) {
        ASSERT( item );
        initItems();
        VERIFY( items_->add( item ) );
    }

    /**
     * @brief Create and insert `Item` before `index_t`.
     */
    void addItem( index_t i, const Item::ptr_t item ) {
        ASSERT( item );
        initItems();
        VERIFY( items_->add( i, item ) );
    }

    void removeItem( index_t i ) {
        initItems();
        VERIFY( items_->remove( i ) );
    }

    void clearItem() {
        initItems();
        items_->clear();
    }

    const LazyList< Item >::ptr_t &items() {
        initItems();
        return items_;
    }


    virtual json_t toJSON( rapidjson::Document *outer ) const override {

        using namespace rapidjson;

        Document d( kObjectType );
        Document::AllocatorType &a =
                outer ? outer->GetAllocator() : d.GetAllocator();
        d.AddMember( "uid", uid(), a );
        d.AddMember( "type", stype(), a );
        if ( items_ && !items_->empty() ) {
            d.AddMember( "countItem", items_->count(), a );
            // not only order, a list also
            Document nd( kArrayType );
            for (const auto &pd : items_->dps()) {
                nd.PushBack( pd.first, a );
            }
            d.AddMember( "orderItem", nd, a );
        }

        return d;
    }


    /**
     * @warning Not uses `DM::emit()`.
     */
    static Backpack::ptr_t fromJSON( const json_t &json, parent_t parent ) {

        using namespace rapidjson;

        ASSERT( json.IsObject() );

        INFO( "Backpack::fromJSON()\n" << std::to_string( json ) );

        ASSERT( json.HasMember( "type" ) );
        const std::string type = json[ "type" ].GetString();
        if ( type != stypeStatic() ) {
            return nullptr;
        }

        ASSERT( json.HasMember( "uid" ) );
        const uid_t uid = json[ "uid" ].GetInt();
        if ( !is::correct( uid ) ) {
            return nullptr;
        }

        const Backpack::ptr_t backpack =
                std::make_shared< Backpack >( uid, parent );

        // Item
        // \todo fine Wrap to `ASSERT_JSON_OBJECT( object, field, Is#Type() )`.
        const bool hasItem = json.HasMember( "countItem" );
        if ( hasItem ) {
            // \todo optimize Don't parse `countItem`.
            const int countItem = json[ "countItem" ].GetInt();
            ASSERT( json.HasMember( "orderItem" ) );
            const Value &orderItem = json[ "orderItem" ];
            ASSERT( orderItem.IsArray() );
            ASSERT( countItem == orderItem.GetArray().Size() );
            backpack->addItems( orderItem );
        }

        return backpack;
    }


private:
    void initItems() {
        if ( !items_ ) {
            items_.reset( new LazyList< Item >( cast< D >(), ownJSONParser ) );
        }
    }

    void addItems( const rapidjson::Value &uids ) {
        ASSERT( uids.IsArray() );
        initItems();
        VERIFY( items_->add( uids ) );
    }


private:
    LazyList< Item >::ptr_t items_;
};




std::shared_ptr< D >
ownJSONParser( const json_t &json, const std::shared_ptr< D > parent ) {

    ASSERT( json.HasMember( "type" ) );
    const std::string stype = json[ "type" ].GetString();
    ASSERT( !stype.empty() );

    if (stype == Backpack::stypeStatic()) {
        return Backpack::fromJSON( json, parent );
    }
    else if (stype == Item::stypeStatic()) {
        return Item::fromJSON( json, parent );
    }

    ASSERT( false && "ownJSONParser() Unrecognized type." );
    return nullptr;
}




std::string
ownTypeConverterToS( Type type ) {

    if (type == Backpack::etypeStatic()) {
        return Backpack::stypeStatic();
    }
    else if (type == Item::etypeStatic()) {
        return Item::stypeStatic();
    }

    ASSERT( false && "ownTypeConverterToS() Unrecognized type." );
    return "";
}




bool
sameFileAndType(
        const std::shared_ptr< D > d,
        const std::shared_ptr< D > parent
) {
    std::string stype = d->stype();
    std::transform( stype.cbegin(), stype.cend(), stype.begin(), ::tolower );
    const std::string pp = parent ? parent->path() : "";
    const std::string p = pp + stype + "/";
    Store &s = store();
    StoreFile &sf = static_cast< StoreFile & >( s );

    return sf.hasFolder( p );
}




TEST_CASE( "Save to and load from file store", "[LazyList]" ) {

    dm().clear();
    INFO( "DM dump after clear\n" << dm().dump() );
    REQUIRE( dm().empty() );
    Store &s = store();
    //StoreFile &sf = static_cast< StoreFile & >( s );
    s.typeConverterToS( ownTypeConverterToS );

    // create a backpack
    const Backpack::ptr_t backpack = dm().emitD< Backpack >( nullptr );
    const uid_t uidBackpack = backpack->uid();
    REQUIRE( uidBackpack == 1 );

    // add some Items to world
    constexpr int nItem = 10;
    std::vector< Item::ptr_t > items;
    for (int i = 0; i < nItem; ++i) {
        const std::string name = std::to_string( i + 1 );
        const Item::ptr_t item = dm().emitD< Item >( name, backpack );
        const uid_t uidItem = item->uid();
        REQUIRE( uidItem == (i + 1) );
        backpack->addItem( item );
        items.emplace_back( item );
    }
    INFO( "DM dump after emit items\n" << dm().dump() );

    // save to the store
    dm().flush().wait();
    const int countInMemory = dm().countInMemory< Backpack >();
    const int countInStore= dm().countInStore< Backpack >( nullptr );
    // backpack
    REQUIRE( dm().countInMemory< Backpack >() ==
             dm().countInStore< Backpack >( nullptr ) );
    // item
    REQUIRE( dm().countInMemory< Item >() ==
             dm().countInStore< Item >( backpack ) );

    // remove all from the memory
    dm().clearFromMemory();
    REQUIRE( dm().countInMemory() == 0 );


    SECTION( "Get some from the store" ) {
        // backpack
        const Backpack::ptr_t loadedBackpack =
                dm().get< Backpack >( nullptr, uidBackpack, ownJSONParser );
        REQUIRE( loadedBackpack );
        REQUIRE( Backpack::stypeStatic() == loadedBackpack->stype() );
        REQUIRE( loadedBackpack->countItem() == backpack->countItem() );
        CHECK( *backpack == *loadedBackpack );
        CHECK( sameFileAndType( loadedBackpack, nullptr ) );

        // get some random item from lazy list
        constexpr index_t i = nItem / 2;
        constexpr bool load = true;
        // without load from the store
        Item::ptr_t loadedItem = loadedBackpack->item( i, !load );
        REQUIRE_FALSE( loadedItem );
        // with load from the store
        loadedItem = loadedBackpack->item( i, load );
        REQUIRE( loadedItem );
        REQUIRE( Item::stypeStatic() == loadedItem->stype() );
        // without load from the store another: must stay in the memory
        loadedItem = loadedBackpack->item( i, !load );
        REQUIRE( loadedItem );
        CHECK( *items[ i ] == *loadedItem );
        CHECK( sameFileAndType( loadedItem, loadedBackpack ) );
    }
}


#endif
