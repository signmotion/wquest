#include <wquest/dbgMacros.h>

#ifdef WQUEST_TESTS

#include <catch2/all.hpp>
#include <wquest/QD.h>
#include <wquest/DM.h>


using namespace wquest;


/*
TEST_CASE( "Create without UID", "[QD]" ) {

    INFO( "UID should increase always" );

    {
        const QD::ptr_t a = dm().emitD< QD >();
        REQUIRE( a->uid() == 1 );
        REQUIRE( a->towerFront()->uid() == 1 );
        REQUIRE( a->towerFront()->sceneFront()->uid() == 1 );
    }

    {
        const QD::ptr_t a = dm().emitD< QD >();
        REQUIRE( a->uid() == 2 );
        REQUIRE( a->towerFront()->uid() == 2 );
        REQUIRE( a->towerFront()->sceneFront()->uid() == 2 );
    }

    {
        dm().nextUIDAfter( Type::QD, 1000 - 1 );
        const QD::ptr_t a = dm().emitD< QD >();
        REQUIRE( a->uid() == 1000 );
        REQUIRE( a->towerFront()->uid() == 3 );
        REQUIRE( a->towerFront()->sceneFront()->uid() == 3 );
    }

    {
        const QD::ptr_t a = dm().emitD< QD >();
        REQUIRE( a->uid() == 1001 );
        REQUIRE( a->towerFront()->uid() == 4 );
        REQUIRE( a->towerFront()->sceneFront()->uid() == 4 );
    }

    {
        // less UID should ignore
        dm().nextUIDAfter( Type::QD, 900 - 1 );
        const QD::ptr_t a = dm().emitD< QD >();
        REQUIRE( a->uid() == 1002 );
        REQUIRE( a->towerFront()->uid() == 5 );
        REQUIRE( a->towerFront()->sceneFront()->uid() == 5 );
    }

    {
        const QD::ptr_t a = dm().emitD< QD >();
        REQUIRE( a->uid() == 1003 );
        REQUIRE( a->towerFront()->uid() == 6 );
        REQUIRE( a->towerFront()->sceneFront()->uid() == 6 );
    }
}




TEST_CASE( "Create and work with towers", "[QD]" ) {

    QD::ptr_t qd = dm().emitD< QD >();
    INFO( "We must have 1 tower and 1 scene in tower always" );
    CHECK( qd->countTower() == 1 );
    REQUIRE( qd->towers().size() == 1 );
    REQUIRE( qd->towerFront()->countScene() == 1 );
    CHECK_FALSE( qd->empty() );

    SECTION( "Add a new tower to the front of QD" ) {
        Tower::ptr_t tower = dm().emitD< Tower >();
        qd->addTowerFront( tower );
        REQUIRE( qd->countTower() == 2 );
        REQUIRE( qd->towerFront()->uid() == tower->uid() );
    }

    SECTION( "Add a new tower to the back of QD" ) {
        Tower::ptr_t tower = dm().emitD< Tower >();
        qd->addTowerBack( tower );
        REQUIRE( qd->countTower() == 2 );
        REQUIRE( qd->towerBack()->uid() == tower->uid() );
    }

    SECTION( "Add a new tower to the middle of QD" ) {
        Tower::ptr_t towerC = dm().emitD< Tower >();
        qd->addTowerBack( towerC );
        Tower::ptr_t towerB = dm().emitD< Tower >();
        qd->addTower( 1, towerB );
        REQUIRE( qd->countTower() == 3 );
        REQUIRE( qd->tower( 0 )->uid() != towerB->uid() );
        REQUIRE( qd->tower( 1 )->uid() == towerB->uid() );
        REQUIRE( qd->tower( 2 )->uid() == towerC->uid() );
    }
}




TEST_CASE( "Create and work with scenes", "[QD]" ) {

    const QD::ptr_t qd = dm().emitD< QD >();
    INFO( "We must have 1 tower and 1 scene in tower always" );
    CHECK( qd->countTower() == 1 );
    REQUIRE( qd->towers().size() == 1 );
    REQUIRE( qd->towerFront()->countScene() == 1 );
    CHECK_FALSE( qd->empty() );

    SECTION( "Add a new scene to the front of tower" ) {
        Scene::ptr_t scene = dm().emitD< Scene >();
        qd->addSceneFront( 0, scene );
        REQUIRE( qd->countTower() == 1 );
        REQUIRE( qd->towerFront()->countScene() == 2 );
        REQUIRE( qd->towerFront()->sceneFront()->uid() == scene->uid() );
    }

    SECTION( "Add a new scene to the back of tower" ) {
        Scene::ptr_t scene = dm().emitD< Scene >();
        qd->addSceneBack( 0, scene );
        REQUIRE( qd->countTower() == 1 );
        REQUIRE( qd->towerFront()->countScene() == 2 );
        REQUIRE( qd->towerFront()->sceneBack()->uid() == scene->uid() );
    }

    SECTION( "Add a new scene to the middle of tower" ) {
        Scene::ptr_t sceneC = dm().emitD< Scene >();
        qd->addSceneBack( 0, sceneC );
        Scene::ptr_t sceneB = dm().emitD< Scene >();
        qd->addScene( 0, 1, sceneB );
        REQUIRE( qd->countTower() == 1 );
        REQUIRE( qd->towerFront()->countScene() == 3 );
        REQUIRE( qd->towerFront()->scene( 0 )->uid() != sceneB->uid() );
        REQUIRE( qd->towerFront()->scene( 1 )->uid() == sceneB->uid() );
        REQUIRE( qd->towerFront()->scene( 2 )->uid() == sceneC->uid() );
    }
}




TEST_CASE( "Moving scenes", "[QD]" ) {

    QD::ptr_t qd = dm().emitD< QD >();
    INFO( "We must have 1 tower and 1 scene in tower always" );
    // add more towers and fill it scenes

    // tower A, 3 scenes
    Tower::ptr_t towerA = qd->tower( 0 );
    Scene::ptr_t sceneA0 = towerA->scene( 0 );
    Scene::ptr_t sceneA1 = dm().emitD< Scene >();
    qd->addSceneBack( 0, sceneA1 );
    Scene::ptr_t sceneA2 = dm().emitD< Scene >();
    qd->addSceneBack( 0, sceneA2 );
    REQUIRE( towerA->countScene() == 3 );

    // tower B, 1 scene
    Tower::ptr_t towerB = dm().emitD< Tower >();
    qd->addTowerBack( towerB );
    Scene::ptr_t sceneB0 = towerB->scene( 0 );
    REQUIRE( towerB->countScene() == 1 );

    REQUIRE( qd->countTower() == 2 );
    // A0  B0
    // A1
    // A2


    SECTION( "Move the scenes from first tower to second tower" ) {
        // move a first scene to the back of tower B
        qd->moveScene( 0, 0, 1, 1 );
        // A1  B0
        // A2  A0
        REQUIRE( towerA->countScene() == 2 );
        REQUIRE( towerB->countScene() == 2 );
        CHECK( sceneA1->uid() == towerA->scene( 0 )->uid() );
        CHECK( sceneA2->uid() == towerA->scene( 1 )->uid() );
        CHECK( sceneB0->uid() == towerB->scene( 0 )->uid() );
        CHECK( sceneA0->uid() == towerB->scene( 1 )->uid() );

        // move a last scene before moved early scene (in middle tower B)
        qd->moveScene( 0, 1, 1, 1 );
        // A1  B0
        //     A2
        //     A0
        REQUIRE( towerA->countScene() == 1 );
        REQUIRE( towerB->countScene() == 3 );
        CHECK( sceneA1->uid() == towerA->scene( 0 )->uid() );
        CHECK( sceneB0->uid() == towerB->scene( 0 )->uid() );
        CHECK( sceneA2->uid() == towerB->scene( 1 )->uid() );
        CHECK( sceneA0->uid() == towerB->scene( 2 )->uid() );

        // move a remaining scene from tower A to the front of tower B
        // the tower A should disappear
        qd->moveScene( 0, 0, 1, 0 );
        // A1
        // B0
        // A2
        // A0
        REQUIRE( qd->countTower() == 1 );
        REQUIRE( towerA->countScene() == 0 );
        REQUIRE( towerB->countScene() == 4 );
        CHECK( sceneA1->uid() == towerB->scene( 0 )->uid() );
        CHECK( sceneB0->uid() == towerB->scene( 1 )->uid() );
        CHECK( sceneA2->uid() == towerB->scene( 2 )->uid() );
        CHECK( sceneA0->uid() == towerB->scene( 3 )->uid() );
    }
}
*/


#endif
