#include <wquest/dbgMacros.h>

#ifdef WQUEST_TESTS

#include <catch2/all.hpp>
#include <wquest/World.h>
#include <wquest/ProvisoWhenDistance2.h>
#include <wquest/DM.h>

#include "DefineTestStore.h"


using namespace wquest;




TEST_CASE( "Create the new world", "[DM]" ) {

    INFO( "DM should save or load QD to or from store" );
    dm().clear();
    INFO( "DM dump after clear\n" << dm().dump() );
    REQUIRE( dm().empty() );
    Store &s = store();
    StoreFile &sf = static_cast< StoreFile & >( s );
    REQUIRE_FALSE( sf.hasFolder( "world" ) );


    SECTION( "Add some QDs" ) {
        // create a world
        const World::ptr_t world = dm().emitD< World >( "My World", nullptr );
        const uid_t uidWorld = world->uid();
        REQUIRE( uidWorld == 1 );

        // add some QDs to world
        // QD with 3 towers
        const QD::ptr_t qdA = world->addQDBack();
        INFO( "DM dump after emit `qdA`\n" << dm().dump() );
        const uid_t uidQDA = qdA->uid();
        REQUIRE( uidQDA == 1 );
        Title::ptr_t titleQDA = qdA->title( "Title A" );
        Annotation::ptr_t annotationQDA = qdA->annotation( "Annotation A." );
        Tower::ptr_t tower1QDA = qdA->addTowerBack();
        Scene::ptr_t scene1Tower1QDA = tower1QDA->addSceneBack();
        Scene::ptr_t scene2Tower1QDA = tower1QDA->addSceneBack();
        Tower::ptr_t tower2QDA = qdA->addTowerBack();
        Scene::ptr_t scene1Tower2QDA = tower2QDA->addSceneBack();
        Tower::ptr_t tower3QDA = qdA->addTowerBack();
        Scene::ptr_t scene1Tower3QDA = tower3QDA->addSceneBack();
        Proviso::ptr_t provisoScene1Tower3QDA =
                scene1Tower3QDA->addProvisoBack< ProvisoWhenDistance2 >();

        INFO( "Title, Chapter, Annotation, Apron must be created by request only" );
        REQUIRE( dm().countInMemory< World >() == 1 );
        REQUIRE( dm().countInMemory< QD >() == 1 );
        REQUIRE( dm().countInMemory< Title >() == 1 );
        REQUIRE( dm().countInMemory< Annotation >() == 1 );
        REQUIRE( dm().countInMemory< Tower >() == 3 );
        REQUIRE( dm().countInMemory< Scene >() == 4 );
        REQUIRE( dm().countInMemory< Chapter >() == 0 );
        REQUIRE( dm().countInMemory< Impact >() == 0 );
        REQUIRE( dm().countInMemory< Apron >() == 0 );
        REQUIRE( dm().countInMemory< Proviso >() == 1 );
        REQUIRE( dm().countInStore() == 0 );

        INFO( "Work with memory only, save like files after call flush()" );
        // \todo optimize? Short the file extensions.
        // \todo optimize? Short the folder names.
        const std::string pw = "world/" + world->suid() + "/";
        const std::string pqd = pw + "qd/" + qdA->suid() + "/" ;
        CHECK_FALSE( sf.hasFolder( pw ) );
        CHECK_FALSE( sf.hasFolder( pqd ) );

        dm().flush();
        INFO( "Wait while saved to " << sf.basePath() );
        dm().wait();
        CHECK_FALSE( dm().stillFlushing() );

        // files checking by present
        REQUIRE( sf.hasFolder( pw ) );
        CHECK( sf.hasFile( pw + "world.1.i" ) );
        REQUIRE( sf.hasFolder( pqd ) );
        CHECK( sf.hasFile( pqd + "qd.1.i" ) );
        CHECK( sf.hasFile( pqd + "title/title.i" ) );
        CHECK( sf.hasFile( pqd + "annotation/annotation.i" ) );
        CHECK( sf.hasFile( pqd + "tower/1/tower.1.i" ) );
        CHECK( sf.hasFile( pqd + "tower/1/scene/1/scene.1.i" ) );
        CHECK( sf.hasFile( pqd + "tower/1/scene/2/scene.2.i" ) );
        CHECK( sf.hasFile( pqd + "tower/2/tower.2.i" ) );
        CHECK( sf.hasFile( pqd + "tower/2/scene/3/scene.3.i" ) );
        CHECK( sf.hasFile( pqd + "tower/3/tower.3.i" ) );
        CHECK( sf.hasFile( pqd + "tower/3/scene/4/scene.4.i" ) );
        CHECK( sf.hasFile( pqd + "tower/3/scene/4/proviso/1/proviso.1.i" ) );

        // files checking by content
        CHECK( sf.content( pw + "world.1.i" ) == "{\n"
               "  \"uid\": 1,\n"
               "  \"type\": \"World\",\n"
               "  \"name\": \"My World\",\n"
               "  \"countQD\": 1,\n"
               "  \"orderQD\": [1]\n"
               "}" );
        CHECK( sf.content( pqd + "qd.1.i" ) == "{\n"
               "  \"uid\": 1,\n"
               "  \"type\": \"QD\",\n"
               "  \"countTower\": 3,\n"
               "  \"orderTower\": [1, 2, 3]\n"
               "}" );
        CHECK( sf.content( pqd + "title/title.i" ) == "{\n"
               "  \"uid\": 1,\n"
               "  \"type\": \"Title\",\n"
               "  \"text\": \"Title A\"\n"
               "}" );
        CHECK( sf.content( pqd + "annotation/annotation.i" ) == "{\n"
               "  \"uid\": 1,\n"
               "  \"type\": \"Annotation\",\n"
               "  \"text\": \"Annotation A.\"\n"
               "}" );
        CHECK( sf.content( pqd + "tower/1/tower.1.i" ) == "{\n"
               "  \"uid\": 1,\n"
               "  \"type\": \"Tower\",\n"
               "  \"countScene\": 2,\n"
               "  \"orderScene\": [1, 2]\n"
               "}" );
        CHECK( sf.content( pqd + "tower/1/scene/1/scene.1.i" ) == "{\n"
               "  \"uid\": 1,\n"
               "  \"type\": \"Scene\"\n"
               "}" );
        CHECK( sf.content( pqd + "tower/2/tower.2.i" ) == "{\n"
               "  \"uid\": 2,\n"
               "  \"type\": \"Tower\",\n"
               "  \"countScene\": 1,\n"
               "  \"orderScene\": [3]\n"
               "}" );
        CHECK( sf.content( pqd + "tower/3/scene/4/scene.4.i" ) == "{\n"
               "  \"uid\": 4,\n"
               "  \"type\": \"Scene\",\n"
               "  \"countProviso\": 1,\n"
               "  \"orderProviso\": [1]\n"
               "}" );
        CHECK( sf.content( pqd + "tower/3/scene/4/proviso/1/proviso.1.i" ) ==
               "{\n"
               "  \"uid\": 1,\n"
               "  \"type\": \"Proviso\",\n"
               "  \"declare\": \"When distance between"
                                " [instance A] and [instance B]"
                                " is [compare] than [distance] m\"\n"
               "}" );
    } // SECTION "Add some QDs"
}




TEST_CASE( "Load from file store", "[DM]" ) {

    dm().clear();
    INFO( "DM dump after clear\n" << dm().dump() );
    REQUIRE( dm().empty() );
    store();

    // create a world
    const World::ptr_t world = dm().emitD< World >( "My World", nullptr );
    const uid_t uidWorld = world->uid();
    REQUIRE( uidWorld == 1 );

    // add some QDs to world
    // QD with 3 towers
    const QD::ptr_t qdA = world->addQDBack();
    INFO( "DM dump after emit `qdA`\n" << dm().dump() );
    const uid_t uidQDA = qdA->uid();
    REQUIRE( uidQDA == 1 );
    Title::ptr_t titleQDA = qdA->title( "Title A" );
    Annotation::ptr_t annotationQDA = qdA->annotation( "Annotation A." );
    Tower::ptr_t tower1QDA = qdA->addTowerBack();
    Scene::ptr_t scene1Tower1QDA = tower1QDA->addSceneBack();
    Scene::ptr_t scene2Tower1QDA = tower1QDA->addSceneBack();
    Tower::ptr_t tower2QDA = qdA->addTowerBack();
    Scene::ptr_t scene1Tower2QDA = tower2QDA->addSceneBack();
    Tower::ptr_t tower3QDA = qdA->addTowerBack();
    Scene::ptr_t scene1Tower3QDA = tower3QDA->addSceneBack();
    Proviso::ptr_t provisoScene1Tower3QDA =
            scene1Tower3QDA->addProvisoBack< ProvisoWhenDistance2 >();

    // save to the store
    dm().flush().wait();
    const int countInMemory = dm().countInMemory< Title >();
    const int countInStore= dm().countInStore< Title >( qdA );
    // world
    REQUIRE( dm().countInMemory< World >() ==
             dm().countInStore< World >( nullptr ) );
    // qd
    REQUIRE( dm().countInMemory< QD >() ==
             dm().countInStore< QD >( world ) );
    REQUIRE( dm().countInMemory< Title >() ==
             dm().countInStore< Title >( qdA ) );
    REQUIRE( dm().countInMemory< Annotation >() ==
             dm().countInStore< Annotation >( qdA ) );
    REQUIRE( dm().countInMemory< Tower >() ==
             dm().countInStore< Tower >( qdA ) );
    // scenes
    REQUIRE( dm().countInMemory< Scene >() ==
             dm().countInStore< Scene >( tower1QDA ) +
             dm().countInStore< Scene >( tower2QDA ) +
             dm().countInStore< Scene >( tower3QDA ));
    REQUIRE( dm().countInMemory< Chapter >() ==
             dm().countInStore< Chapter >( scene1Tower1QDA ) +
             dm().countInStore< Chapter >( scene2Tower1QDA ) +
             dm().countInStore< Chapter >( scene1Tower2QDA ) +
             dm().countInStore< Chapter >( scene1Tower3QDA ) );
    REQUIRE( dm().countInMemory< Impact >() ==
             dm().countInStore< Impact >( scene1Tower1QDA ) +
             dm().countInStore< Impact >( scene2Tower1QDA ) +
             dm().countInStore< Impact >( scene1Tower2QDA ) +
             dm().countInStore< Impact >( scene1Tower3QDA ) );
    REQUIRE( dm().countInMemory< Apron >() ==
             dm().countInStore< Apron >( scene1Tower1QDA ) +
             dm().countInStore< Apron >( scene2Tower1QDA ) +
             dm().countInStore< Apron >( scene1Tower2QDA ) +
             dm().countInStore< Apron >( scene1Tower3QDA ) );
    REQUIRE( dm().countInMemory< Proviso >() ==
             dm().countInStore< Proviso >( scene1Tower1QDA ) +
             dm().countInStore< Proviso >( scene2Tower1QDA ) +
             dm().countInStore< Proviso >( scene1Tower2QDA ) +
             dm().countInStore< Proviso >( scene1Tower3QDA ) );

    // remove all from the memory
    dm().clearFromMemory();
    REQUIRE( dm().countInMemory() == 0 );


    SECTION( "Get from the store" ) {
        // world
        const World::ptr_t loadedWorld =
                dm().get< World >( nullptr, uidWorld, nullptr );
        REQUIRE( loadedWorld );
        REQUIRE( World::stypeStatic() == loadedWorld->stype() );
        REQUIRE( world->name() == loadedWorld->name() );
        REQUIRE( world->countQD() == loadedWorld->countQD() );
        CHECK( *world == *loadedWorld );

        // qdA
        const QD::ptr_t loadedQDA = loadedWorld->qd( uidQDA );
        REQUIRE( loadedQDA );
        REQUIRE( qdA->countTower() == loadedQDA->countTower() );
        REQUIRE( tower1QDA->countScene() ==
                 loadedQDA->towerFront()->countScene() );

        REQUIRE( tower1QDA->sceneFront()->uid() ==
                 loadedQDA->towerFront()->sceneFront()->uid() );
        CHECK( *tower1QDA->sceneFront() ==
                 *loadedQDA->towerFront()->sceneFront() );

        INFO( "loadedQDA tower front " <<
              loadedQDA->towerFront()->toSJSON( nullptr ) );
        CHECK( *qdA->towerFront() == *loadedQDA->towerFront() );

        INFO( "qdA tower front " <<
              qdA->towerFront()->toSJSON( nullptr ) );
        CHECK( *qdA == *loadedQDA );
    }
}


#endif
