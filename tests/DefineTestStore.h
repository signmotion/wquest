#pragma once

#include <wquest/dbgMacros.h>

#ifdef WQUEST_TESTS


// develop the global method for own storage
#define WQUEST_OWN_STORE
#include <wquest/StoreFile.h>

namespace wquest {

inline Store &
store() {
    static const std::string path = "data/test-temp/";
    static StoreFile inst( path );
    return inst;
}

} // wquest


#endif
