#if 0

#include <wquest/dbgMacros.h>

#ifdef WQUEST_TESTS

#include <catch2/all.hpp>
#include <wquest/QD.h>
#include <wquest/DM.h>


using namespace wquest;


TEST_CASE( "Create and work with `ProvisoWhenDistance2`", "[Proviso]" ) {

    auto proviso = dm().emitD< ProvisoWhenDistance2 >();
    CHECK( proviso->invalid() );
    REQUIRE( proviso->countMorph() == 4 );

    SECTION( "Init the proviso with correct morphes" ) {
        proviso->instanceA( "Gamer" );
        CHECK( proviso->invalid() );
        proviso->instanceB( "Treasure" );
        CHECK( proviso->invalid() );
        proviso->compare( Compare::build( "less" ) );
        CHECK( proviso->invalid() );
        proviso->distance( 238 );
        CHECK( proviso->valid() );
    }
}


#endif

#endif
