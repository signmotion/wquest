#include <wquest/dbgMacros.h>

#ifdef WQUEST_TESTS

#include <catch2/all.hpp>
#include <wquest/DM.h>
#include <wquest/QD.h>
#include <wquest/World.h>


using namespace wquest;


TEST_CASE( "Build a path", "[D]" ) {

    const World::ptr_t world = dm().emitD< World >( "Peace World", nullptr );
    REQUIRE( world->uid() == 1 );
    CHECK( "world/1/" == world->path() );
    CHECK( world->multiInParent( Type::QD ) );

    // first QD
    {
        const QD::ptr_t qd = world->addQDBack();
        REQUIRE( qd->uid() == 1 );
        CHECK( qd->singleInParent( Type::Title ) );
        CHECK( qd->singleInParent( Type::Annotation ) );
        CHECK( qd->multiInParent( Type::Tower ) );
        CHECK( "world/1/qd/1/" == qd->path() );

        const Title::ptr_t title = dm().emitD< Title >( "", qd );
        REQUIRE( title->uid() == 1 );
        CHECK( "world/1/qd/1/title/" == title->path() );

        const Annotation::ptr_t annotation = dm().emitD< Annotation >( "", qd );
        REQUIRE( annotation->uid() == 1 );
        CHECK( "world/1/qd/1/annotation/" == annotation->path() );
    }

    // second QD
    {
        const QD::ptr_t qd = world->addQDBack();
        REQUIRE( qd->uid() == 2 );
        CHECK( "world/1/qd/2/" == qd->path() );

        // first title
        const Title::ptr_t titleA = dm().emitD< Title >( "A", qd );
        REQUIRE( titleA->uid() == 2 );
        CHECK( "world/1/qd/2/title/" == titleA->path() );

        // second title
        const Title::ptr_t titleB = dm().emitD< Title >( "B", qd );
        REQUIRE( titleB->uid() == 3 );
        CHECK( "world/1/qd/2/title/" == titleB->path() );
        // \todo Where does first title go?
    }
}


#endif
