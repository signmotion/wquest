#include "WorldWindow.h"
#include "WindowManager.h"

// model
#include "models/QDModel.h"
#include "models/ValueModel.h"

#include <wquest/DM.h>


using namespace QtNodes;


namespace wquest {


WorldWindow::WorldWindow( uid_t uid, QWidget *parent ) :
    FlowWindow( dm().getFromStore( Type::World, nullptr, uid, nullptr ),
                parent )
{
    setModel();

    setMinimumSize( quantSize( false ) * 4 );
    //setMaximumSize( minimumSize() * 2 );

    updateUI();

    putNodes();
}




WorldWindow::~WorldWindow() {
    // do it in closeEvent()
    //WindowManager::instance().closeAllExcludeMain();
}




void WorldWindow::closeEvent( QCloseEvent *e ) {

    WindowManager::instance().closeAllExcludeWorld();
    FlowWindow::closeEvent( e );
}




void WorldWindow::updateUI() {

    FlowWindow::updateUI();

    const QString title = nameTitle();
    setWindowTitle( title );
}




void WorldWindow::setModel() {

    auto r = std::make_shared< DataModelRegistry >();

    // base elements
    r->registerModel< QDModel >( tr( "Base elements" ) );
    r->registerModel< ValueModel >( tr( "Base elements" ) );

    setRegistry( r );
}




void WorldWindow::putNodes() {
    // \TODO
}


} // wquest
