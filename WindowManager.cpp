#include "WindowManager.h"
#include <QApplication>
#include <QDir>
#include <QLocale>


namespace wquest {


WindowManager::WindowManager() :
    currentLanguageFile_( "" ),
    translator_( nullptr )
{
    // init language UI
    // to Russian UI by start
    // \todo fine stable Use configure for set a base language.
    setLanguageUI( "ru" );
}




WindowManager::~WindowManager() {

    closeAllExcludeWorld();
    if ( worldWindow_ ) {
        worldWindow_.reset();
    }
}




WindowManager &WindowManager::instance() {

    static WindowManager inst;
    return inst;
}




WorldWindow::ptr_t WindowManager::world( uid_t uid ) {

    ASSERT( is::correct( uid ) );

    if ( !worldWindow_ ) {
        worldWindow_.reset( new WorldWindow( uid ) );
    }
    worldWindow_->showNormal();
    worldWindow_->activateWindow();

    return worldWindow_;
}




QDWindow::ptr_t WindowManager::qd( uid_t uid, QWidget *parent ) {

    ASSERT( is::correct( uid ) );

    if ( qdWindow_ ) {
        // recreate a window with new `uid`
        qdWindow_->deleteLater();
    }

    qdWindow_.reset( new QDWindow( uid, parent ) );
    qdWindow_->showMaximized();
    qdWindow_->activateWindow();

    return qdWindow_;
}




void WindowManager::closeAllExcludeWorld() {

    if ( qdWindow_ ) {
        qdWindow_.reset();
    }
}




void
WindowManager::setLanguageUI(const QString &needLocale) {

    ASSERT(needLocale.length() == 2);

    const QString path = ":languages/";
    const QDir dir( path );
    const QStringList files = dir.entryList( QStringList( "*.qm" ) );
    index_t foundI = get::undefined< index_t >();
    for (int i = 0; i < files.size(); ++i) {
        // extracted a locale by filename
        QString locale = files[ i ];
        locale.truncate( locale.lastIndexOf( '.' ) );
        DBG_STRING( "locale", locale.toStdString() );
        if (locale == needLocale) {
            foundI = i;
            const QString language =
                    QLocale::languageToString( QLocale( locale ).language() );
            DBG_STRING( "language", language.toStdString() );
            break;
        }
    } // for i

    ASSERT(is::defined(foundI) && "Undefined locale.");

    if ( translator_ ) {
        qApp->removeTranslator( translator_ );
        delete translator_;
    }
    translator_ = new QTranslator;
    const QString &file = files[ foundI ];
    const bool r = translator_->load( path + file );
    ASSERT( r );
    qApp->installTranslator( translator_ );

    DBG_STRING( "file", file.toStdString() );
    currentLanguageFile_ = file;
}




void
WindowManager::nextLanguageUI() {

    const QString path = ":languages/";
    const QDir dir( path );
    const QStringList files = dir.entryList( QStringList( "*.qm" ) );
    index_t nextLanguage = get::undefined< index_t >();
    for (int i = 0; i < files.size(); ++i) {
        // extracted a locale by filename
        QString locale = files[ i ];
        locale.truncate( locale.lastIndexOf( '.' ) );
        DBG_STRING( "locale", locale.toStdString() );
        const QString language =
                QLocale::languageToString( QLocale( locale ).language() );
        DBG_STRING( "language", language.toStdString() );
        if (currentLanguageFile_ == files[ i ]) {
            nextLanguage = i + 1;
        }
    } // for i

    if ( is::undefined( nextLanguage ) || (nextLanguage == files.size()) ) {
        nextLanguage = 0;
    }

    if ( translator_ ) {
        qApp->removeTranslator( translator_ );
        delete translator_;
    }
    translator_ = new QTranslator;
    const QString &file = files[ nextLanguage ];
    const bool r = translator_->load( path + file );
    ASSERT( r );
    qApp->installTranslator( translator_ );

    DBG_STRING( "currentLanguageFile_", file.toStdString() );
    currentLanguageFile_ = file;
}


} // wquest
