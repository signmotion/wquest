#-------------------------------------------------
#
# Project created by QtCreator 2019-01-03T19:01:08
#
#-------------------------------------------------

QT += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = wquest
TEMPLATE = app

TRANSLATIONS += \
    resources/languages/en.ts \
    resources/languages/ru.ts

CODECFORSRC = UTF-8

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
# DEFINES += QT_BUILD_INTERNAL
DEFINES += NODE_EDITOR_STATIC

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# \todo Optimized the widget coords. Need rebuild the Qt.
#DEFINES += QT_COORD_TYPE=float

# (!) Use this string in Tools/Options.../Code Model/Manage,
# do copy and paste this for suppressed annoyning warnings
# -Weverything -Wunused -Wno-c++98-compat -Wno-c++98-compat-pedantic -Wno-unused-macros -Wno-newline-eof -Wno-exit-time-destructors -Wno-global-constructors -Wno-gnu-zero-variadic-macro-arguments -Wno-documentation -Wno-shadow -Wno-switch-enum -Wno-switch-default -Wno-missing-prototypes -Wno-used-but-marked-unused -Wno-sign-compare -Wno-switch -Wno-cast-function-type -Wno-old-style-declaration -Wno-conversion -Wno-old-style-cast -Wno-builtin-declaration-mismatch -Wno-float-equal

CONFIG += c++17

SOURCES += \
    main.cpp \
    export/wquest/common.cpp \
    export/wquest/QD.cpp \
    FlowLayout.cpp \
    Window.cpp \
    WindowManager.cpp \
    export/wquest/RepresentJSON.cpp \
    tests/TestQD.cpp \
    tests/TestDM.cpp \
    tests/TestCommon.cpp \
    export/wquest/DM.cpp \
    export/wquest/Store.cpp \
    export/wquest/StoreFile.cpp \
    export/wquest/D.cpp \
    QDWindow.cpp \
    external/nodeeditor/src/Connection.cpp \
    external/nodeeditor/src/ConnectionBlurEffect.cpp \
    external/nodeeditor/src/ConnectionGeometry.cpp \
    external/nodeeditor/src/ConnectionGraphicsObject.cpp \
    external/nodeeditor/src/ConnectionPainter.cpp \
    external/nodeeditor/src/ConnectionState.cpp \
    external/nodeeditor/src/ConnectionStyle.cpp \
    external/nodeeditor/src/DataModelRegistry.cpp \
    external/nodeeditor/src/FlowScene.cpp \
    external/nodeeditor/src/FlowView.cpp \
    external/nodeeditor/src/FlowViewStyle.cpp \
    external/nodeeditor/src/Node.cpp \
    external/nodeeditor/src/NodeConnectionInteraction.cpp \
    external/nodeeditor/src/NodeDataModel.cpp \
    external/nodeeditor/src/NodeGeometry.cpp \
    external/nodeeditor/src/NodeGraphicsObject.cpp \
    external/nodeeditor/src/NodePainter.cpp \
    external/nodeeditor/src/NodeState.cpp \
    external/nodeeditor/src/NodeStyle.cpp \
    external/nodeeditor/src/Properties.cpp \
    external/nodeeditor/src/StyleCollection.cpp \
    external/nodeeditor/examples/calculator/Converters.cpp \
    external/nodeeditor/examples/calculator/MathOperationDataModel.cpp \
    external/nodeeditor/examples/calculator/ModuloModel.cpp \
    external/nodeeditor/examples/calculator/NumberDisplayDataModel.cpp \
    external/nodeeditor/examples/calculator/NumberSourceDataModel.cpp \
    external/nodeeditor/examples/example2/TextDisplayDataModel.cpp \
    external/nodeeditor/examples/example2/TextSourceDataModel.cpp \
    external/nodeeditor/examples/styles/models.cpp \
    external/nodeeditor/examples/images/ImageLoaderModel.cpp \
    external/nodeeditor/examples/images/ImageShowModel.cpp \
    models/SceneModel.cpp \
    models/ValueModel.cpp \
    models/ChapterData.cpp \
    models/ApronData.cpp \
    models/ProvisosData.cpp \
    models/ProvisosModel.cpp \
    external/nodeeditor/examples/dynamic_node/RiverListData.cpp \
    external/nodeeditor/examples/dynamic_node/RiverListModel.cpp \
    external/nodeeditor/examples/dynamic_node/RiverModel.cpp \
    models/ProvisoWhenDistance2Model.cpp \
    tests/TestProviso.cpp \
    export/wquest/common.cpp \
    export/wquest/D.cpp \
    export/wquest/DM.cpp \
    export/wquest/Morpher.cpp \
    export/wquest/MorpherCompare.cpp \
    export/wquest/Proviso.cpp \
    export/wquest/ProvisoWhenDistance2.cpp \
    export/wquest/QD.cpp \
    export/wquest/RepresentJSON.cpp \
    export/wquest/Store.cpp \
    export/wquest/StoreFile.cpp \
    models/ApronData.cpp \
    models/ChapterData.cpp \
    models/ProvisoData.cpp \
    models/ProvisosData.cpp \
    models/ProvisosModel.cpp \
    models/ProvisoWhenDistance2Model.cpp \
    models/SceneModel.cpp \
    models/ValueModel.cpp \
    export/wquest/MorpherDistance.cpp \
    export/external/fmt/src/format.cc \
    export/external/fmt/src/posix.cc \
    export/wquest/MorpherInstance.cpp \
    export/wquest/Name.cpp \
    models/ApronData.cpp \
    models/ChapterData.cpp \
    models/ProvisoData.cpp \
    models/ProvisoModel.cpp \
    models/ProvisosData.cpp \
    models/ProvisosModel.cpp \
    models/ProvisoWhenDistance2Model.cpp \
    models/SceneModel.cpp \
    models/ValueModel.cpp \
    models/Model.cpp \
    external/nodeeditor/examples/connection_colors/NaiveDataModel.cpp \
    models/ApronData.cpp \
    models/ChapterData.cpp \
    models/Model.cpp \
    models/ProvisoData.cpp \
    models/ProvisoModel.cpp \
    models/ProvisosData.cpp \
    models/ProvisosModel.cpp \
    models/ProvisoWhenDistance2Model.cpp \
    models/SceneModel.cpp \
    models/TowerData.cpp \
    models/TowerModel.cpp \
    models/ValueModel.cpp \
    export/wquest/Tower.cpp \
    export/wquest/Apron.cpp \
    export/wquest/Chapter.cpp \
    export/wquest/common.cpp \
    export/wquest/D.cpp \
    export/wquest/DM.cpp \
    export/wquest/Impact.cpp \
    export/wquest/Morpher.cpp \
    export/wquest/MorpherCompare.cpp \
    export/wquest/MorpherDistance.cpp \
    export/wquest/MorpherInstance.cpp \
    export/wquest/Name.cpp \
    export/wquest/Proviso.cpp \
    export/wquest/ProvisoWhenDistance2.cpp \
    export/wquest/QD.cpp \
    export/wquest/RepresentJSON.cpp \
    export/wquest/Scene.cpp \
    export/wquest/Store.cpp \
    export/wquest/StoreFile.cpp \
    export/wquest/Tower.cpp \
    models/ApronData.cpp \
    models/ChapterData.cpp \
    models/Model.cpp \
    models/ProvisoData.cpp \
    models/ProvisoModel.cpp \
    models/ProvisosData.cpp \
    models/ProvisosModel.cpp \
    models/ProvisoWhenDistance2Model.cpp \
    models/SceneData.cpp \
    models/SceneModel.cpp \
    models/TowerData.cpp \
    models/TowerModel.cpp \
    models/ValueModel.cpp \
    FlowLayout.cpp \
    main.cpp \
    QDWindow.cpp \
    Window.cpp \
    WindowManager.cpp \
    models/ApronData.cpp \
    models/ChapterData.cpp \
    models/Model.cpp \
    models/ProvisoData.cpp \
    models/ProvisoModel.cpp \
    models/ProvisosData.cpp \
    models/ProvisosModel.cpp \
    models/ProvisoWhenDistance2Model.cpp \
    models/SceneData.cpp \
    models/SceneModel.cpp \
    models/ScenesData.cpp \
    models/ScenesModel.cpp \
    models/TowerData.cpp \
    models/TowerModel.cpp \
    models/ValueModel.cpp \
    FlowLayout.cpp \
    FlowWindow.cpp \
    QDWindow.cpp \
    models/ApronData.cpp \
    models/ChapterData.cpp \
    models/Model.cpp \
    models/ProvisoData.cpp \
    models/ProvisoModel.cpp \
    models/ProvisosData.cpp \
    models/ProvisosModel.cpp \
    models/ProvisoWhenDistance2Model.cpp \
    models/QDData.cpp \
    models/QDModel.cpp \
    models/SceneData.cpp \
    models/SceneModel.cpp \
    models/ScenesData.cpp \
    models/ScenesModel.cpp \
    models/TowerData.cpp \
    models/TowerModel.cpp \
    models/ValueModel.cpp \
    export/wquest/Annotation.cpp \
    export/wquest/Title.cpp \
    models/AnnotationData.cpp \
    models/TitleData.cpp \
    export/wquest/World.cpp \
    WorldWindow.cpp \
    tests/TestD.cpp \
    export/wquest/Type.cpp \
    tests/TestLazyList.cpp \
    export/wquest/RapidJSON.cpp \
    export/wquest/RapidJSON.cpp

HEADERS += \
    FlowLayout.h \
    export/wquest/Cast.h \
    export/wquest/common.h \
    export/wquest/dbgMacros.h \
    export/wquest/CartesianCoord.h \
    export/wquest/CartesianSize.h \
    export/wquest/QD.h \
    Window.h \
    WindowManager.h \
    export/wquest/RepresentJSON.h \
    export/wquest/Represent.h \
    export/wquest/D.h \
    export/wquest/DM.h \
    export/wquest/Store.h \
    export/wquest/Store.h \
    export/wquest/StoreFile.h \
    export/wquest/Type.h \
    QDWindow.h \
    external/nodeeditor/include/nodes/Connection \
    external/nodeeditor/include/nodes/ConnectionStyle \
    external/nodeeditor/include/nodes/DataModelRegistry \
    external/nodeeditor/include/nodes/FlowScene \
    external/nodeeditor/include/nodes/FlowView \
    external/nodeeditor/include/nodes/FlowViewStyle \
    external/nodeeditor/include/nodes/Node \
    external/nodeeditor/include/nodes/NodeData \
    external/nodeeditor/include/nodes/NodeDataModel \
    external/nodeeditor/include/nodes/NodeGeometry \
    external/nodeeditor/include/nodes/NodePainterDelegate \
    external/nodeeditor/include/nodes/NodeState \
    external/nodeeditor/include/nodes/NodeStyle \
    external/nodeeditor/include/nodes/TypeConverter \
    external/nodeeditor/include/nodes/internal/Compiler.hpp \
    external/nodeeditor/include/nodes/internal/Connection.hpp \
    external/nodeeditor/include/nodes/internal/ConnectionGeometry.hpp \
    external/nodeeditor/include/nodes/internal/ConnectionGraphicsObject.hpp \
    external/nodeeditor/include/nodes/internal/ConnectionState.hpp \
    external/nodeeditor/include/nodes/internal/ConnectionStyle.hpp \
    external/nodeeditor/include/nodes/internal/DataModelRegistry.hpp \
    external/nodeeditor/include/nodes/internal/Export.hpp \
    external/nodeeditor/include/nodes/internal/FlowScene.hpp \
    external/nodeeditor/include/nodes/internal/FlowView.hpp \
    external/nodeeditor/include/nodes/internal/FlowViewStyle.hpp \
    external/nodeeditor/include/nodes/internal/memory.hpp \
    external/nodeeditor/include/nodes/internal/Node.hpp \
    external/nodeeditor/include/nodes/internal/NodeData.hpp \
    external/nodeeditor/include/nodes/internal/NodeDataModel.hpp \
    external/nodeeditor/include/nodes/internal/NodeGeometry.hpp \
    external/nodeeditor/include/nodes/internal/NodeGraphicsObject.hpp \
    external/nodeeditor/include/nodes/internal/NodePainterDelegate.hpp \
    external/nodeeditor/include/nodes/internal/NodeState.hpp \
    external/nodeeditor/include/nodes/internal/NodeStyle.hpp \
    external/nodeeditor/include/nodes/internal/OperatingSystem.hpp \
    external/nodeeditor/include/nodes/internal/PortType.hpp \
    external/nodeeditor/include/nodes/internal/QStringStdHash.hpp \
    external/nodeeditor/include/nodes/internal/QUuidStdHash.hpp \
    external/nodeeditor/include/nodes/internal/Serializable.hpp \
    external/nodeeditor/include/nodes/internal/Style.hpp \
    external/nodeeditor/include/nodes/internal/TypeConverter.hpp \
    external/nodeeditor/src/ConnectionBlurEffect.hpp \
    external/nodeeditor/src/ConnectionPainter.hpp \
    external/nodeeditor/src/NodeConnectionInteraction.hpp \
    external/nodeeditor/src/NodePainter.hpp \
    external/nodeeditor/src/Properties.hpp \
    external/nodeeditor/src/StyleCollection.hpp \
    external/nodeeditor/examples/calculator/AdditionModel.hpp \
    external/nodeeditor/examples/calculator/Converters.hpp \
    external/nodeeditor/examples/calculator/DecimalData.hpp \
    external/nodeeditor/examples/calculator/DivisionModel.hpp \
    external/nodeeditor/examples/calculator/IntegerData.hpp \
    external/nodeeditor/examples/calculator/MathOperationDataModel.hpp \
    external/nodeeditor/examples/calculator/ModuloModel.hpp \
    external/nodeeditor/examples/calculator/MultiplicationModel.hpp \
    external/nodeeditor/examples/calculator/NumberDisplayDataModel.hpp \
    external/nodeeditor/examples/calculator/NumberSourceDataModel.hpp \
    external/nodeeditor/examples/calculator/SubtractionModel.hpp \
    external/nodeeditor/examples/example2/TextData.hpp \
    external/nodeeditor/examples/example2/TextDisplayDataModel.hpp \
    external/nodeeditor/examples/example2/TextSourceDataModel.hpp \
    external/nodeeditor/examples/styles/models.hpp \
    external/nodeeditor/examples/images/ImageLoaderModel.hpp \
    external/nodeeditor/examples/images/ImageShowModel.hpp \
    external/nodeeditor/examples/images/PixmapData.hpp \
    models/SceneModel.h \
    models/ValueData.h \
    models/ValueModel.h \
    models/ChapterData.h \
    models/ApronData.h \
    models/ProvisosData.h \
    models/ProvisosModel.h \
    external/nodeeditor/examples/dynamic_node/RiverData.h \
    external/nodeeditor/examples/dynamic_node/RiverListData.h \
    external/nodeeditor/examples/dynamic_node/RiverListModel.h \
    external/nodeeditor/examples/dynamic_node/RiverModel.h \
    models/ProvisoWhenDistance2Model.h \
    export/wquest/Proviso.h \
    export/wquest/CartesianCoord.h \
    export/wquest/CartesianSize.h \
    export/wquest/Cast.h \
    export/wquest/common.h \
    export/wquest/D.h \
    export/wquest/dbgMacros.h \
    export/wquest/DM.h \
    export/wquest/Morpher.h \
    export/wquest/MorpherCompare.h \
    export/wquest/Proviso.h \
    export/wquest/ProvisoWhenDistance2.h \
    export/wquest/QD.h \
    export/wquest/Represent.h \
    export/wquest/RepresentJSON.h \
    export/wquest/Store.h \
    export/wquest/StoreFile.h \
    export/wquest/Type.h \
    export/wquest/TypeCompare.h \
    models/ApronData.h \
    models/ChapterData.h \
    models/ProvisoData.h \
    models/ProvisosData.h \
    models/ProvisosModel.h \
    models/ProvisoWhenDistance2Model.h \
    models/SceneModel.h \
    models/ValueData.h \
    models/ValueModel.h \
    export/wquest/MorpherDistance.h \
    export/external/fmt/include/fmt/chrono.h \
    export/external/fmt/include/fmt/color.h \
    export/external/fmt/include/fmt/core.h \
    export/external/fmt/include/fmt/format.h \
    export/external/fmt/include/fmt/format-inl.h \
    export/external/fmt/include/fmt/locale.h \
    export/external/fmt/include/fmt/ostream.h \
    export/external/fmt/include/fmt/posix.h \
    export/external/fmt/include/fmt/prepare.h \
    export/external/fmt/include/fmt/printf.h \
    export/external/fmt/include/fmt/ranges.h \
    export/external/fmt/include/fmt/time.h \
    export/wquest/MorpherInstance.h \
    export/wquest/Name.h \
    models/ApronData.h \
    models/ChapterData.h \
    models/ProvisoData.h \
    models/ProvisoModel.h \
    models/ProvisosData.h \
    models/ProvisosModel.h \
    models/ProvisoWhenDistance2Model.h \
    models/SceneModel.h \
    models/ValueData.h \
    models/ValueModel.h \
    models/Model.h \
    external/nodeeditor/examples/connection_colors/NaiveDataModel.h \
    models/ApronData.h \
    models/ChapterData.h \
    models/Model.h \
    models/ProvisoData.h \
    models/ProvisoModel.h \
    models/ProvisosData.h \
    models/ProvisosModel.h \
    models/ProvisoWhenDistance2Model.h \
    models/SceneModel.h \
    models/TowerData.h \
    models/TowerModel.h \
    models/ValueData.h \
    models/ValueModel.h \
    export/wquest/Tower.h \
    export/wquest/Apron.h \
    export/wquest/CartesianCoord.h \
    export/wquest/CartesianSize.h \
    export/wquest/Cast.h \
    export/wquest/Chapter.h \
    export/wquest/common.h \
    export/wquest/D.h \
    export/wquest/dbgMacros.h \
    export/wquest/DM.h \
    export/wquest/Impact.h \
    export/wquest/Morpher.h \
    export/wquest/MorpherCompare.h \
    export/wquest/MorpherDistance.h \
    export/wquest/MorpherInstance.h \
    export/wquest/Name.h \
    export/wquest/Proviso.h \
    export/wquest/ProvisoWhenDistance2.h \
    export/wquest/QD.h \
    export/wquest/Represent.h \
    export/wquest/RepresentJSON.h \
    export/wquest/Scene.h \
    export/wquest/Store.h \
    export/wquest/StoreFile.h \
    export/wquest/Tower.h \
    export/wquest/Type.h \
    export/wquest/TypeCompare.h \
    models/ApronData.h \
    models/ChapterData.h \
    models/Model.h \
    models/ProvisoData.h \
    models/ProvisoModel.h \
    models/ProvisosData.h \
    models/ProvisosModel.h \
    models/ProvisoWhenDistance2Model.h \
    models/SceneData.h \
    models/SceneModel.h \
    models/TowerData.h \
    models/TowerModel.h \
    models/ValueData.h \
    models/ValueModel.h \
    FlowLayout.h \
    QDWindow.h \
    Window.h \
    WindowManager.h \
    models/ApronData.h \
    models/ChapterData.h \
    models/Model.h \
    models/ProvisoData.h \
    models/ProvisoModel.h \
    models/ProvisosData.h \
    models/ProvisosModel.h \
    models/ProvisoWhenDistance2Model.h \
    models/SceneData.h \
    models/SceneModel.h \
    models/ScenesData.h \
    models/ScenesModel.h \
    models/TowerData.h \
    models/TowerModel.h \
    models/ValueData.h \
    models/ValueModel.h \
    FlowLayout.h \
    FlowWindow.h \
    models/ApronData.h \
    models/ChapterData.h \
    models/Model.h \
    models/ProvisoData.h \
    models/ProvisoModel.h \
    models/ProvisosData.h \
    models/ProvisosModel.h \
    models/ProvisoWhenDistance2Model.h \
    models/QDData.h \
    models/QDModel.h \
    models/SceneData.h \
    models/SceneModel.h \
    models/ScenesData.h \
    models/ScenesModel.h \
    models/TowerData.h \
    models/TowerModel.h \
    models/ValueData.h \
    models/ValueModel.h \
    export/wquest/Annotation.h \
    export/wquest/Title.h \
    models/AnnotationData.h \
    models/TitleData.h \
    models/Data.h \
    export/wquest/World.h \
    WorldWindow.h \
    export/wquest/LazyList.h \
    tests/DefineTestStore.h \
    export/wquest/RapidJSON.h \
    export/wquest/JSON_T.h \
    export/wquest/JSON.h \
    export/wquest/JSON_T.h \
    export/wquest/RapidJSON.h \
    export/wquest/buildMacros.h \
    export/wquest/buildMacros.h

INCLUDEPATH += $$PWD/export
INCLUDEPATH += $$PWD/export/external
INCLUDEPATH += $$PWD/export/external/fmt/include
INCLUDEPATH += $$PWD/external
INCLUDEPATH += $$PWD/external/nodeeditor/include
INCLUDEPATH += $$PWD/external/nodeeditor/include/nodes/internal


DISTFILES += \
    resources/DefaultStyle.json \
    resources/convert.png \
    external/nodeeditor/examples/calculator/CMakeLists.txt

RESOURCES += \
    resources/resources.qrc


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

FORMS += \
    MainWindow.ui
