#include "FlowWindow.h"
#include "WindowManager.h"
#include "models/Model.h"
#include "models/QDModel.h"

#include <QVBoxLayout>

#include <nodes/FlowView>
#include <nodes/NodeData>
#include <nodes/DataModelRegistry>
#include <nodes/ConnectionStyle>
#include <nodes/FlowViewStyle>
#include <nodes/Node>
#include <nodes/NodeDataModel>

#include <wquest/DM.h>


using namespace QtNodes;


namespace wquest {


FlowWindow::FlowWindow( const D::ptr_t data, QWidget *parent ) :
    Window( data, parent ),
    flowScene_( new FlowScene )
{
    QVBoxLayout *l = new QVBoxLayout( this );
    FlowView *flowView = new FlowView( flowScene_.get() );
    l->addWidget( flowView );
    l->setContentsMargins( 0, 0, 0, 0 );
    l->setSpacing( 0 );


    FlowScene *const scenePtr = flowScene_.get();
    connect( scenePtr, &FlowScene::nodePlaced, [ scenePtr ] (
        Node &placed
    ) {
        NodeDataModel *model = placed.nodeDataModel();
        const auto m = dynamic_cast< Model * >( model );
        m->addInputWidgets( scenePtr, placed );
        m->addOutputWidgets( scenePtr, placed );
        // save to longterm storage
        dm().flush();
    } );


    connect( scenePtr, &FlowScene::nodeMoved, [] (
        Node &moved, const QPointF &
    ) {
        NodeDataModel *model = moved.nodeDataModel();
        const auto m = dynamic_cast< Model * >( model );
        m->layoutInputWidgets( moved );
        //m->layoutOutputWidgets( moved );
    } );


    connect( scenePtr, &FlowScene::nodeDoubleClicked, [ scenePtr, this ] (
        Node &node
    ) {
        NodeDataModel *model = node.nodeDataModel();
        const auto m = dynamic_cast< Model * >( model );
        open( m );
    } );


    updateUI();
}




FlowWindow::~FlowWindow() {
}




void FlowWindow::changeEvent( QEvent *e ) {

    if (e->type() == QEvent::LanguageChange) {
        updateUI();
    }
}




void FlowWindow::updateUI() {

    const QString title = "";
    setWindowTitle( title );

    flowScene_->iterateOverNodes( [] ( Node *node ) {
        node->nodeGraphicsObject().setGeometryChanged();
        node->nodeGeometry().recalculateSize();
        node->nodeGraphicsObject().update();
    } );
}




void FlowWindow::setRegistry(
        std::shared_ptr< QtNodes::DataModelRegistry > m
) {
    ASSERT( m );
    flowScene_->setRegistry( m );
}




void FlowWindow::open( Model *m ) {

    ASSERT( m );

    QDModel *model = dynamic_cast< QDModel * >( m );
    if ( model ) {
        open( model );
        return;
    }

    // ignore a not recognized model
    DBG_STRING( "Not recognized model", m->name().toStdString() );
}




void FlowWindow::open( QDModel *m ) {

    ASSERT( m );

    DBG_STRING( "FlowWindow::open() model", m->name().toStdString() );
    WindowManager &wm = WindowManager::instance();
    QD::ptr_t qd = m->data()->data< QD >();
    wm.qd( qd->uid(), parentWidget() );
}


} // wquest
