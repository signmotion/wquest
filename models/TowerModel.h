#pragma once

#include "Model.h"
#include "TowerData.h"


namespace wquest {


class TowerModel : public Model {
    /// @see portCaption()
    static constexpr int maxLengthPortCaption = 12;


public:
    TowerModel();

    virtual ~TowerModel() override;


public:
    static QString sname();

    QString caption() const override;

    QString name() const override {
        return sname();
    }

    unsigned int nPorts( PortType ) const override;

    QString portCaption( PortType, PortIndex ) const override;

    NodeDataType dataType( PortType, PortIndex ) const override;

    std::shared_ptr< NodeData > outData( PortIndex ) override;

    void setInData( std::shared_ptr< NodeData >, PortIndex ) override;

    void addInputWidgets(
            FlowScene *,
            Node &node ) const override;

private:
    std::shared_ptr< TowerData > data_;
};


} // wquest
