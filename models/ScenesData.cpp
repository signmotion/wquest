#include "ScenesData.h"


namespace wquest {


ScenesData::ScenesData() {
}




Scene::ptr_t
ScenesData::data( index_t i ) const {
    return *std::next( data_.begin(), i );
}




void
ScenesData::update( index_t i, const Scene::ptr_t newScene ) {

    ASSERT( i >= 0 );
    ASSERT( i < data_.size() );
    //ASSERT( newScene ); - we can reset a scene by `i`

    const auto it = std::next( data_.begin(), i );
    *it = newScene;
}




void
ScenesData::addBack( const Scene::ptr_t scene ) {

    //ASSERT( scene ); - we can init like `nullptr`
    data_.emplace_back( scene );
}




void ScenesData::remove( index_t i ) {

    ASSERT( i >= 0 );
    ASSERT( i < data_.size() );

    const auto it = std::next( data_.cbegin(), i );
    data_.erase( it );
}


} // wquest
