#include "TitleData.h"
#include <wquest/DM.h>


namespace wquest {


TitleData::TitleData() :
    Data( dm().emitD< Title >( "", nullptr ) )
{
}


} // wquest
