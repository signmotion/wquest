#include "TowerModel.h"
#include "TowerData.h"
#include "ScenesData.h"
#include "ScenesModel.h"
#include <wquest/DM.h>


namespace wquest {


TowerModel::TowerModel() :
    Model( std::make_shared< TowerData >( dm().emitD< Tower >( nullptr ) ) )
{
}




TowerModel::~TowerModel() {
}




QString
TowerModel::sname() {
    return "Tower";
}




QString
TowerModel::caption() const {
    return TowerData::sdt().name;
}




unsigned int
TowerModel::nPorts( PortType portType ) const {

    switch ( portType ) {
        case PortType::In:
            // Tower + Scenes
            return 1 + 1;

        case PortType::Out:
            return 1;
    }

    return 0;
}




QString
TowerModel::portCaption( PortType portType, PortIndex portIndex ) const {

    if ( portType == PortType::In ) {
        if ( portIndex == 0 ) {
            // from other tower
            return TowerData::sdt().name;
        }
        if ( portIndex == 1 ) {
            // scenes
            return ScenesData::sdt().name;
        }
    }

    return "";
}




NodeDataType
TowerModel::dataType( PortType portType, PortIndex portIndex ) const {

    switch ( portType ) {
        case PortType::In:
            if ( portIndex == 0 ) {
                // from other tower
                return TowerData::sdt();
            }
            if ( portIndex == 1 ) {
                // scenes
                return ScenesData::sdt();
            }
            break;

        case PortType::Out:
            return TowerData::sdt();
    }

    return TowerData::sdt();
}




std::shared_ptr< NodeData >
TowerModel::outData( PortIndex ) {
    return data_;
}




void
TowerModel::setInData( std::shared_ptr< NodeData > nd, PortIndex portIndex ) {

    const std::string s = nd ? nd->type().id.toStdString() : "nullptr";
    DBG_STRING( "TowerModel::setInData() ", s );

    // tower
    Tower::ptr_t scene = data_->data< Tower >();
    if ( portIndex == 0 ) {
        const auto v = std::dynamic_pointer_cast< TowerData >( nd );
        const Tower::ptr_t nt = v ? v->data< Tower >() : nullptr;
        data_->data< Tower >()->chainFrom( nt );
        //nt->chainTo( data_->data() ); - Save one-direction.
        return;
    }

    // scenes
    if (portIndex == 1) {
        const auto v = std::dynamic_pointer_cast< ScenesData >( nd );
        // \todo setOrPushScene();
    }

    //emit dataUpdated( portIndex );
}




void
TowerModel::addInputWidgets(
        FlowScene *fs,
        Node &node
) const {

    ASSERT( fs );

    // scenes
    addWidget( fs, &node, ScenesModel::sname(), 0, 1 );
}


} // wquest
