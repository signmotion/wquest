#include "ProvisoWhenDistance2Model.h"
#include "ValueData.h"
#include "ValueModel.h"
#include "ProvisosModel.h"
#include <wquest/DM.h>
#include <wquest/ProvisoWhenDistance2.h>
#include <nodes/Node>
#include <fmt/core.h>


namespace wquest {


ProvisoWhenDistance2Model::ProvisoWhenDistance2Model() :
    ProvisoModel( dm().emitD< ProvisoWhenDistance2 >( nullptr ) )
{
    validate();
}




ProvisoWhenDistance2Model::~ProvisoWhenDistance2Model() {
}




QString
ProvisoWhenDistance2Model::sname() {

    // copy from ProvisoWhenDistance2::declare()
    // for translate on other languages
    return tr( "When distance between [instance A] and [instance B]"
               " is [compare] than [distance] m" );
}




QString
ProvisoWhenDistance2Model::caption() const {

    using namespace fmt;

    const Proviso::ptr_t d = data()->data< Proviso >();
    const auto p = std::dynamic_pointer_cast< ProvisoWhenDistance2 >( d );
    if ( !p ) {
        return sname();
    }

    // copy from ProvisoWhenDistance2::caption()
    const std::string instanceA =
            Morpher::stringOrBraces( p->morpherInstanceA() );
    const std::string instanceB =
            Morpher::stringOrBraces( p->morpherInstanceB() );
    const std::string compare =
            Morpher::stringOrBraces( p->morpherCompare() );
    const std::string distance =
            Morpher::stringOrBraces( p->morpherDistance() );

    const std::string s = make::emptyBraces( sname().toStdString() );
    const std::string r = format(
        s,
        instanceA, instanceB, compare, distance );

    return r.c_str();
}




unsigned int
ProvisoWhenDistance2Model::nPorts( PortType portType ) const {

    switch ( portType ) {
        case PortType::In:
            return 4;

        case PortType::Out:
            return 1;
    }

    return 0;
}




QString
ProvisoWhenDistance2Model::portCaption( PortType portType, PortIndex portIndex ) const {

    if (portType == PortType::Out) {
        return dataType( portType, portIndex ).name;
    }

    switch ( portIndex ) {
        case 0:
            return tr( "Instance A" );
        case 1:
            return tr( "Instance B" );
        case 2:
            return tr( "Compare" );
        case 3:
            return tr( "Distance" );
    }

    return dataType( portType, portIndex ).name;
}




NodeDataType
ProvisoWhenDistance2Model::dataType( PortType portType, PortIndex portIndex ) const {

    switch ( portType ) {
        case PortType::In :
            switch ( portIndex ) {
                case 0:
                    return ValueData::sdt();
                case 1:
                    return ValueData::sdt();
                case 2:
                    return ValueData::sdt();
                case 3:
                    return ValueData::sdt();
            }
            break;

        case PortType::Out :
            return ProvisoData::sdt();
    }

    //ASSERT( false && "Unrecognized port or index." );
    return NodeDataType();
}




void
ProvisoWhenDistance2Model::setInData( std::shared_ptr< NodeData > nd, PortIndex portIndex ) {

    const auto v = std::dynamic_pointer_cast< ValueData >( nd );

    Proviso::ptr_t d = data()->data< Proviso >();
    auto p = std::dynamic_pointer_cast< ProvisoWhenDistance2 >( d );
    if ( !p ) {
        p = dm().emitD< ProvisoWhenDistance2 >( nullptr );
    }
    ASSERT( p );

    if (portIndex == 0) {
        // instance A
        const std::string instanceA =
                v ? capitalizeFirstCopy( v->data() ).toStdString() : "";
        const auto m = std::make_shared< MorpherInstance >( instanceA );
        p->morpherInstanceA( m );
    }
    else if (portIndex == 1) {
        // instance B
        const std::string instanceB =
                v ? capitalizeFirstCopy( v->data() ).toStdString() : "";
        const auto m = std::make_shared< MorpherInstance >( instanceB );
        p->morpherInstanceB( m );
    }
    else if (portIndex == 2) {
        // compare
        const std::string typeCompare = v ? v->data().toStdString() : "";
        const auto m = std::make_shared< MorpherCompare >( typeCompare );
        p->morpherCompare( m );
    }
    else if (portIndex == 3) {
        // distance
        const QString dist = v ? v->data() : "";
        bool ok = false;
        float distance = dist.toFloat( &ok );
        if ( !ok ) {
            distance = get::undefined< float >();
        }
        const auto m = std::make_shared< MorpherDistance >( distance );
        p->morpherDistance( m );
    }

    validate();

    //emit dataUpdated( portIndex );
}




void
ProvisoWhenDistance2Model::addInputWidgets(
        FlowScene *fs,
        Node &node
) const {

    ASSERT( fs );

    // instance A
    addWidget( fs, &node, ValueModel::sname(), 0, 0 );
    // instance B
    addWidget( fs, &node, ValueModel::sname(), 0, 1 );
    // Compare
    addWidget( fs, &node, ValueModel::sname(), 0, 2 );
    // Distance
    addWidget( fs, &node, ValueModel::sname(), 0, 3 );
}




void
ProvisoWhenDistance2Model::addOutputWidgets(
        FlowScene */*scene*/,
        Node &/*node*/
) const {

    /* - Don't add it because a node `Provisos` waits many nodes.
    ASSERT( scene );

    auto &registry = scene->registry();

    const auto create = [ &registry, scene, &node, this ] (
        const QString &nameRecipient,
        PortIndex portIndexRecipient,
        PortIndex portIndexOut
    ) {
        auto typeRecipient = registry.create( nameRecipient );
        ASSERT( typeRecipient );
        auto& nodeRecipient = scene->createNode( std::move( typeRecipient ) );
        layoutOutputWidget( nodeRecipient, node, portIndexOut );
        const std::shared_ptr< Connection > connection =
            scene->createConnection(
                    nodeRecipient, portIndexRecipient,
                    node, portIndexOut );
    };

    // Provisos
    create( ProvisosModel::sname(), 0, 0 );
    */
}




void
ProvisoWhenDistance2Model::validate() {

    const Proviso::ptr_t d = data()->data< Proviso >();
    const auto p = std::dynamic_pointer_cast< ProvisoWhenDistance2 >( d );
    ASSERT( p );

    validationState_ = NodeValidationState::Valid;
    validationMessage_ = "";

#if 0
    // test
    const bool instanceA =
            p->morpherInstanceA() && p->morpherInstanceA()->valid();
    const bool instanceB =
            p->morpherInstanceB() && p->morpherInstanceB()->valid();
    const bool compare =
            p->morpherCompare() && p->morpherCompare()->valid();
    const bool distance =
            p->morpherDistance() && p->morpherDistance()->valid();

    if ( !instanceA ) {
        validationMessage_ += " -instanceA";
    }
    if ( !instanceB ) {
        validationMessage_ += " -instanceB";
    }
    if ( !compare ) {
        validationMessage_ += " -compare";
    }
    if ( !distance ) {
        validationMessage_ += " -distance";
    }
#endif

    if ( p->allValid() ) {
        validationState_ = NodeValidationState::Valid;
        validationMessage_ = tr( "Good!" );
        return;
    }

    if ( p->allInvalid() ) {
        validationState_ = NodeValidationState::Warning;
        // \todo fine Friendly all messages: bro, man, girl...
        //       For example: "Need something, bro..."
        validationMessage_ += tr( " Need something..." );
        return;
    }

    if ( p->someInvalid() ) {
        validationState_ = NodeValidationState::Warning;
        validationMessage_ += tr( " Need something more..." );
        return;
    }
}


} // wquest
