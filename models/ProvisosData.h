#pragma once

#include <wquest/Proviso.h>
#include <wquest/dbgMacros.h>
#include <nodes/NodeData>
#include <QCoreApplication>


using QtNodes::NodeData;
using QtNodes::NodeDataType;


namespace wquest {


class ProvisosData : public NodeData {
    Q_DECLARE_TR_FUNCTIONS( ProvisosData )


public:
    ProvisosData();

    NodeDataType type() const override {
        return sdt();
    }

    static NodeDataType sdt() {
        const QString name = tr( "Provisos" );
        return NodeDataType{ "Provisos", name };
    }

    const Proviso::ptrs_t &data() const {
        return data_;
    }

    Proviso::ptr_t data( index_t ) const;

    void update( index_t, const Proviso::ptr_t newProviso );

    void addBack( const Proviso::ptr_t );

    void remove( index_t );


private:
    Proviso::ptrs_t data_;
};


} // wquest
