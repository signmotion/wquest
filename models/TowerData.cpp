#include "TowerData.h"
#include <wquest/DM.h>


namespace wquest {


TowerData::TowerData( const Tower::ptr_t tower ) :
    Data( tower )
{
}




Scene::ptr_t
TowerData::dataScene( index_t i ) const {
    return Data::data< Tower >()->scene( i );
}




void
TowerData::updateScene( index_t i, const Scene::ptr_t newScene ) {

    ASSERT( i >= 0 );
    //ASSERT( newPScene ); - we can reset a scene by `i`

    Data::data< Tower >()->removeScene( i );
    Data::data< Tower >()->addScene( i );
}




void
TowerData::addSceneBack( const Scene::ptr_t scene ) {

    //ASSERT( scene ); - we can init like `nullptr`
    Data::data< Tower >()->addSceneBack( scene );
}




void TowerData::removeScene( index_t i ) {

    ASSERT( i >= 0 );

    Data::data< Tower >()->removeScene( i );
}


} // wquest
