#include "ProvisosData.h"


namespace wquest {


ProvisosData::ProvisosData() {
}




Proviso::ptr_t
ProvisosData::data( index_t i ) const {
    return *std::next( data_.begin(), i );
}




void
ProvisosData::update( index_t i, const Proviso::ptr_t newProviso ) {

    ASSERT( i >= 0 );
    ASSERT( i < data_.size() );
    //ASSERT( newProviso ); - we can reset a proviso by `i`

    const auto it = std::next( data_.begin(), i );
    *it = newProviso;
}




void
ProvisosData::addBack( const Proviso::ptr_t proviso ) {

    //ASSERT( proviso ); - we can init like `nullptr`
    data_.emplace_back( proviso );
}




void ProvisosData::remove( index_t i ) {

    ASSERT( i >= 0 );
    ASSERT( i < data_.size() );

    const auto it = std::next( data_.cbegin(), i );
    data_.erase( it );
}


} // wquest
