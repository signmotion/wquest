#include "ApronData.h"
#include <wquest/DM.h>


namespace wquest {


ApronData::ApronData() :
    Data( dm().emitD< Apron >( "", nullptr ) )
{
}


} // wquest
