#include "AnnotationData.h"
#include <wquest/DM.h>


namespace wquest {


AnnotationData::AnnotationData() :
    Data( dm().emitD< Annotation >( "", nullptr ) )
{
}


} // wquest
