#pragma once

#include <nodes/NodeData>


using QtNodes::NodeData;
using QtNodes::NodeDataType;


namespace wquest {


class ValueData : public NodeData {
public:
    ValueData() : data_( "" ) {
    }

    explicit ValueData( const QString &s ) : data_( s ) {
    }

    NodeDataType type() const override {
        return sdt();
    }

    static NodeDataType sdt() {
        return NodeDataType{ "Value", "" };
    }

    QString data() const {
        return data_;
    }


private:
    QString data_;
};


} // wquest
