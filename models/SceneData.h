#pragma once

#include "Data.h"
#include <wquest/Scene.h>
#include <QCoreApplication>


using QtNodes::NodeData;
using QtNodes::NodeDataType;


namespace wquest {


class SceneData : public Data {
    Q_DECLARE_TR_FUNCTIONS( SceneData )


public:
    explicit SceneData( const Scene::ptr_t );

    NodeDataType type() const override {
        return sdt();
    }

    static NodeDataType sdt() {
        const QString name = tr( "Scene" );
        return NodeDataType{ "Scene", name };
    }
};


} // wquest
