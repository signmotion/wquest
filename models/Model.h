#pragma once

#include "Data.h"
#include <nodes/NodeDataModel>
#include <nodes/Node>
#include <nodes/FlowScene>
#include <nodes/Connection>
#include <memory>
#include <QCoreApplication>


using QtNodes::Connection;
using QtNodes::Node;
using QtNodes::NodeData;
using QtNodes::NodeDataModel;
using QtNodes::NodeDataType;
using QtNodes::PortIndex;
using QtNodes::PortType;
using QtNodes::NodeValidationState;
using QtNodes::FlowScene;


namespace wquest {


/**
 * @brief Abstract class for all models.
 */
class Model : public NodeDataModel {
    Q_DECLARE_TR_FUNCTIONS( Model )


protected:
    Model();

    explicit Model( const std::shared_ptr< Data > );


public:
    virtual ~Model() override;

    bool captionVisible() const override {
        return true;
    }

    bool portCaptionVisible( PortType, PortIndex ) const override {
        return true;
    }

    ConnectionPolicy portOutConnectionPolicy( PortIndex ) const override {
        return ConnectionPolicy::One;
    }

    QWidget *embeddedWidget() override;

    NodeValidationState validationState() const override {
        return validationState_;
    }

    QString validationMessage() const override {
        return validationMessage_;
    }

    template< class T >
    std::shared_ptr< T > data() const {
        return std::dynamic_pointer_cast< T >( data_ );
    }

    const std::shared_ptr< Data > &data() const {
        return data_;
    }

    /**
     * @brief
     * Create the needed input widgets on the scene and connect
     * its to the `node`.
     */
    virtual void addInputWidgets(
            FlowScene *,
            Node &node ) const;

    /**
     * @brief
     * Create the needed output widgets on the scene and connect
     * its from the `node`.
     */
    virtual void addOutputWidgets(
            FlowScene *,
            Node &node ) const;

    /**
     * @brief
     * Layout the sender widget according to recipient widget on the scene.
     * @details
     * Changes a position for `sender` widget.
     * @see layoutInputWidgets()
     */
    virtual void layoutInputWidget(
            Node &sender,
            const Node &recipient,
            PortIndex portIndexRecipient ) const;

    /**
     * @brief
     * Layout the recipient widget according to sender widget on the scene.
     * @details
     * Changes a position for `recipient` widget.
     * @see layoutOutputWidgets()
     */
    virtual void layoutOutputWidget(
            Node &recipient,
            const Node &sender,
            PortIndex portIndexSender ) const;

    virtual void layoutInputWidgets( Node & ) const;

    virtual void layoutOutputWidgets( Node & ) const;

    /**
     * @brief Capitalizes a first letter.
     * @warning String will be simplified: trimmed and each sequence of
     * internal whitespace replaced with a single space.
     * \todo Replace to make::capitalizeFirst().
     */
    static QString &capitalizeFirst( QString & );

    /**
     * @see capitalizeFirst()
     */
    static QString capitalizeFirstCopy( const QString & );


protected:
    /**
     * @brief Set state and message.
     * @see validationState(), validationMessage()
     */
    virtual void validate();

    /**
     * @brief
     * Build model by `nameSender` and `portIndexSender` on the FlowScene
     * and connect it to the `portIndexRecipient` of `nodeRecipient`.
     */
    void addWidget(
            FlowScene *,
            Node *nodeRecipient,
            const QString &nameSender,
            PortIndex portIndexSender,
            PortIndex portIndexRecipient ) const;


protected:
    NodeValidationState validationState_ = NodeValidationState::Valid;
    QString validationMessage_ = tr( "Good " );


private:
    std::shared_ptr< Data > data_;
};


} // wquest
