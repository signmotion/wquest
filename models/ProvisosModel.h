#pragma once

#include "Model.h"
#include "ProvisosData.h"


namespace wquest {


class ProvisosModel : public Model {
    /// @see portCaption()
    static constexpr int maxLengthPortCaption = 12;


public:
    ProvisosModel();

    virtual ~ProvisosModel() override;


public:
    static QString sname();

    QString caption() const override;

    QString name() const override {
        return sname();
    }

    unsigned int nPorts( PortType ) const override;

    QString portCaption( PortType, PortIndex ) const override;

    NodeDataType dataType( PortType, PortIndex ) const override;

    std::shared_ptr< NodeData > outData( PortIndex ) override;

    void setInData( std::shared_ptr< NodeData >, PortIndex ) override;


private:
    std::shared_ptr< ProvisosData > data_;
};


} // wquest
