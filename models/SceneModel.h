#pragma once

#include "Model.h"
#include "SceneData.h"


namespace wquest {


class SceneModel : public Model {
public:
    SceneModel();

    virtual ~SceneModel() override;


public:
    static QString sname();

    QString caption() const override;

    QString name() const override {
        return sname();
    }

    unsigned int nPorts( PortType ) const override;

    QString portCaption( PortType, PortIndex ) const override;

    NodeDataType dataType( PortType, PortIndex ) const override;

    std::shared_ptr< NodeData > outData( PortIndex ) override;

    void setInData( std::shared_ptr< NodeData >, PortIndex ) override;

    void addInputWidgets(
            FlowScene *,
            Node &node ) const override;
};


} // wquest
