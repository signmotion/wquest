#pragma once

#include "Data.h"
#include <wquest/Proviso.h>
#include <QCoreApplication>


using QtNodes::NodeData;
using QtNodes::NodeDataType;


namespace wquest {


class ProvisoData : public Data {
    Q_DECLARE_TR_FUNCTIONS( ProvisoData )


public:
    explicit ProvisoData( const Proviso::ptr_t );

    NodeDataType type() const override {
        return sdt();
    }

    static NodeDataType sdt() {
        const QString name = tr( "Proviso" );
        return NodeDataType{ "Proviso", name };
    }
};


} // wquest
