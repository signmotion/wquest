#include "ProvisoModel.h"


namespace wquest {


ProvisoModel::ProvisoModel( const Proviso::ptr_t proviso ) :
    Model( std::make_shared< ProvisoData >( proviso ) )
{
}




ProvisoModel::~ProvisoModel() {
}




std::shared_ptr< NodeData >
ProvisoModel::outData( PortIndex ) {
    return data();
}


} // wquest
