#pragma once

#include <wquest/D.h>
#include <nodes/NodeData>


using QtNodes::NodeData;


namespace wquest {


/**
 * @brief The class which incapsulate `D`.
 */
class Data : public NodeData {
protected:
    Data() = delete;

    Data( const D::ptr_t d ) {
        ASSERT( d );
        data_ = d;
        setDirt();
    }


public:
    template< class T >
    std::shared_ptr< T > data() const {
        return data_->cast< T >();
    }

    const D::ptr_t &data() const {
        return data_;
    }

    void setDirt() {
        data_->setDirt();
    }

    bool getDirt() const {
        return data_->dirt();
    }


private:
    D::ptr_t data_;
};


} // wquest
