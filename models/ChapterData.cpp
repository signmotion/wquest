#include "ChapterData.h"
#include <wquest/DM.h>


namespace wquest {


ChapterData::ChapterData() :
    Data( dm().emitD< Chapter >( "", nullptr ) )
{
}


} // wquest
