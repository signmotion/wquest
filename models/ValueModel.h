#pragma once

#include "Model.h"
#include "ValueData.h"
#include <QTextEdit>


namespace wquest {


class ValueModel : public Model {
    Q_OBJECT


public:
    ValueModel();

    virtual ~ValueModel() override;


public:
    static QString sname();

    QString caption() const override;

    QString name() const override {
        return sname();
    }

    bool captionVisible() const override {
        return false;
    }

    unsigned int nPorts( PortType ) const override;

    NodeDataType dataType( PortType, PortIndex ) const override;

    std::shared_ptr< NodeData > outData( PortIndex ) override;

    void setInData( std::shared_ptr< NodeData >, PortIndex ) override;

    QWidget *embeddedWidget() override;


private slots:
  void onTextEdited();


private:
  std::shared_ptr< ValueData > data_;
  QTextEdit *text_;
};


} // wquest
