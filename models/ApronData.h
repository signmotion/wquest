#pragma once

#include "Data.h"
#include <wquest/Apron.h>
#include <wquest/dbgMacros.h>
#include <QCoreApplication>


using QtNodes::NodeData;
using QtNodes::NodeDataType;


namespace wquest {


class ApronData : public Data {
    Q_DECLARE_TR_FUNCTIONS( ApronData )


public:
    ApronData();

    NodeDataType type() const override {
        return sdt();
    }

    static NodeDataType sdt() {
        const QString name = tr( "Apron" );
        return NodeDataType{ "Value", name };
    }
};


} // wquest
