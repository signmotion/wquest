#pragma once

#include "Model.h"
#include "ProvisoData.h"


namespace wquest {


class ProvisoModel : public Model {
protected:
    explicit ProvisoModel( const Proviso::ptr_t );


public:
    virtual ~ProvisoModel() override;

    std::shared_ptr< NodeData > outData( PortIndex ) override;
};


} // wquest
