#include "ValueModel.h"


namespace wquest {


ValueModel::ValueModel() :
    data_( new ValueData() ),
    text_( new QTextEdit() )
{
    text_->setMaximumSize( text_->sizeHint() );
    const float height = text_->document()->size().height();
    text_->setMaximumHeight(height);
    text_->setMinimumHeight(height);
    text_->setWordWrapMode( QTextOption::WordWrap );
    text_->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    text_->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    connect( text_, &QTextEdit::textChanged,
        this, &ValueModel::onTextEdited);
}




ValueModel::~ValueModel() {
}




QString
ValueModel::sname() {
    return "Value";
}




QString
ValueModel::caption() const {
    return ValueData::sdt().name;;
}




unsigned int
ValueModel::nPorts( PortType portType ) const {

    switch ( portType ) {
        case PortType::In:
            return 0;

        case PortType::Out:
            return 1;
    }

    return 0;
}




NodeDataType
ValueModel::dataType( PortType, PortIndex ) const {
    return ValueData::sdt();
}




std::shared_ptr< NodeData >
ValueModel::outData( PortIndex ) {
    return data_;
}




void
ValueModel::setInData( std::shared_ptr< NodeData >, PortIndex ) {
}




QWidget *
ValueModel::embeddedWidget() {
    return text_;
}




void
ValueModel::onTextEdited() {

    const QString s = text_->toPlainText();
    data_.reset( new ValueData( s ) );
    constexpr PortIndex portIndex = 0;
    emit dataUpdated( portIndex );

    // change a height of text
    const float height = text_->document()->size().height();
    text_->setMaximumHeight(height);
    text_->setMinimumHeight(height);
}


} // wquest
