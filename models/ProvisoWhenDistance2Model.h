#pragma once

#include "ProvisoModel.h"


namespace wquest {


class ProvisoWhenDistance2Model : public ProvisoModel {
    Q_DECLARE_TR_FUNCTIONS( ProvisoWhenDistance2Model )


public:
    ProvisoWhenDistance2Model();

    virtual ~ProvisoWhenDistance2Model() override;

    static QString sname();

    QString name() const override {
        return sname();
    }

    QString caption() const override;

    unsigned int nPorts( PortType ) const override;

    QString portCaption( PortType, PortIndex ) const override;

    NodeDataType dataType( PortType, PortIndex ) const override;

    void setInData( std::shared_ptr< NodeData >, PortIndex ) override;

    void addInputWidgets(
            FlowScene *,
            Node &node ) const override;

    void addOutputWidgets(
            FlowScene *,
            Node &node ) const override;


protected:
    void validate() override;
};


} // wquest
