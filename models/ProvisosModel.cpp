#include "ProvisosModel.h"
#include "ProvisoData.h"
#include <wquest/DM.h>


namespace wquest {


ProvisosModel::ProvisosModel() :
    data_( new ProvisosData() )
{
    // fix count of ports
    // Reason: Errors when connections are deleted.
    // See https://github.com/paceholder/nodeeditor/pull/209/
    for (int i = 0; i < 5; ++i) {
        // don't add an empty Proviso because every instance of Proviso
        // will increase a Proviso's UID
        data_->addBack( nullptr );
    }
}




ProvisosModel::~ProvisosModel() {
}




QString
ProvisosModel::sname() {
    return "Provisos";
}




QString
ProvisosModel::caption() const {
    return ProvisosData::sdt().name;
}




unsigned int
ProvisosModel::nPorts( PortType portType ) const {

    switch ( portType ) {
        case PortType::In:
            return data_->data().size();

        case PortType::Out:
            return 1;
    }

    return 0;
}




QString
ProvisosModel::portCaption( PortType portType, PortIndex portIndex ) const {

    if ( portType == PortType::In ) {
        const Proviso::ptr_t proviso = data_->data( portIndex );
        const QString text = proviso ? proviso->declare().c_str() : "";
        const std::string txt =
            make::shorterCopy( text.toStdString(), maxLengthPortCaption );
        return QString::number( portIndex + 1 ) + ") " + txt.c_str();
    }

    return "";
}




NodeDataType
ProvisosModel::dataType( PortType portType, PortIndex ) const {

    switch ( portType ) {
        case PortType::In:
            return ProvisoData::sdt();

        case PortType::Out:
            return ProvisosData::sdt();
    }

    return ProvisosData::sdt();
}




std::shared_ptr< NodeData >
ProvisosModel::outData( PortIndex ) {
    return data_;
}




void
ProvisosModel::setInData( std::shared_ptr< NodeData > nd, PortIndex portIndex ) {

    const std::string s = nd ? nd->type().id.toStdString() : "nullptr";
    DBG_STRING( "ProvisosModel::setInData() ", s );

    const auto v = std::dynamic_pointer_cast< ProvisoData >( nd );
    const Proviso::ptr_t np = v ? v->data< Proviso >() : nullptr;
    data_->update( portIndex, np );

    //emit dataUpdated( portIndex );
}


} // wquest
