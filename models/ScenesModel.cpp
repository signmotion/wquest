#include "ScenesModel.h"
#include "SceneData.h"
#include <wquest/DM.h>


namespace wquest {


ScenesModel::ScenesModel() :
    data_( new ScenesData )
{
    // fix count of ports
    // Reason: Errors when connections are deleted.
    // See https://github.com/paceholder/nodeeditor/pull/209/
    for (int i = 0; i < 5; ++i) {
        // don't add an empty Scene because every instance of Scene
        // will increase a Scene's UID
        data_->addBack( nullptr );
    }
}




ScenesModel::~ScenesModel() {
}




QString
ScenesModel::sname() {
    return "Scenes";
}




QString
ScenesModel::caption() const {
    return ScenesData::sdt().name;
}




unsigned int
ScenesModel::nPorts( PortType portType ) const {

    switch ( portType ) {
        case PortType::In:
            return data_->data().size();

        case PortType::Out:
            return 1;
    }

    return 0;
}




QString
ScenesModel::portCaption( PortType portType, PortIndex portIndex ) const {

    if ( portType == PortType::In ) {
        const Scene::ptr_t scene = data_->data( portIndex );
        const QString text = scene ? scene->chapter()->name().c_str() : "";
        const std::string txt =
            make::shorterCopy( text.toStdString(), maxLengthPortCaption );
        return QString::number( portIndex + 1 ) + ") " + txt.c_str();
    }

    return "";
}




NodeDataType
ScenesModel::dataType( PortType portType, PortIndex ) const {

    switch ( portType ) {
        case PortType::In:
            return SceneData::sdt();

        case PortType::Out:
            return ScenesData::sdt();
    }

    return ScenesData::sdt();
}




std::shared_ptr< NodeData >
ScenesModel::outData( PortIndex ) {
    return data_;
}




void
ScenesModel::setInData( std::shared_ptr< NodeData > nd, PortIndex portIndex ) {

    const std::string s = nd ? nd->type().id.toStdString() : "nullptr";
    DBG_STRING( "ScenesModel::setInData() ", s );

    const auto v = std::dynamic_pointer_cast< SceneData >( nd );
    const Scene::ptr_t np = v ? v->data< Scene >() : nullptr;
    data_->update( portIndex, np );

    //emit dataUpdated( portIndex );
}


} // wquest
