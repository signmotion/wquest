#include "SceneModel.h"
#include "ChapterData.h"
#include "ApronData.h"
#include "ProvisosData.h"
#include "ProvisosModel.h"
#include "ValueData.h"
#include "ValueModel.h"
#include <wquest/DM.h>


namespace wquest {


SceneModel::SceneModel() :
    Model( std::make_shared< SceneData >(
               dm().emitD< Scene >( nullptr ) ) )
{
}




SceneModel::~SceneModel() {
}




QString
SceneModel::sname() {
    return "Scene";
}




QString
SceneModel::caption() const {
    return SceneData::sdt().name;
}




unsigned int
SceneModel::nPorts( PortType portType ) const {

    switch ( portType ) {
        case PortType::In:
            return 3;

        case PortType::Out:
            return 1;
    }

    return 0;
}




QString
SceneModel::portCaption( PortType portType, PortIndex portIndex ) const {

    if (portType == PortType::Out) {
        // outcoming port is without caption
        return "";
    }

    const Scene::ptr_t scene = data()->data< Scene >();
    if (portIndex == 0) {
        // chapter
        const QString chapter = scene->chapter()->name().c_str();
        if ( !chapter.isEmpty() ) {
            return "\"" + chapter + "\"";
        }
    }
    else if (portIndex == 1) {
        // apron
        const QString apron = scene->apron()->text().c_str();
        if ( !apron.isEmpty() ) {
            return apron;
        }
    }
    else if (portIndex == 2) {
        // provisos
        const int count = scene->countProviso();
        if (count > 0) {
            return dataType( portType, portIndex ).name +
                    " " + QString::number( count );
        }
    }

    return dataType( portType, portIndex ).name;
}




NodeDataType
SceneModel::dataType( PortType portType, PortIndex portIndex ) const {

    switch ( portType ) {
        case PortType::In :
            switch ( portIndex ) {
                case 0:
                    return ChapterData::sdt();
                case 1:
                    return ApronData::sdt();
                case 2:
                    return ProvisosData::sdt();
            }
            break;

        case PortType::Out :
            return SceneData::sdt();
    }

    //ASSERT( false && "Unrecognized port or index." );
    return SceneData::sdt();
}




std::shared_ptr< NodeData >
SceneModel::outData( PortIndex ) {
    return nullptr;
}




void
SceneModel::setInData( std::shared_ptr< NodeData > nd, PortIndex portIndex ) {

    const std::string s = nd ? nd->type().id.toStdString() : "nullptr";
    DBG_STRING( "SceneModel::setInData() ", s );

    const auto v = std::dynamic_pointer_cast< ValueData >( nd );
    const QString vc = v ? capitalizeFirstCopy( v->data() ) : "";

    // \see dataType()
    Scene::ptr_t scene = data()->data< Scene >();
    switch ( portIndex ) {
        case 0:
            // chapter
            scene->chapter()->name( vc.toStdString() );
            break;
        case 1:
            // apron
            scene->apron()->text( vc.toStdString() );
            break;
        case 2:
            // provisos
            const std::string declaration =
                    v ? v->data().toStdString() : "";
            // \todo setOrPushProviso( declaration );
            break;
    }

    //emit dataUpdated( portIndex );
}




void
SceneModel::addInputWidgets(
        FlowScene *fs,
        Node &node
) const {

    ASSERT( fs );

    // chapter
    addWidget( fs, &node, ValueModel::sname(), 0, 0 );
    // apron
    addWidget( fs, &node, ValueModel::sname(), 0, 1 );
    // provisos
    addWidget( fs, &node, ProvisosModel::sname(), 0, 2 );
}


} // wquest
