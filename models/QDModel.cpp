#include "QDModel.h"
#include "TitleData.h"
#include "AnnotationData.h"
#include "ValueData.h"
#include "ValueModel.h"
#include <wquest/DM.h>


namespace wquest {


QDModel::QDModel() :
    Model( std::make_shared< QDData >( dm().emitD< QD >( nullptr ) ) )
{
}




QDModel::~QDModel() {
}




QString
QDModel::sname() {
    return "QD";
}




QString
QDModel::caption() const {

    /* - We start with UID 2. Not nice to show it.
    const uid_t uid = data_->data()->uid();
    const QString name = QDData::sdt().name + " " + QString::number(uid);
    */

    const QString name = QDData::sdt().name;
    return name;
}




unsigned int
QDModel::nPorts( PortType portType ) const {

    switch ( portType ) {
        case PortType::In:
            return 2;
    }

    return 0;
}




QString
QDModel::portCaption( PortType portType, PortIndex portIndex ) const {

    if (portType == PortType::Out) {
        return "";
    }

    QD::ptr_t qd = data()->data< QD >();
    if (portIndex == 0) {
        // title
        const QString title = qd->title()->text().c_str();
        if ( !title.isEmpty() ) {
            return "\"" + title + "\"";
        }
    }
    else if (portIndex == 1) {
        // annotation
        const QString annotation = qd->annotation()->text().c_str();
        if ( !annotation.isEmpty() ) {
            return annotation;
        }
    }

    return dataType( portType, portIndex ).name;
}




NodeDataType
QDModel::dataType( PortType portType, PortIndex portIndex ) const {

    switch ( portType ) {
        case PortType::In :
            switch ( portIndex ) {
                case 0:
                    return TitleData::sdt();
                case 1:
                    return AnnotationData::sdt();
            }
            break;
    }

    //ASSERT( false && "Unrecognized port or index." );
    return QDData::sdt();
}




std::shared_ptr< NodeData >
QDModel::outData( PortIndex ) {
    return data();
}




void
QDModel::setInData( std::shared_ptr< NodeData > nd, PortIndex portIndex ) {

    const std::string s = nd ? nd->type().id.toStdString() : "nullptr";
    DBG_STRING( "QDModel::setInData()", s );

    const auto v = std::dynamic_pointer_cast< ValueData >( nd );
    const QString vc = v ? capitalizeFirstCopy( v->data() ) : "";

    // \see dataType()
    QD::ptr_t qd = data()->data< QD >();
    switch ( portIndex ) {
        case 0:
            // title
            qd->title()->text( vc.toStdString() );
            break;
        case 1:
            // annotation
            qd->annotation()->text( vc.toStdString() );
            break;
    }
    qd->setDirt();
}




void
QDModel::addInputWidgets(
        FlowScene *fs,
        Node &node
) const {

    ASSERT( fs );

    // title
    addWidget( fs, &node, ValueModel::sname(), 0, 0 );
    // annotation
    addWidget( fs, &node, ValueModel::sname(), 0, 1 );
}


} // wquest
