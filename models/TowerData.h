#pragma once

#include "Data.h"
#include <wquest/Tower.h>
#include <wquest/dbgMacros.h>
#include <QCoreApplication>


using QtNodes::NodeData;
using QtNodes::NodeDataType;


namespace wquest {


class TowerData : public Data {
    Q_DECLARE_TR_FUNCTIONS( TowerData )


public:
    explicit TowerData( const Tower::ptr_t );

    NodeDataType type() const override {
        return sdt();
    }

    static NodeDataType sdt() {
        const QString name = tr( "Tower" );
        return NodeDataType{ "Tower", name };
    }

    Scene::ptr_t dataScene( index_t ) const;

    void updateScene( index_t, const Scene::ptr_t newScene );

    void addSceneBack( const Scene::ptr_t );

    void removeScene( index_t );
};


} // wquest
