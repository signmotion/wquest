#pragma once

#include "Data.h"
#include <wquest/Chapter.h>
#include <wquest/dbgMacros.h>
#include <QCoreApplication>


using QtNodes::NodeData;
using QtNodes::NodeDataType;


namespace wquest {


class ChapterData : public Data {
    Q_DECLARE_TR_FUNCTIONS( ChapterData )


public:
    ChapterData();

    NodeDataType type() const override {
        return sdt();
    }

    static NodeDataType sdt() {
        const QString name = tr( "Chapter" );
        return NodeDataType{ "Value", name };
    }
};


} // wquest
