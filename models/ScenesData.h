#pragma once

#include <wquest/Scene.h>
#include <wquest/dbgMacros.h>
#include <nodes/NodeData>
#include <QCoreApplication>


using QtNodes::NodeData;
using QtNodes::NodeDataType;


namespace wquest {


class ScenesData : public NodeData {
    Q_DECLARE_TR_FUNCTIONS( ScenesData )


public:
    ScenesData();

    NodeDataType type() const override {
        return sdt();
    }

    static NodeDataType sdt() {
        const QString name = tr( "Scenes" );
        return NodeDataType{ "Scenes", name };
    }

    const Scene::ptrs_t &data() const {
        return data_;
    }

    Scene::ptr_t data( index_t ) const;

    void update( index_t, const Scene::ptr_t newScene );

    void addBack( const Scene::ptr_t );

    void remove( index_t );


private:
    Scene::ptrs_t data_;
};


} // wquest
