#include "Model.h"
#include "ValueModel.h"
#include "Data.h"
#include <wquest/common.h>
#include <wquest/dbgMacros.h>
#include <wquest/DM.h>


namespace wquest {


Model::Model() {
}




Model::Model( const std::shared_ptr< Data > data ) :
    data_( data )
{
    ASSERT( data_ );
}




Model::~Model() {
}




QWidget *
Model::embeddedWidget() {
    return nullptr;
}




void
Model::addInputWidgets(
        FlowScene *,
        Node &
) const {
}




void
Model::addOutputWidgets(
        FlowScene *,
        Node &
) const {
}




void Model::layoutInputWidget(
        Node &sender,
        const Node &recipient,
        PortIndex portIndexRecipient
) const {

    const int nSinksRecipient = recipient.nodeGeometry().nSinks();
    ASSERT( portIndexRecipient < nSinksRecipient );

    const QTransform recipientTransform =
            recipient.nodeGraphicsObject().sceneTransform();
    const QPointF portPosRecipient =
            recipient.nodeGeometry().portScenePosition(
                portIndexRecipient,
                PortType::In,
                recipientTransform );

    // calc a position and height the connected widget above
    int heightAbove = 0;
    QPointF posAbove;
    if ( portIndexRecipient > 0 ) {
        // widget above
        const int i = portIndexRecipient - 1;
        auto connections = recipient.nodeState()
                           .connections( PortType::In, i );
        for ( auto& conn : connections ) {
            const Node *s = conn.second->getNode( PortType::Out );
            heightAbove = s->nodeGeometry().height();
            posAbove = s->nodeGraphicsObject().pos();
            // by one connection only
            break;
        }
    } // if

    static const int dividerX = math::goldenRatio38 * 200;
    static const int dividerY = math::goldenRatio38 * 50;

    // calc and set a new position
    const bool hasAbove = (heightAbove != 0);
    const int width = sender.nodeGeometry().width();
    const int paddingX = width + dividerX;
    int paddingY = posAbove.y() + heightAbove;
    if (hasAbove) {
        paddingY += dividerY;
    }
    const int pxPort = portPosRecipient.x() - paddingX;
    const int px = hasAbove ? posAbove.x() : pxPort;
    const QTransform senderTransform =
            sender.nodeGraphicsObject().sceneTransform();
    const QPointF portPosSender =
            sender.nodeGeometry().portScenePosition(
                0,
                PortType::Out,
                senderTransform );
    const QPointF posSender = sender.nodeGraphicsObject().pos();
    const int shiftPortY = portPosSender.y() - posSender.y();
    const int pyPort = portPosRecipient.y() - shiftPortY;
    const int py = hasAbove ? paddingY : pyPort;
    const QPointF pos( px, py );

    sender.nodeGraphicsObject().setPos( pos );
}




void Model::layoutOutputWidget(
        Node &recipient,
        const Node &sender,
        PortIndex portIndexSender
) const {

    const int nSourcesSender = sender.nodeGeometry().nSources();
    ASSERT( portIndexSender < nSourcesSender );

    // according a position by output ports
    const QTransform transform =
            sender.nodeGraphicsObject().sceneTransform();
    const QPointF portPos =
            sender.nodeGeometry().portScenePosition(
                portIndexSender,
                PortType::Out,
                transform );
    const float height = sender.nodeGeometry().height();
    const bool oddPort = ((portIndexSender + 1) % 2 == 0);
    constexpr float px = math::goldenRatio38 * 200;
    const float paddingX = px * (oddPort ? 2 : 1);
    // \todo fine Use the above widget heights for calculates a padding by Y.
    const float paddingY = height / 2;
    const QPointF pos{
        portPos.x() + paddingX,
        portPos.y() + paddingY };
    recipient.nodeGraphicsObject().setPos( pos );
}




void
Model::layoutInputWidgets( Node &node ) const {

    // according a position by input ports
    const NodeDataModel *m = node.nodeDataModel();
    const int n  = m->nPorts( PortType::In );
    for (int i = 0; i < n; ++i) {
        auto connections = node.nodeState().connections( PortType::In, i );
        for ( auto& conn : connections ) {
            Node *sender = conn.second->getNode( PortType::Out );
            // layout by default `ValueModel` only
            const NodeDataModel *model = sender->nodeDataModel();
            //if ( model->name() == ValueModel::sname() ) {
                layoutInputWidget( *sender, node, i );
            //}
        }
    } // for i
}




void
Model::layoutOutputWidgets( Node &node ) const {

    const NodeDataModel *m = node.nodeDataModel();
    const int n  = m->nPorts( PortType::Out );
    for (int i = 0; i < n; ++i) {
        auto connections = node.nodeState().connections( PortType::Out, i );
        for ( auto& conn : connections ) {
            Node *recipient = conn.second->getNode( PortType::In );
            // layout by default `ValueModel` only
            const NodeDataModel *model = recipient->nodeDataModel();
            if ( model->name() == ValueModel::sname() ) {
                layoutOutputWidget( *recipient, node, i );
            }
        }
    } // for i
}




void
Model::validate() {
}




void
Model::addWidget(
        FlowScene *fs,
        Node *nodeRecipient,
        const QString &nameSender,
        PortIndex portIndexSender,
        PortIndex portIndexRecipient
) const {

    ASSERT( fs );
    ASSERT( nodeRecipient );

    auto &registry = fs->registry();
    auto typeSender = registry.create( nameSender );
    ASSERT( typeSender );
    auto& nodeSender = fs->createNode( std::move( typeSender ) );
    layoutInputWidget( nodeSender, *nodeRecipient, portIndexRecipient );
    const std::shared_ptr< Connection > connection =
        fs->createConnection(
                *nodeRecipient, portIndexRecipient,
                nodeSender, portIndexSender );
}




QString &
Model::capitalizeFirst( QString &s ) {

    s = s.simplified();
    if ( s.isEmpty() ) {
        return s;
    }

    s.front() = s.front().toUpper();
    return s;
}




QString
Model::capitalizeFirstCopy( const QString &s ) {
    QString ps = s;
    return capitalizeFirst( ps );
}


} // wquest
