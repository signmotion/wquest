#pragma once

#include "Data.h"
#include <wquest/Annotation.h>
#include <wquest/dbgMacros.h>
#include <QCoreApplication>


using QtNodes::NodeData;
using QtNodes::NodeDataType;


namespace wquest {


class AnnotationData : public Data {
    Q_DECLARE_TR_FUNCTIONS( AnnotationData )


public:
    AnnotationData();

    NodeDataType type() const override {
        return sdt();
    }

    static NodeDataType sdt() {
        const QString name = tr( "Annotation" );
        return NodeDataType{ "Value", name };
    }
};


} // wquest
