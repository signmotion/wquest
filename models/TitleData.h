#pragma once

#include "Data.h"
#include <wquest/Title.h>
#include <wquest/dbgMacros.h>
#include <QCoreApplication>


using QtNodes::NodeData;
using QtNodes::NodeDataType;


namespace wquest {


class TitleData : public Data {
    Q_DECLARE_TR_FUNCTIONS( TitleData )


public:
    TitleData();

    NodeDataType type() const override {
        return sdt();
    }

    static NodeDataType sdt() {
        const QString name = tr( "Title" );
        return NodeDataType{ "Value", name };
    }
};


} // wquest
