#pragma once

#include "Data.h"
#include <wquest/QD.h>
#include <QCoreApplication>


using QtNodes::NodeData;
using QtNodes::NodeDataType;


namespace wquest {


class QDData : public Data {
    Q_DECLARE_TR_FUNCTIONS( QDData )


public:
    explicit QDData( const QD::ptr_t );

    NodeDataType type() const override {
        return sdt();
    }

    static NodeDataType sdt() {
        const QString name = tr( "QD" );
        return NodeDataType{ "QD", name };
    }
};


} // wquest
