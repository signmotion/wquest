#include "Window.h"
#include "WindowManager.h"

#include <wquest/DM.h>


namespace wquest {


Window::Window( const D::ptr_t data, QWidget *parent ) :
    QWidget( parent ),
    data_( data )
{
    ASSERT( data_ );
    setPalette();
}




Window::~Window() {
}




void
Window::setPalette() {

    QPalette p = palette();
    p.setColor( QPalette::Window, Qt::black );
    QWidget::setPalette( p );
}




void
Window::keyPressEvent( QKeyEvent *e ) {

    const int key = e->key();
    if (key == Qt::Key_F11) {
        setWindowState( windowState() ^ Qt::WindowFullScreen );
    }
    else if (key == Qt::Key_F12 ) {
       WindowManager::instance().nextLanguageUI();
    }

    QWidget::keyPressEvent( e );
}




void
Window::closeEvent( QCloseEvent *e ) {

    // save all changes to long term store
    dm().flush().wait();

    QWidget::closeEvent( e );
}


} // wquest
