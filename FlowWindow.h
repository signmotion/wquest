#pragma once

#include "Window.h"

#include <nodes/DataModelRegistry>
#include <nodes/FlowScene>


namespace wquest {


class Model;
class QDModel;


class FlowWindow : public Window {
    Q_OBJECT


public:
    WQUEST_DECL_PTR_T( FlowWindow )

    FlowWindow( const D::ptr_t, QWidget *parent );

    virtual ~FlowWindow() override;


protected:
    virtual void changeEvent( QEvent * ) override;

    virtual void updateUI() override;

    virtual void setModel() = 0;

    void setRegistry( std::shared_ptr< QtNodes::DataModelRegistry > );

    /**
     * @brief Open model in new window.
     */
    void open( Model * );
    void open( QDModel * );

    /**
     * @brief Draw the nodes to flow window.
     */
    virtual void putNodes() = 0;


private:
    std::unique_ptr< QtNodes::FlowScene > flowScene_;
};


} // wquest
