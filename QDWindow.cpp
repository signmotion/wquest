#include "QDWindow.h"

// model
#include "models/ProvisosModel.h"
#include "models/ProvisoWhenDistance2Model.h"
#include "models/SceneModel.h"
#include "models/ScenesModel.h"
#include "models/TowerModel.h"
#include "models/ValueModel.h"

#include <wquest/DM.h>

#if 0
// example "calculator"
#include <nodeeditor/examples/calculator/NumberSourceDataModel.hpp>
#include <nodeeditor/examples/calculator/NumberDisplayDataModel.hpp>
#include <nodeeditor/examples/calculator/AdditionModel.hpp>
#include <nodeeditor/examples/calculator/SubtractionModel.hpp>
#include <nodeeditor/examples/calculator/MultiplicationModel.hpp>
#include <nodeeditor/examples/calculator/DivisionModel.hpp>
#include <nodeeditor/examples/calculator/ModuloModel.hpp>
#include <nodeeditor/examples/calculator/Converters.hpp>

// example "connection_colors"
#include <nodeeditor/examples/connection_colors/NaiveDataModel.h>

// example "dynamic_node"
#include <nodeeditor/examples/dynamic_node/RiverListModel.h>
#include <nodeeditor/examples/dynamic_node/RiverModel.h>

// example "images"
#include <nodeeditor/examples/images/ImageShowModel.hpp>
#include <nodeeditor/examples/images/ImageLoaderModel.hpp>

// example "styles"
#include <nodeeditor/examples/styles/models.hpp>

// example "text"
#include <nodeeditor/examples/example2/TextSourceDataModel.hpp>
#include <nodeeditor/examples/example2/TextDisplayDataModel.hpp>
#endif


using namespace QtNodes;


namespace wquest {


QDWindow::QDWindow( uid_t uid, QWidget *parent ) :
    FlowWindow(
        dm().getFromStore( Type::QD, nullptr, uid, nullptr ),
        parent )
{
    setModel();

    //setExampleModelCalculator();
    //setExampleModelConnectionColors();
    //setExampleModelDynamicNode();
    //setExampleModelImages();
    //setExampleModelStyles();
    //setExampleModelText();

    setMinimumSize( quantSize() * 4 );
    //setMaximumSize( minimumSize() * 2 );

    updateUI();

    putNodes();
}




QDWindow::~QDWindow() {
}




void QDWindow::updateUI() {

    FlowWindow::updateUI();

    const uid_t uid = data()->uid();
    const QString title = nameTitle() + " " + QString::number( uid );
    setWindowTitle( title );
}




void QDWindow::setModel() {

    auto r = std::make_shared< DataModelRegistry >();

    // base elements
    r->registerModel< ProvisosModel >( tr( "Base elements" ) );
    r->registerModel< ScenesModel >( tr( "Base elements" ) );
    r->registerModel< SceneModel >( tr( "Base elements" ) );
    r->registerModel< TowerModel >( tr( "Base elements" ) );
    r->registerModel< ValueModel >( tr( "Base elements" ) );

    // provisos
    r->registerModel< ProvisoWhenDistance2Model >( tr( "Provisos" ) );

    setRegistry( r );
}




#if 0
void QDWindow::setExampleModelCalculator() {

    auto r = std::make_shared< DataModelRegistry >();
    r->registerModel< NumberSourceDataModel >( "Sources" );
    r->registerModel< NumberDisplayDataModel >( "Displays" );
    r->registerModel< AdditionModel >( "Operators" );
    r->registerModel< SubtractionModel >( "Operators" );
    r->registerModel< MultiplicationModel >( "Operators" );
    r->registerModel< DivisionModel >( "Operators" );
    r->registerModel< ModuloModel >( "Operators" );
    r->registerTypeConverter(
        std::make_pair( DecimalData().type(), IntegerData().type() ),
        TypeConverter{ DecimalToIntegerConverter() } );
    r->registerTypeConverter(
        std::make_pair( IntegerData().type(), DecimalData().type() ),
        TypeConverter{ IntegerToDecimalConverter() } );

    flowScene_->setRegistry( r );

    ConnectionStyle::setConnectionStyle(
      R"( {
        "ConnectionStyle": {
          "ConstructionColor": "green",
          "NormalColor": "black",
          "SelectedColor": "green",
          "SelectedHaloColor": "deepskyblue",
          "HoveredColor": "deepskyblue",

          "LineWidth": 3.0,
          "ConstructionLineWidth": 2.0,
          "PointDiameter": 10.0,

          "UseDataDefinedColors": true
        }
      } )" );
}




void QDWindow::setExampleModelConnectionColors() {

    auto r = std::make_shared< DataModelRegistry >();
    r->registerModel< NaiveDataModel >();

    flowScene_->setRegistry( r );

    ConnectionStyle::setConnectionStyle(
      R"( {
          "ConnectionStyle": {
          "UseDataDefinedColors": true
        }
      } )" );
}




void QDWindow::setExampleModelDynamicNode() {

    auto r = std::make_shared< DataModelRegistry >();
    r->registerModel< RiverListModel >();
    r->registerModel< RiverModel >();

    flowScene_->setRegistry( r );
}




void QDWindow::setExampleModelImages() {

    auto r = std::make_shared< DataModelRegistry >();
    r->registerModel< ImageShowModel >();
    r->registerModel< ImageLoaderModel >();

    flowScene_->setRegistry( r );
}




void QDWindow::setExampleModelStyles() {

    auto r = std::make_shared< DataModelRegistry >();
    r->registerModel< MyStyleDataModel >();

    flowScene_->setRegistry( r );

    FlowViewStyle::setStyle(
    R"( {
      "FlowViewStyle": {
        "BackgroundColor": [255, 255, 240],
        "FineGridColor": [245, 245, 230],
        "CoarseGridColor": [235, 235, 220]
      }
    } )");

    NodeStyle::setNodeStyle(
    R"( {
      "NodeStyle": {
        "NormalBoundaryColor": "darkgray",
        "SelectedBoundaryColor": "deepskyblue",
        "GradientColor0": "mintcream",
        "GradientColor1": "mintcream",
        "GradientColor2": "mintcream",
        "GradientColor3": "mintcream",
        "ShadowColor": [200, 200, 200],
        "FontColor": [10, 10, 10],
        "FontColorFaded": [100, 100, 100],
        "ConnectionPointColor": "white",
        "PenWidth": 2.0,
        "HoveredPenWidth": 2.5,
        "ConnectionPointDiameter": 10.0,
        "Opacity": 1.0
      }
    } )");

    ConnectionStyle::setConnectionStyle(
    R"( {
      "ConnectionStyle": {
        "ConstructionColor": "gray",
        "NormalColor": "black",
        "SelectedColor": "gray",
        "SelectedHaloColor": "deepskyblue",
        "HoveredColor": "deepskyblue",

        "LineWidth": 3.0,
        "ConstructionLineWidth": 2.0,
        "PointDiameter": 10.0,

        "UseDataDefinedColors": false
      }
    } )");
}




void QDWindow::setExampleModelText() {

    auto r = std::make_shared< DataModelRegistry >();
    r->registerModel< TextSourceDataModel >();
    r->registerModel< TextDisplayDataModel >();

    flowScene_->setRegistry( r );
}
#endif




void QDWindow::putNodes() {

    // \TODO
}


} // wquest
