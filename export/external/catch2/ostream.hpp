#if 0

#pragma once

#include "export/wquest/dbgMacros.h"


#ifdef WQUEST_TESTS


#include "export/external/catch2/catch.hpp"
#include <sstream>
#include <iostream>




class out_buff : public std::stringbuf {
    std::FILE *mStream;

public:
    out_buff( std::FILE *stream ) : mStream( stream ) {}


    ~out_buff() { pubsync(); }


    int sync() {
        int ret = 0;
#if 0
        for ( unsigned char c : str()) {
            if ( putc( c, mStream ) == EOF) {
                ret = -1;
                break;
            }
        }
#else
        const std::string s{ str().cbegin(), str().cend() };
        printf( "%s\n", s.c_str() );
#endif
        // reset the buffer to avoid printing it multiple times
        str( "" );
        return ret;
    }
};




namespace Catch {


inline std::ostream &cout() {
    static std::ostream ret( new out_buff(stdout));
    return ret;
}


inline std::ostream &clog() {
    static std::ostream ret( new out_buff(stderr));
    return ret;
}


inline std::ostream &cerr() {
    return clog();
}


} // Catch


#endif

#endif
