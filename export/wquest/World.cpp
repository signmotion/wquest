#include "World.h"
#include "DM.h"


namespace wquest {


World::World(
        uid_t uid,
        const std::string &name,
        const parent_t parent
) :
    D( uid, parent )
{
    ASSERT( !name.empty() );

    World::name( name );
}




bool
World::operator==( const World &b ) const {
    return (uid() == b.uid()) &&
           (name_ == b.name_);
}




void
World::name( const std::string &name ) {

    initField( &name_, name );

    // \todo Not correct work for non-unicode languages. See TestCommon.
    //make::capitalizeFirst( name_ );
}




json_t
World::toJSON( rapidjson::Document *outer ) const {

    using namespace rapidjson;

    Document d( kObjectType );
    Document::AllocatorType &a =
            outer ? outer->GetAllocator() : d.GetAllocator();
    d.AddMember( "uid", uid(), a );
    d.AddMember( "type", stype(), a );
    d.AddMember( "name", name(), a );

    if ( qdList_ && !qdList_->empty() ) {
        d.AddMember( "countQD", qdList_->count(), a );
        // not only order, a list also
        Document nd( kArrayType );
        for (const auto &pd : qdList_->dps()) {
            nd.PushBack( pd.first, a );
        }
        d.AddMember( "orderQD", nd, a );
    }

    return d;
}




World::ptr_t
World::fromJSON( const JSON &json, parent_t parent ) {

    using namespace rapidjson;

    ASSERT( json.isObject() );

    DBG_STRING( "World::fromJSON()\n", std::to_string( json ) );

    const std::string stype = json.getSType( stypeStatic() );
    const uid_t uid = json.getUID();
    const std::string name = json.getName();
    const World::ptr_t r = std::make_shared< World >( uid, name, parent );
    runWhenHas( json, "QD", [ &r ] ( const std::vector< uid_t > &uids ) {
        r->addQDList( uids );
    } );

    return r;
}




void
World::initQDList() {

    if ( !qdList_ ) {
        qdList_.reset( new LazyList< QD >( cast< D >(), nullptr ) );
    }
}




void
World::addQDList( const std::vector< uid_t > &uids ) {

    ASSERT( !uids.empty() );

    initQDList();
    qdList_->add( uids );
}


} // wquest
