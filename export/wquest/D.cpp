#include "D.h"


namespace wquest {


D::D( uid_t uid, const parent_t parent ) :
    uid_( uid ),
    parent_( parent ),
    dirt_( true )
{
    ASSERT( uid_ > 0 );
    ASSERT( is::defined( uid_ ) );
    //ASSERT( parent_.expired() ); - We can have a data without poarent.
}




D::~D() {
    //DBG( "D::~D() %s %i", stype().c_str(), uid() );
}




std::string
D::path() const {

    std::string r;
    buildPath( &r, this );
    return r;
}




void
D::buildPath( std::string *r, const D *d ) {

    ASSERT( r );
    ASSERT( d );

    const parent_t parent = d->parent();
    std::string stype = d->stype();
    std::transform( stype.cbegin(), stype.cend(), stype.begin(), ::tolower );
    const bool single = parent ? parent->singleInParent( d->type() ) : false;
    *r = stype + "/" + (single ? "" : (d->suid() + "/")) + *r;
    if ( parent ) {
        buildPath( r, parent.get() );
    }
}




void
D::initField( std::string *field, const std::string &v ) {

    ASSERT( field );

    const std::string pv = make::trimCopy( v );
    if (*field != pv) {
        *field = pv;
        setDirt();
    }
}




void
D::runWhenHas(
        const JSON &json,
        const std::string &stype,
        std::function< void( const std::vector< uid_t > & ) > run
) {
    const std::string countName = "count" + stype;
    if ( json.has( countName ) ) {
        const std::string orderName = "order" + stype;
        const std::vector< uid_t > order = json.getArrayInt( orderName );
        // \todo optimize Don't parse `countQD`.
        const int count = json.getInt( countName );
        ASSERT( count == order.size() );
        run( order );
    }
}


} // wquest
