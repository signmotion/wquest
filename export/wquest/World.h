#pragma once

#include "D.h"
#include "QD.h"
#include "LazyList.h"


namespace wquest {


/**
 * @brief Incapsulate a world by analogy with `QD`.
 */
class World : public D {
public:
    WQUEST_DECL_CAST_AND_PTR( World )

    World() = delete;

    World( uid_t, const std::string &name, const parent_t );

    virtual Type type() const override {
        return etypeStatic();
    }

    virtual std::string stype() const override {
        return stypeStatic();
    }

    static std::string stypeStatic() {
        return std::to_string( etypeStatic() );
    }

    static Type etypeStatic() {
        return Type::World;
    }

    bool operator==( const World & ) const;

    const std::string &name() const {
        return name_;
    }

    void name( const std::string & );


    WQUEST_BUILD_LAZY_LIST_METHODS( QD, qd )


    virtual json_t toJSON( rapidjson::Document *outer ) const override;

    /**
     * @warning Not uses `DM::emit()`.
     */
    static World::ptr_t fromJSON( const JSON &, parent_t );


private:
    void initQDList();

    void addQDList( const std::vector< uid_t > & );


private:
    std::string name_;
    LazyList< QD >::ptr_t qdList_;
};


} // wquest
