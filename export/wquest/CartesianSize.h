#pragma once

#include "CartesianCoord.h"


namespace wquest {


/// \brief The size like segment 2D-box.
class CartesianSize : public CartesianCoord {
public:
    CartesianSize() : CartesianCoord() {}
    explicit CartesianSize( float x ) : CartesianCoord( x ) {}
    CartesianSize( float x, float y ) : CartesianCoord( x, y ) {}
    explicit CartesianSize( int x ) : CartesianCoord( x ) {}
    CartesianSize( int x, int y ) : CartesianCoord( x, y ) {}
    virtual ~CartesianSize() {}

    /// \alias y()
    float height() const { return y(); }
    void height( const int& v ) { y( v ); }
    void height( const float& v ) { y( v ); }

    /// \alias x()
    float width() const { return x(); }
    void width( const int& v ) { x( v ); }
    void width( const float& v ) { x( v ); }

    /// \brief Centre point of size.
    CartesianCoord centre() const { CartesianCoord r; for ( int i = 0; i < D; ++i ) { r[ i ] /= 2; } return r; }

    /// \brief Square for 2D: width * height.
    float square() const { return width() * height(); }

    static const CartesianSize& UNDEFINED();
};




inline const CartesianSize&
CartesianSize::UNDEFINED() {
    static const CartesianSize  r;
    return r;
}




// \brief The size like segment 2D-box.
class CartesianSizeInt : public CartesianCoordInt {
public:
    CartesianSizeInt() : CartesianCoordInt() {}
    explicit CartesianSizeInt( float x ) : CartesianCoordInt( x ) { ASSERT( x > 0 ); }
    CartesianSizeInt( float x, float y ) : CartesianCoordInt( x, y ) { ASSERT( (x > 0) && (y > 0) ); }
    explicit CartesianSizeInt( int x ) : CartesianCoordInt( x ) { ASSERT( x > 0 ); }
    CartesianSizeInt( int x, int y ) : CartesianCoordInt( x, y ) { ASSERT( (x > 0) && (y > 0) ); }
    virtual ~CartesianSizeInt() {}

    // \alias y()
    const int& height() const { return y(); }
    void height( const int& v ) { y( v ); }
    void height( const float& v ) { y( v ); }

    // \alias x()
    const int& width() const { return x(); }
    void width( const int& v ) { x( v ); }
    void width( const float& v ) { x( v ); }

    // \brief Centre point of size.
    CartesianCoord centre() const { CartesianCoordInt r; for ( int i = 0; i < D; ++i ) { r[ i ] /= 2; } return r; }

    // \brief Square for 2D: width * height.
    int square() const { return width() * height(); }

    static const CartesianSizeInt& UNDEFINED();
};




inline const CartesianSizeInt&
CartesianSizeInt::UNDEFINED() {
    static const CartesianSizeInt  r;
    return r;
}


} // wquest
