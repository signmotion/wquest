#pragma once

#include <assert.h>
#include <iostream>


namespace wquest {


/**
 * \brief Uncomment this define for run unit tests only.
 */
#define WQUEST_TESTS


// For debug.
#ifdef _DEBUG

/// \brief Guarantee to flush all output std-streams.
class FlushOutputs {
public:
    ~FlushOutputs() {
        std::cout.flush();
        std::cerr.flush();
    }
};

static const FlushOutputs flushOutputs;


/// \brief Seed for random generators.
/// \details After defined it we have same random sequence every start the app.
#define WQUEST_RAND_SEED 38

#define QUOTE_(WHAT)              #WHAT
#define QUOTE(WHAT)               QUOTE_(WHAT)
#define DBG(format, ...)          { const FlushOutputs fo; printf("D) %s: " format "\n", __FILE__ ":" QUOTE(__LINE__), ## __VA_ARGS__); }
#define DBG_A( msg, atman )       DBG(msg"\n%s", std::to_string( atman ).c_str())
#define DBG_D( msg, data )        DBG(msg"\n%s", std::to_string( data ).c_str())
#define DBG_E( msg, element )     DBG(msg"\n%s", std::to_string( element ).c_str())
#define DBG_Q( msg, quest )       DBG(msg"\n%s", std::to_string( quest ).c_str())
#define DBG_S( msg, suite )       DBG(msg"\n%s", std::to_string( suite ).c_str())
#define DBG_COORD( msg, coord )   DBG(msg" %f %f", (coord).x, (coord).y)
#define DBG_COORDI( msg, coord )  DBG(msg" %d %d", (coord).x, (coord).y)
#define DBG_FLOAT( msg, v )       DBG(msg" %f", v)
#define DBG_INT( msg, v )         DBG(msg" %d", v)
#define DBG_JSON( msg, json )     DBG(msg" JSON '%s'", toPrettyJSON(json).c_str())
#define DBG_SIZE( msg, size )     DBG(msg" %f %f", (size).width, (size).height)
#define DBG_SIZEI( msg, size )    DBG(msg" %d %d", (size).width, (size).height)
#define DBG_STRING( msg, s )      DBG(msg" '%s'", (s).c_str())

#ifdef WQUEST_TESTS
// skip a debug output when test
#define ASSERT(expr)   if (!(expr)) { DBG("Test assert "#expr" worked. It's right?"); throw std::runtime_error(QUOTE(expr)); }
#else
#define ASSERT(expr)   if (!(expr)) { DBG("Assert "#expr" failed."); throw std::runtime_error(QUOTE(expr)); }
#endif

#define DASSERT(expr)  if (!(expr)) __debugbreak()
#define VERIFY(expr)   ASSERT(expr)




#else

#define ASSERT( EXPR )     ((void)0)
#define VERIFY( EXPR )      (EXPR)
#define DBG( format, ... )  ((void)0)

#define WQUEST_RAND_SEED std::chrono::high_resolution_clock::now().time_since_epoch().count()

#endif


#if defined(__GNUC__) && ((__GNUC__ >= 4) || ((__GNUC__ == 3) && (__GNUC_MINOR__ >= 1)))
#define DEPRECATED_ATTRIBUTE_WQUEST __attribute__((deprecated))
#elif _MSC_VER >= 1400 //vs 2005 or higher
#define DEPRECATED_ATTRIBUTE_WQUEST __declspec(deprecated)
#endif


} // wquest
