#pragma once

#include "Proviso.h"
#include "MorpherInstance.h"
#include "MorpherCompare.h"
#include "MorpherDistance.h"


namespace wquest {


class ProvisoWhenDistance2 : public Proviso {
public:
    WQUEST_DECL_CAST_AND_PTR( ProvisoWhenDistance2 )


public:
    using Proviso::Proviso;

    virtual bool define() const override;

    virtual int countDefine() const override;

    virtual bool valid() const override;

    virtual int countValid() const override;

    virtual declare_t declare() const override;
    
    virtual std::string caption() const override;

    virtual json_t jsonMorphers( rapidjson::Document *outer ) const override;


public:
    void morpherInstanceA( const MorpherInstance::ptr_t m ) {
        morpherInstanceA_ = m;
    }

    const MorpherInstance::ptr_t &morpherInstanceA() const {
        return morpherInstanceA_;
    }

    void morpherInstanceB( const MorpherInstance::ptr_t m ) {
        morpherInstanceB_ = m;
    }

    const MorpherInstance::ptr_t &morpherInstanceB() const {
        return morpherInstanceB_;
    }

    void morpherCompare( const MorpherCompare::ptr_t m ) {
        morpherCompare_ = m;
    }

    const MorpherCompare::ptr_t &morpherCompare() const {
        return morpherCompare_;
    }

    void morpherDistance( const MorpherDistance::ptr_t m ) {
        morpherDistance_ = m;
    }

    const MorpherDistance::ptr_t &morpherDistance() const {
        return morpherDistance_;
    }


private:
    MorpherInstance::ptr_t morpherInstanceA_;
    MorpherInstance::ptr_t morpherInstanceB_;
    MorpherCompare::ptr_t morpherCompare_;
    MorpherDistance::ptr_t morpherDistance_;
};


} // wquest
