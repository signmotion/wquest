#pragma once

#include <assert.h>
#include <string>


/**
 * @see WQUEST_BUILD_LAZY_LIST_METHODS
 */
#define WQUEST_BUILD_LAZY_LIST_METHODS_WITHOUT_EMIT(Upper,lower)  \
    bool empty##Upper() { \
        return count##Upper() == 0; \
    } \
 \
    int count##Upper() { \
        init##Upper##List(); \
        return lower##List_->count(); \
    } \
 \
    Upper::ptr_t lower##Front() { \
        return lower##I( 0 ); \
    } \
 \
    Upper::ptr_t lower##Back() { \
        init##Upper##List(); \
        return lower##I( lower##List_->count() - 1 ); \
    } \
 \
    Upper::ptr_t lower##I( index_t i ) { \
        init##Upper##List(); \
        return lower##List_->getI( i ); \
    } \
 \
    Upper::ptr_t lower( uid_t uid ) { \
        init##Upper##List(); \
        return lower##List_->get( uid ); \
    } \
 \
    void add##Upper##Front( const Upper::ptr_t d ) { \
        ASSERT( d ); \
        init##Upper##List(); \
        VERIFY( lower##List_->add( 0, d ) ); \
    } \
 \
    void add##Upper##Back( const Upper::ptr_t d ) { \
        ASSERT( d ); \
        init##Upper##List(); \
        VERIFY( lower##List_->add( d ) ); \
    } \
 \
    void add##Upper( index_t i, const Upper::ptr_t d ) { \
        ASSERT( d ); \
        init##Upper##List(); \
        VERIFY( lower##List_->add( i, d ) ); \
    } \
 \
    void remove##Upper( index_t i ) { \
        init##Upper##List(); \
        VERIFY( lower##List_->remove( i ) ); \
    } \
 \
    void clear##Upper() { \
        init##Upper##List(); \
        lower##List_->clear(); \
    } \
 \
    const LazyList< Upper >::ptr_t &lower##List() { \
        init##Upper##List(); \
        return lower##List_; \
    }




/**
 * @brief Build the needed methods for lazy data.
 * @see `World` for example to use it.
 * @see WQUEST_BUILD_LAZY_LIST_METHODS_WITHOUT_EMIT
 */
#define WQUEST_BUILD_LAZY_LIST_METHODS(Upper,lower)  \
    WQUEST_BUILD_LAZY_LIST_METHODS_WITHOUT_EMIT( Upper, lower ) \
 \
    Upper::ptr_t add##Upper##Front() { \
        const Upper::ptr_t d = dm().emitD< Upper >( cast< D >() ); \
        add##Upper##Front( d ); \
        return d; \
    } \
 \
    Upper::ptr_t add##Upper##Back() { \
        const Upper::ptr_t d = dm().emitD< Upper >( cast< D >() ); \
        add##Upper##Back( d ); \
        return d; \
    } \
 \
    Upper::ptr_t add##Upper( index_t i ) { \
        const Upper::ptr_t d = dm().emitD< Upper >( cast< D >() ); \
        add##Upper( i, d ); \
        return d; \
    }
