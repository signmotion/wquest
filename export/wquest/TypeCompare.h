#pragma once

#include "dbgMacros.h"
#include <array>
#include <string>


namespace wquest {


/// \waning Must be accordance with 'strEnumTypeCompare'.
/// \see is::correct( TypeCompare )
enum class TypeCompare {
    Undefined = 0,
    Less,
    More,
    // count of enum
    count
};


/// \brief For the humanly output.
/// \waning Must be accordance with 'enum TypeCompare'.
static const std::array< std::string, ( int )TypeCompare::count > strEnumTypeCompare = {
    "Undefined",
    "Less",
    "More",
};




namespace is {


inline bool correct( TypeCompare type ) {
    return (type >= TypeCompare::Undefined) && (type < TypeCompare::count);
}


inline bool undefined( TypeCompare type ) {
    return (type == TypeCompare::Undefined);
}


inline bool defined( TypeCompare type ) {
    return !undefined( type );
}


} // is


} // wquest




namespace std {

inline std::string to_string( wquest::TypeCompare type ) {

    using namespace wquest;
    ASSERT( is::correct( type ) );
    return strEnumTypeCompare[ (int)type ];
}

} // std
