#include "StoreFile.h"
#include "D.h"
#include "QD.h"
#include "World.h"
#include "DM.h"
#include <filesystem>
#include <fstream>


namespace fs = std::filesystem;


namespace wquest {


StoreFile::StoreFile( const std::string &basePath ) :
    basePath_( basePath )
{
    ASSERT( !basePath_.empty() );

    if ( basePath_.back() != '/' ) {
        basePath_ += '/';
    }

    createBasePath();
}




StoreFile::~StoreFile() {
}




void
StoreFile::emitD( const D::ptr_t d ) {

    ASSERT( d );

    using namespace rapidjson;

    const uid_t uid = d->uid();
    ASSERT( is::correct( uid ) );
    const std::string p = basePath_ + d->path();
    fs::create_directories( p );

    const sjson_t &s = d->toSJSON( nullptr );
    std::string name = d->stype();
    std::transform( name.cbegin(), name.cend(), name.begin(), ::tolower );
    const D::parent_t parent = d->parent();
    const bool multi = parent ? parent->multiInParent( d->type() ) : true;
    if ( multi ) {
        name += "." + d->suid();
    }
    const std::string f = p + name + extension();
    std::ofstream ofs( f );
    ofs << s;
}




bool
StoreFile::empty() const {
    return count() == 0;
}




void
StoreFile::clear() {

    std::error_code error;
    fs::remove_all( basePath_, error );
    ASSERT( !error && "Error when clear a store." );
    createBasePath();
}




int
StoreFile::count() const {

    // direct depends from emitD()

    // \todo optimize Count without getting a list of UIDs.
    const int countWorld = get( Type::World, nullptr ).size();

    return countWorld;
}




std::string
StoreFile::content( const std::string &path ) const {

    ASSERT( !path.empty() );

    const std::string p = refreshPath( path );

    return get::content( p );
}




json_t
StoreFile::get( Type type, const std::shared_ptr< D > parent, uid_t uid ) {

    //ASSERT( is::correct( type ) ); - Type can be user default.
    ASSERT( is::correct( uid ) );

    const auto handle = [ this ] (
            Type type,
            const std::shared_ptr< D > parent,
            uid_t uid
    ) -> json_t {
        // load from file
        //std::lock_guard lockStore( mutex );
        // direct depends from emitD()
        std::string stype = typeConverterToS_( type );
        std::transform(
                    stype.cbegin(), stype.cend(), stype.begin(), ::tolower );
        const std::string parentPath = parent ? parent->path() : "";
        const suid_t suid = std::to_string( uid );
        const std::string path =
                basePath_ + parentPath + stype + "/" + suid + "/";;

        // load an info from file
        const std::string fileName = stype + "." + suid + extension();
        const std::string pf = path + fileName;
        ASSERT( hasFile( pf ) );
        const sjson_t content = get::content( pf );
        ASSERT( !content.empty() );

        // \todo optimize Remove in Release.
        {
            std::string errors;
            const json_t &json = toJSON( content, &errors );
            ASSERT( errors.empty() );
            ASSERT( json.IsObject() );
        }

        return toJSON( content, nullptr );
    };

    return handle( type, parent, uid );
}




uids_t
StoreFile::get( Type type, const std::shared_ptr< D > parent ) const {

    // direct depends from emitD()

    //ASSERT( is::correct( type ) ); - Type can be user default.

    std::string stype = typeConverterToS_( type );
    std::transform( stype.cbegin(), stype.cend(), stype.begin(), ::tolower );
    const std::string parentPath = parent ? parent->path() : "";
    const std::string path = basePath_ + parentPath + stype + "/";

    return getUIDsFromFolder( path );
}




uids_t
StoreFile::getUIDsFromFolder( const std::string &path ) const {

    // direct depends from emitD()

    uids_t r;
    if ( !hasFolder( path ) ) {
        return r;
    }

    for ( const fs::path &dir : fs::directory_iterator( path ) ) {
        const std::string &file = dir.filename().string();
        const std::string &pathFile = dir.string();
        const std::string &ext = dir.extension().string();
        // get uid from file content or folder name
        const uid_t uid =
                (ext == ".i") ?
                    getUIDFromFile( pathFile ) :
                    parse::numberInteger( file );
        ASSERT( is::correct( uid ) );
        r.emplace( uid );
        // \todo optimize Break immediately after meeting
        //       a file instead of folder.
    }

    return r;
}




uid_t
StoreFile::getUIDFromFile( const std::string &pathFile ) const {

    if ( !hasFile( pathFile ) ) {
        return get::undefined< uid_t >();
    }

    // get UID from file
    const sjson_t content = get::content( pathFile );
    ASSERT( !content.empty() );
    std::string errors;
    const json_t json = toJSON( content, &errors );
    ASSERT( errors.empty() );
    ASSERT( json.IsObject() );
    ASSERT( json.GetObject().HasMember( "uid" ) );
    const uid_t uid = json[ "uid" ].GetInt();
    ASSERT( is::correct( uid ) );

    return uid;
}




bool
StoreFile::hasFolder( const std::string &folder ) const {

    ASSERT( !folder.empty() );

    const std::string p = refreshPath( folder);

    //return fs::exists( p ) && fs::is_directory( p );
    if ( !fs::exists( p ) ) {
        return false;
    }
    if ( !fs::is_directory( p ) ) {
        return false;
    }

    return true;
}




bool
StoreFile::hasFile( const std::string &file ) const {

    ASSERT( !file.empty() );

    const std::string p = refreshPath( file );

    //return fs::exists( p ) && fs::is_regular_file( p );
    if ( !fs::exists( p ) ) {
        return false;
    }
    if ( !fs::is_regular_file( p ) ) {
        return false;
    }

    return true;
}




void
StoreFile::createBasePath() const {

    const bool error = fs::create_directories( basePath_ );
    ASSERT( !error && "Error when create a path for store." );
}




std::string
StoreFile::refreshPath( const std::string &path ) const {

    const bool startFromPath = (path.find( basePath_ ) == 0);
    return startFromPath ? path : (basePath_ + path);
}


} // wquest
