#include "RapidJSON.h"


namespace wquest {


std::vector< int >
RapidJSON::getArrayInt(
        const std::string &name,
        std::vector< int > defaultValue
) const {
    using namespace rapidjson;

    ASSERT( !defaultValue.empty() || doc().HasMember( name ) );
    if ( !doc().HasMember( name ) || !isArray( name ) ) {
        return defaultValue;
    }

    std::vector< int > r;
    const rapidjson::Value &a = doc()[ name ];
    for ( const Value &v : a.GetArray() ) {
        ASSERT( v.IsInt() );
        const int n = v.GetInt();
        r.emplace_back( n );
    }

    return r;
}




std::string
RapidJSON::toPrettyJSON() const {

    using namespace rapidjson;

    StringBuffer  sb;
    PrettyWriter< StringBuffer > w( sb );
    w.SetIndent( ' ', 2 );
    w.SetFormatOptions( kFormatSingleLineArray );
    doc().Accept( w );

    return sb.GetString();
}


} // wquest
