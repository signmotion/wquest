#include "Title.h"
#include "common.h"
#include "dbgMacros.h"
#include <cctype>


namespace wquest {


Title::Title(
        uid_t uid,
        const std::string &text,
        const parent_t parent
) :
    D( uid, parent )
{
    //ASSERT( !name.empty() );

    Title::text( text );
}




bool
Title::operator==( const Title &b ) const {
    return (uid() == b.uid()) &&
           (text_ == b.text_);
}




void
Title::text( const std::string &text ) {

    initField( &text_, text );

    // \todo Not correct work for non-unicode languages. See TestCommon.
    //make::capitalizeFirst( text_ );
}




json_t
Title::toJSON( rapidjson::Document *outer ) const {

    using namespace rapidjson;

    Document d( kObjectType );
    Document::AllocatorType &a =
            outer ? outer->GetAllocator() : d.GetAllocator();
    d.AddMember( "uid", uid(), a );
    d.AddMember( "type", stype(), a );
    d.AddMember( "text", StringRef( text().c_str() ), a );

    return d;
}




Title::ptr_t
Title::fromJSON( const json_t &json, parent_t ) {

    /* - TODO
    ASSERT( json.IsObject() );

    using namespace rapidjson;

    ASSERT( json.HasMember( "type" ) );
    const std::string type = json[ "type" ].GetString();
    if ( type != stype() ) {
        return nullptr;
    }

    ASSERT( json.HasMember( "uid" ) );
    const uid_t uid = json[ "uid" ].GetInt();
    if ( !is::correct( uid ) ) {
        return nullptr;
    }

    const std::string name =
            json.HasMember( "name" ) ? json[ "name" ].GetString() : "";
    const Title::ptr_t title = std::make_shared< Title >( uid, name );

    return title;
    */

    return nullptr;
}


} // wquest
