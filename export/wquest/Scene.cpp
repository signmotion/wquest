#include "Scene.h"
#include "DM.h"
#include "dbgMacros.h"


namespace wquest {


Scene::Scene( uid_t uid, const parent_t parent ) :
    D( uid, parent )
{
}




bool
Scene::operator==( const Scene &b ) const {

    /* - TODO
    return (uid() == b.uid()) &&
           (*chapter_ == *b.chapter_) &&
           (*apron_ == *b.apron_);
    */
    return (uid() == b.uid());
}




Chapter::ptr_t
Scene::chapter() {

    if ( !chapter_ ) {
        chapter_ = dm().emitD< Chapter >( "", cast< D >() );
    }

    return chapter_;
}




const Impact::ptrs_t &
Scene::impacts() const {
    return impacts_;
}




Apron::ptr_t
Scene::apron() {

    if ( !apron_ ) {
        apron_ = dm().emitD< Apron >( "", cast< D >() );
    }

    return apron_;
}




json_t
Scene::toJSON( rapidjson::Document *outer ) const {

    using namespace rapidjson;

    Document d( kObjectType );
    Document::AllocatorType &a =
            outer ? outer->GetAllocator() : d.GetAllocator();
    d.AddMember( "uid", uid(), a );
    d.AddMember( "type", stype(), a );

    if ( chapter_ ) {
        Document nd = chapter_->toJSON( outer );
        d.AddMember( "chapter", nd, a );
    }

    // \todo Include impacts_ like proviso_.

    if ( apron_ ) {
        Document nd = apron_->toJSON( outer );
        d.AddMember( "apron", nd, a );
    }

    if ( provisoList_ && !provisoList_->empty() ) {
        d.AddMember( "countProviso", provisoList_->count(), a );
        // not only order, a list also
        Document nd( kArrayType );
        for (const auto &pd : provisoList_->dps()) {
            nd.PushBack( pd.first, a );
        }
        d.AddMember( "orderProviso", nd, a );
    }

    return d;
}




Scene::ptr_t
Scene::fromJSON( const JSON &json, parent_t parent ) {

    using namespace rapidjson;

    ASSERT( json.isObject() );

    DBG_STRING( "Scene::fromJSON()\n", std::to_string( json ) );

    const std::string stype = json.getSType( stypeStatic() );
    const uid_t uid = json.getUID();
    const Scene::ptr_t r = std::make_shared< Scene >( uid, parent );

    /* - TODO
    // chapter
    if ( json.HasMember( "chapter" ) ) {
        auto &v = json[ "chapter" ];
        Document djson;
        djson.CopyFrom( v, djson.GetAllocator() );
        const D::ptr_t d = dm().emitD( djson );
        ASSERT( d );
        scene->chapter_ = d->cast< Chapter >();
        ASSERT( scene->chapter_ );
    }

    // \TODO impacts

    // apron
    if ( json.HasMember( "apron" ) ) {
        auto &v = json[ "apron" ];
        Document djson;
        djson.CopyFrom( v, djson.GetAllocator() );
        const D::ptr_t d = dm().emitD( djson );
        ASSERT( d );
        scene->apron_= d->cast< Apron >();
        ASSERT( scene->apron_ );
    }
    */

    runWhenHas( json, "Proviso", [ &r ] ( const std::vector< uid_t > &uids ) {
        r->addProvisoList( uids );
    } );

    return r;
}




void
Scene::initProvisoList() {

    if ( !provisoList_ ) {
        provisoList_.reset( new LazyList< Proviso >( cast< D >(), nullptr ) );
    }
}




void
Scene::addProvisoList( const std::vector< uid_t > &uids ) {

    ASSERT( !uids.empty() );

    initProvisoList();
    provisoList_->add( uids );
}


} // wquest
