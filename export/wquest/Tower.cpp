#include "Tower.h"
#include "DM.h"


namespace wquest {


Tower::Tower( uid_t uid, const parent_t parent ) :
    D( uid, parent )
{
    // without scenes
}




bool
Tower::operator==( const Tower &b ) const {

    /* - todo optimize
    if (uid() != b.uid()) {
        return false;
    }

    if ( scenes_.size() != b.scenes_.size() ) {
        return false;
    }

    auto ita = scenes_.cbegin();
    ASSERT( ita != scenes_.cend() );
    auto itb = b.scenes_.cend();
    ASSERT( itb != b.scenes_.cend() );
    for ( ; ita != scenes_.cend() || itb != b.scenes_.cend(); ) {
        if ( *ita != *itb ) {
            return false;
        }
        ++ita;
        ++itb;
    }

    return true;
    */

    return toSJSON( nullptr ) == b.toSJSON( nullptr );
}




json_t
Tower::toJSON( rapidjson::Document *outer ) const {

    using namespace rapidjson;

    Document d( kObjectType );
    Document::AllocatorType &a =
            outer ? outer->GetAllocator() : d.GetAllocator();
    d.AddMember( "uid", uid(), a );
    d.AddMember( "type", stype(), a );

    if ( sceneList_ && !sceneList_->empty() ) {
        d.AddMember( "countScene", sceneList_->count(), a );
        // not only order, a list also
        Document nd( kArrayType );
        for (const auto &pd : sceneList_->dps()) {
            nd.PushBack( pd.first, a );
        }
        d.AddMember( "orderScene", nd, a );
    }

    return d;
}




Tower::ptr_t
Tower::fromJSON( const JSON &json, parent_t parent ) {

    using namespace rapidjson;

    ASSERT( json.isObject() );

    DBG_STRING( "Tower::fromJSON()\n", std::to_string( json ) );

    const std::string stype = json.getSType( stypeStatic() );
    const uid_t uid = json.getUID();
    const Tower::ptr_t r = std::make_shared< Tower >( uid, parent );
    runWhenHas( json, "Scene", [ &r ] ( const std::vector< uid_t > &uids ) {
        r->addSceneList( uids );
    } );

    return r;
}




void
Tower::initSceneList() {

    if ( !sceneList_ ) {
        sceneList_.reset( new LazyList< Scene >( cast< D >(), nullptr ) );
    }
}




void
Tower::addSceneList( const std::vector< uid_t > &uids ) {

    ASSERT( !uids.empty() );

    initSceneList();
    sceneList_->add( uids );
}


} // wquest
