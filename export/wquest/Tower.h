#pragma once

#include "D.h"
#include "common.h"
#include "Scene.h"
#include "LazyList.h"


namespace wquest {


/**
 * @brief Scenes for choices by provisos.
 * @details Don't wrap `Scene` to `Tower` for simplifer using.
 * @see Scene
 */
class Tower : public D {
public:
    WQUEST_DECL_CAST_AND_PTR( Tower )

    /**
     * @brief Use `dm().emitD< Tower >()` instead of.
     */
    Tower() = delete;

    /**
     * @warning Has 1 scene after created.
     */
    Tower( uid_t, const parent_t );

    virtual Type type() const override {
        return etypeStatic();
    }

    virtual std::string stype() const override {
        return stypeStatic();
    }

    static std::string stypeStatic() {
        return std::to_string( etypeStatic() );
    }

    static Type etypeStatic() {
        return Type::Tower;
    }

    bool operator==( const Tower & ) const;


    WQUEST_BUILD_LAZY_LIST_METHODS( Scene, scene )


    /**
     * @brief
     * When indicated then starts this tower only after `from` was involved.
     */
    void chainFrom( const Tower::wptr_t from ) {
        chainFrom_ = from;
    }

    Tower::wptr_t chainFrom() const {
        return chainFrom_;
    }


    virtual json_t toJSON( rapidjson::Document *outer ) const override;

    /**
     * @warning Not uses `DM::emit()`.
     */
    static Tower::ptr_t fromJSON( const JSON &, parent_t );


private:
    void initSceneList();

    void addSceneList( const std::vector< uid_t > & );


private:
    Tower::wptr_t chainFrom_;
    LazyList< Scene >::ptr_t sceneList_;
};


} // wquest
