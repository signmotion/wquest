#pragma once

#include "JSON_T.h"
#include "dbgMacros.h"

#define RAPIDJSON_HAS_STDSTRING 1
#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/stringbuffer.h>


namespace wquest {


/**
 * \brief Wrapper for RapidJSON library.
 */
class RapidJSON : public JSON_T< rapidjson::Document, rapidjson::Value > {
public:
    explicit RapidJSON( const rapidjson::Document *doc ) :
        JSON_T( doc ) {
    }


    virtual bool has( const std::string &name ) const override {
        return doc().HasMember( name );
    }


    virtual bool isObject() const override {
        return doc().IsObject();
    }

    virtual bool isArray() const override {
        return doc().IsArray();
    }

    virtual bool isString() const override {
        return doc().IsString();
    }

    virtual bool isInt() const override {
        return doc().IsInt();
    }

    virtual bool isReal() const override {
        return doc().IsFloat() || doc().IsDouble();
    }


    virtual bool isObject( const std::string &name ) const override {
        ASSERT( has( name ) );
        return doc()[ name ].IsObject();
    }

    virtual bool isArray( const std::string &name ) const override {
        ASSERT( has( name ) );
        return doc()[ name ].IsArray();
    }

    virtual bool isString( const std::string &name ) const override {
        ASSERT( has( name ) );
        return doc()[ name ].IsString();
    }

    virtual bool isInt( const std::string &name ) const override {
        ASSERT( has( name ) );
        return doc()[ name ].IsInt();
    }

    virtual bool isReal( const std::string &name ) const override {
        ASSERT( has( name ) );
        return doc()[ name ].IsFloat() || doc()[ name ].IsDouble();
    }


    virtual std::string getName(
            const std::string &defaultName = "" ) const override {
        static const std::string name = "name";
        ASSERT( !defaultName.empty() || hasString( name ) );
        if ( !hasString( name ) ) {
            return defaultName;
        }
        const std::string r = doc()[ name ].GetString();
        // \todo Assert for correct name. See `Name`.
        return r;
    }

    virtual std::string getSType(
            const std::string &expectedType = "" ) const override {
        static const std::string name = "type";
        VERIFY( hasString( name ) );
        const std::string stype = doc()[ name ].GetString();
        VERIFY( expectedType.empty() || (expectedType == stype) );
        return stype;
    }

    virtual uid_t getUID(
            uid_t defaultUID = get::undefined< uid_t >() ) const override {
        static const std::string name = "uid";
        ASSERT( is::defined( defaultUID ) || hasInt( name ) );
        if ( !hasInt( name ) ) {
            return defaultUID;
        }
        const uid_t uid = doc()[ name ].GetInt();
        ASSERT( is::correct( uid ) );
        return uid;
    }


    virtual const rapidjson::Value &getValue(
            const std::string &name ) const override {
        ASSERT( has( name ) );
        return doc()[ name ];
    }

    virtual std::vector< int > getArrayInt(
            const std::string &name,
            std::vector< int > defaultValue = {} ) const override;

    virtual std::string getString(
            const std::string &name,
            const std::string &defaultValue = "" ) const override {
        ASSERT( is::defined( defaultValue ) || hasString( name ) );
        return hasString( name ) ?
                    doc()[ name ].GetString() :
                    defaultValue;
    }

    virtual int getInt(
            const std::string &name,
            int defaultValue = get::undefined< int >() ) const override {
        ASSERT( is::defined( defaultValue ) || hasInt( name ) );
        return hasInt( name ) ?
                    doc()[ name ].GetInt() :
                    defaultValue;
    }

    virtual float getReal(
            const std::string &name,
            float defaultValue = get::undefined< float >() ) const override {
        ASSERT( is::defined( defaultValue ) || hasReal( name ) );
        if ( !hasReal( name ) ) {
            return defaultValue;
        }
        return doc()[ name ].IsFloat() ?
                    doc()[ name ].GetFloat() :
                    doc()[ name ].GetDouble();
    }


    virtual std::string toPrettyJSON() const override;
};


} // wquest
