#pragma once

#define WIN32_LEAN_AND_MEAN

#include "dbgMacros.h"
#include <math.h>
#include <string>
#include <list>
#include <vector>
#include <set>
#include <map>
#include <memory>
#include <chrono>
#include <climits>
#include <algorithm>
#include <sstream>
#include <codecvt>
#include <stdint.h>
#include <atomic>
#include <filesystem>


namespace wquest {


/// \see is::correct( uid_t )
typedef int uid_t;
typedef std::string suid_t;

// \todo optimize Change to `unordered_set`.
typedef std::set< int > uids_t;


/// \brief For iterate and access by index.
/// \see is::undefined( int )
typedef int index_t;


typedef struct coord_t {
    float x, y;
} coord_t;


typedef struct coordI_t {
    int x, y;
} coordI_t;



// \brief Count of ticks from start world.
typedef int pulse_t;


typedef std::set< std::string > files_t;

typedef std::string name_t;

typedef std::set< name_t > names_t;

// \brief The general name.
typedef std::string gname_t;

typedef std::map< gname_t, names_t > mapNames_t;


class D;


/// Shared pointer for structure.
typedef std::shared_ptr< D > dptr_t;

typedef std::list< dptr_t > dptrs_t;

/// Weak pointer for structure.
typedef std::weak_ptr< D > dwptr_t;

typedef std::list< dwptr_t > dwptrs_t;


/// @brief Must be `0` or above.
typedef std::atomic< int > currentProgress_t;
/// @warning Must be above `0`.
typedef std::atomic< int > limitProgress_t;
/// @brief Progress for some operations.
typedef std::pair< currentProgress_t, limitProgress_t > progress_t;




namespace count {

// \brief Returns an indents of char.
int indent( const std::string &, char = ' ' );

} // count




namespace get {

/// \breaf Конвертер UTF8 / UTF16.
/// \thanks http://stackoverflow.com/a/18597384/963948
/// \code
///     converter_t  converter;
///     const std::string  narrow = converter.to_bytes( L"Хорошего дня!" );
///     const std::wstring   wide = converter.from_bytes( narrow );
/// \endcode
typedef std::wstring_convert< std::codecvt_utf8_utf16< wchar_t > > converter_t;


/**
 * @brief Get a string content from the file.
 */
std::string content( const std::string &filePath );


std::string fileExtension( const std::string &filePath );


std::string fileName( const std::string &filePath );


/// \brief Returns a current time.
/// \see make::dateTime()
inline std::clock_t now() { return std::clock(); }


std::string pathWithoutRelative( const std::string &filePath );


std::vector< std::string > split( const std::string & );


template< typename T >
inline T undefined() { return std::numeric_limits< T >::max(); }


template<>
inline char undefined< char >() { return '?'; }


template<>
inline std::string undefined< std::string >() { return "?"; }


} // get




namespace is {

bool boolean( const std::string & );

inline bool between01( float v ) { return (v >= 0.0f) && (v <= 1.0f); }

template< typename V, typename A, typename B >
inline bool between( V v, A a, B b ) { return (v >= (V) a) && (v <= (V) b); }

// \todo Don't work for non-unicode languages. See TestCommon.
bool capitalizeFirst( const std::string & );

// \todo Don't work for non-unicode languages. See TestCommon.
bool capitalizeFirst( const std::wstring & );

bool correct( uid_t );

bool digit10( char );

bool empty( char );

bool empty( float, float epsilon = 0.00001f );

bool empty( int );

bool empty( char * );

bool empty( const std::string & );

bool empty( const std::string_view );


template< class T >
inline bool empty( const std::vector< T > &v ) { return v.empty(); }

template< class T >
inline bool empty( const std::shared_ptr< T > &v ) { return v || empty( v ); }


bool equals( float a, float b, float p = std::numeric_limits< float >::epsilon());

bool equals( int a, int b );

bool equals( float a, int b );

bool equals( int a, float b );

/// \see distanceNear()
template< class T >
bool distanceFar( T a, T b, float allowableDistance );

bool name( const std::string & );

/// \see distanceFar()
template< class T >
bool distanceNear( T a, T b, float allowableDistance );

bool startsWith( const std::string &haystack, const std::string &needle );

bool undefined( char );

bool undefined( float );

bool undefined( int );

bool undefined( long );

bool undefined( unsigned long );

bool undefined( long long );

bool undefined( unsigned long long );

bool undefined( const std::string & );

bool undefined( const std::string_view );

bool undefined( uint32_t );


template< class T >
inline bool undefined( const std::vector< T > &v ) { return v.empty(); }


template< class T >
inline bool undefined( const std::shared_ptr< T > &v ) { return !v || undefined( v ); }


inline bool defined( char v ) { return !undefined( v ); }


inline bool defined( float v ) { return !undefined( v ); }


inline bool defined( int v ) { return !undefined( v ); }


inline bool defined( long v ) { return !undefined( v ); }


inline bool defined( unsigned long v ) { return !undefined( v ); }


inline bool defined( long long v ) { return !undefined( v ); }


inline bool defined( unsigned long long v ) { return !undefined( v ); }


inline bool defined( const std::string &v ) { return !undefined( v ); }


inline bool defined( const std::string_view v ) { return !undefined( v ); }


inline bool defined( uint32_t v ) { return !undefined( v ); }


template< class T >
inline bool defined( const std::vector< T > &v ) { return !undefined( v ); }


template< class T >
inline bool defined( const std::shared_ptr< T > &v ) { return !undefined( v ); }

} // is




// \see namespace `parse`
namespace make {


/// \see packNumber(), unpackNumber()
/// \warning some symbols was removed for easy reading: `o`, `l`, `O`
static const char packDigits[] = "0123456789abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ";


template< typename T >
inline std::string packNumber( T v, int base = get::undefined< int >(), int minSize = 1 ) {

    ASSERT( !is::undefined( v ));
    ASSERT( v >= 0 );
    static constexpr int minBase = 2;
    static constexpr int maxBase = sizeof( packDigits ) / sizeof( packDigits[ 0 ] );
    if ( is::undefined( base )) {
        base = maxBase;
    }
    ASSERT((base >= minBase) && (base <= maxBase));

    std::string r;
    while ((v != 0) || ((int) r.size() < minSize)) {
        r += packDigits[ v % base ];
        v /= base;
    }
    std::reverse( r.begin(), r.end());

    return r;
}


template< typename T >
inline T unpackNumber( const std::string &s, int base = get::undefined< int >()) {

    ASSERT( !s.empty());
    static constexpr int minBase = 2;
    static constexpr int maxBase = sizeof( packDigits ) / sizeof( packDigits[ 0 ] );
    if ( is::undefined( base )) {
        base = maxBase;
    }
    ASSERT((base >= minBase) && (base <= maxBase));

    const auto lookAtNumber = []( char ch ) -> int {
        for ( int k = 0; k < maxBase; ++k ) {
            if ( packDigits[ k ] == ch ) {
                return k;
            }
        }
        return get::undefined< int >();
    };

    // str[ length - 1 ] * 1 + str[ length - 2 ] * (base ^ 1) + str[ length - 3 ] * (base ^ 2) + ...
    unsigned long long power = 1;
    T v = T( 0 );
    for ( int i = s.length() - 1; i >= 0; --i ) {
        const int b = lookAtNumber( s[ i ] );
        if ( is::undefined( b )) {
            ASSERT( false && "Invalid symbol by parse a string" );
            return get::undefined< T >();
        }
        v += (T) (b * power);
        power *= base;
    }

    return v;
}


// \todo Don't work for non-unicode languages. See TestCommon.
std::string &capitalizeFirst( std::string & );

// \todo Don't work for non-unicode languages. See TestCommon.
std::wstring &capitalizeFirst( std::wstring & );

// \todo Don't work for non-unicode languages. See TestCommon.
std::string capitalizeFirstCopy( const std::string & );

// \todo Don't work for non-unicode languages. See TestCommon.
std::wstring capitalizeFirstCopy( const std::wstring & );


std::string &trim( std::string & );

std::string trimCopy( const std::string & );

std::string &ltrim( std::string & );

std::string &rtrim( std::string & );


/**
 * @brief The string shortens to a given `length`.
 */
std::string &shorter(
    std::string &,
    int length,
    const std::string &tail = "..." );

std::string shorterCopy(
    const std::string &,
    int length,
    const std::string &tail = "..." );


/**
 * @brief Changes `[any name]` to `{}` for FMT-library.
 * @see ProvisoWhenDistance2Model::name()
 */
std::string emptyBraces( const std::string & );


} // make




namespace math {


/**
 * \brief Golden ratio constant.
 * \details
 * a / b = (a + b) / a
 * 62% & 38%
 * a : b = b : (a - b)
 */
static constexpr float goldenRatio162 = 1.6180339887f;

static constexpr float goldenRatio138 = 3.0f - goldenRatio162;

static constexpr float goldenRatio62 = goldenRatio162 - 1.0f;

static constexpr float goldenRatio38 = 2.0f - goldenRatio162;


// \brief Clamp to [ 0; 1 ].
inline float clamp01Copy( float v ) {
    return (v < 0.0f) ? 0.0f : ((v > 1.0f) ? 1.0f : v);
}


// \brief Clamp to [ -1; 1 ].
inline float clamp1Copy( float v ) {
    return (v < -1.0f) ? -1.0f : ((v > 1.0f) ? 1.0f : v);
}


// \brief Clamp to [ a; b ].
template< typename T = float >
inline T clampCopy( T v, T a, T b ) {
    return (v < a) ? a : ((v > b) ? b : v);
}


template< typename T = float >
inline void clamp( T *v, T a, T b ) {
    if ( *v < a ) { *v = a; }
    else if ( *v > b ) { *v = b; }
}


template< typename T = float >
inline void clampLow( T *v, T low ) {
    if ( *v < low ) { *v = low; }
}


template< typename T = float >
inline void clampHigh( T *v, const T &high ) {
    if ( *v > high ) { *v = high; }
}


// \see iround()
template< int digits = 0, typename Input = float, typename Output = float >
inline Output round( Input v ) {
    // @todo optimize Replace by compile-time pow().
    const float p = powf( 10, digits );
    return (Output) (roundf( v * p ) * p);
}


// \see round<>()
template< typename Input = float >
inline int iround( Input v ) {
    return round< 0, Input, int >( v );
}


/// Linearly interpolate (or extrapolate) between `a` and `b` by `t`%.
inline float lerp( float a, float b, float t ) {
    return a * (1.0f - t) + b * t;
}


/// Linearly interpolate from `a` to `b` by no more than `d`.
inline float lerpconst( float a, float b, float d ) {
    return a + clampCopy( b - a, -d, d );
}


// \return Integer number 'num' in power 'p'.
// \example 3 ^ 10 = pow< 3, 10>::value;
template< int num, size_t p >
struct pow {
    enum {
        value = num * pow< num, p - 1 >::value
    };
};
template< int num >
struct pow< num, 0 > {
    enum {
        value = 1
    };
};


/// \return [a, b]
int random( int a, int b );

float random( float a, float b );

float random01();


// \brief Convert 1D-coord to 2D.
coordI_t coordITo2D( index_t i, int width );

// \brief Convert 2D-coord to 1D.
index_t coord2DToI( const coordI_t &c, int width );

index_t coord2DToI( int x, int y, int width );


} // math




// \see namespace 'make'
namespace parse {

bool boolean( const std::string & );

int numberInteger( const std::string & );

float numberFloat( const std::string & );

uid_t uid( const std::string & );

std::pair< int, int > pairInteger( const char *source, char delimiter );

std::pair< float, float > pairFloat( const char *source, char delimiter );

} // parse

} // ri




namespace std {

template< typename A, typename B >
static std::ostream &
operator<<( std::ostream &out, const std::pair< A, B > &p ) {
    return out << p.first << " " << p.second;
}


} // wquest
