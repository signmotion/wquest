#include "MorpherCompare.h"
#include "common.h"


namespace wquest {


MorpherCompare::MorpherCompare( const std::string &tcs ) :
    typeCompare_( TypeCompare::Undefined )
{
    const std::string t = make::trimCopy( tcs );
    if (t == "less") {
        typeCompare_ = TypeCompare::Less;
    }
    else if (t == "more") {
        typeCompare_ = TypeCompare::More;
    }
}




MorpherCompare::MorpherCompare( TypeCompare tc ) :
    typeCompare_( tc )
{
    if ((typeCompare_ != TypeCompare::Less) && (typeCompare_ != TypeCompare::More)) {
        typeCompare_ = TypeCompare::Undefined;
    }
}




MorpherCompare::operator std::string() const {
    return std::to_string( *this );
}




bool
MorpherCompare::valid() const {
    return is::correct( typeCompare_ ) && is::defined( typeCompare_ );
}


} // wquest




namespace std {


std::string
to_string( const wquest::MorpherCompare &m ) {

    using namespace wquest;

    const TypeCompare tc = m.typeCompare();
    if (tc == TypeCompare::Less) {
        return "less";
    }
    if (tc == TypeCompare::More) {
        return "more";
    }

    return "";
}


} // std
