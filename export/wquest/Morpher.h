#pragma once

#include "Cast.h"
#include "dbgMacros.h"


namespace wquest {


/**
 * @brief Abstract class for build a morpher.
 * @see Proviso
 */
class Morpher : public std::enable_shared_from_this< Morpher > {
public:
    WQUEST_DECL_CAST_AND_PTR( Morpher )


protected:
    Morpher();


public:
    virtual ~Morpher() {
    }

    virtual operator std::string() const = 0;

    /**
     * @brief `true` then contains a permissible value.
     */
    virtual bool valid() const = 0;

    bool invalid() const {
        return !valid();
    }

    /**
     * @brief Use this for cozy print a morpher for some declares.
     * @details Returns a value then morpher is valid and brases overwise.
     * @see Proviso::declare()
     */
    static std::string stringOrBraces( const Morpher::ptr_t );
};




namespace is {


bool valid( const Morpher::ptr_t );

inline bool invalid( const Morpher::ptr_t m ) {
    return !valid( m );
}


}  // is


} // wquest




namespace std {

std::string to_string( const wquest::Morpher & );

} // std
