#pragma once

#include "Morpher.h"
#include "TypeCompare.h"


namespace wquest {


class MorpherCompare : public Morpher {
public:
    WQUEST_DECL_CAST_AND_PTR( MorpherCompare )


public:
    MorpherCompare() = delete;

    /**
     * @warning Throw a type in lower case only according to `TypeCompare`.
     */
    explicit MorpherCompare( const std::string & );

    explicit MorpherCompare( TypeCompare );

    virtual ~MorpherCompare() override {
    }

    operator std::string() const override;

    virtual bool valid() const override;

    TypeCompare typeCompare() const {
        return typeCompare_;
    }


private:
    TypeCompare typeCompare_;
};


} // wquest




namespace std {

std::string to_string( const wquest::MorpherCompare & );

} // std
