#include "MorpherInstance.h"


namespace wquest {


MorpherInstance::MorpherInstance( const std::string &name ) :
    MorpherInstance( Name{ name } )
{
}




MorpherInstance::MorpherInstance( const Name &name ) :
    name_( name )
{
}




MorpherInstance::operator std::string() const {
    return std::to_string( *this );
}




bool
MorpherInstance::valid() const {
    return name_.valid();
}


} // wquest




namespace std {


std::string
to_string( const wquest::MorpherInstance &m ) {
    return std::to_string( m.name() );
}


} // std
