#pragma once

#include "Morpher.h"
#include "Name.h"


namespace wquest {


class MorpherInstance : public Morpher {
public:
    WQUEST_DECL_CAST_AND_PTR( MorpherInstance )


public:
    MorpherInstance() = delete;

    explicit MorpherInstance( const std::string &name );

    explicit MorpherInstance( const Name & );

    virtual ~MorpherInstance() override {
    }

    operator std::string() const override;

    virtual bool valid() const override;

    const Name &name() const {
        return name_;
    }


private:
    Name name_;
};


} // wquest




namespace std {

std::string to_string( const wquest::MorpherInstance & );

} // std
