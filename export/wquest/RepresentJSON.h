#pragma once

#include "Represent.h"
#include "dbgMacros.h"
#include "JSON.h"

#define RAPIDJSON_HAS_STDSTRING 1
#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/stringbuffer.h>

#include <vector>


namespace wquest {


// \todo Remove `json_t`. Use class `JSON`.
typedef rapidjson::Document  json_t;
typedef std::string  sjson_t;
typedef std::vector< unsigned char >  sjsonUTF8_t;




json_t toJSON( const sjson_t&, std::string* errors = nullptr );

sjson_t toPrettyJSON( const json_t& );




/**
 * \see wquest::JSON
 */
class RepresentJSON : public Represent {
public:
    RepresentJSON() {
    }

    virtual ~RepresentJSON() {
    }

    /* - No, client use `nullptr` clearly.
    json_t toJSON() const {
        return toJSON( nullptr );
    }
    json_t toSJSON() const {
        return toSJSON( nullptr );
    }
    */

    virtual json_t toJSON( rapidjson::Document *outer ) const = 0;

    sjson_t toSJSON( rapidjson::Document *outer ) const {
        return toPrettyJSON( toJSON( outer ) );
    }

    template< class T >
    static std::shared_ptr< T > fromJSON( const sjson_t & ) {
        ASSERT( false && "Define it for child." );
        return nullptr;
    }
};


} // wquest




namespace std {

std::string to_string( const wquest::json_t & );

std::ostream &operator<<( std::ostream &, const wquest::json_t & );

} // std
