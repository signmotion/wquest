#include "RepresentJSON.h"
#include "common.h"


namespace wquest {


json_t
toJSON( const sjson_t& s, std::string* errors ) {

    using namespace rapidjson;

    Document  d;
    d.Parse( s.c_str() );
    if ( d.HasParseError() ) {
        if ( errors ) {
            *errors += "JSON parse error on position " +
                std::to_string( d.GetErrorOffset() ) + ": " +
                std::to_string( d.GetParseError() ) + ".";
        }
        return Document{};
    }

    return d;
}




sjson_t
toPrettyJSON( const json_t& v ) {

    using namespace rapidjson;

    StringBuffer  sb;
    PrettyWriter< StringBuffer >  w( sb );
    w.SetIndent( ' ', 2 );
    w.SetFormatOptions( kFormatSingleLineArray );
    v.Accept( w );

    return sb.GetString();
}


} // wquest




namespace std {


std::string
to_string( const wquest::json_t& json ) {
    return wquest::toPrettyJSON( json );
}


std::ostream &
operator<<( std::ostream &o, const wquest::json_t &json ) {
    return o << to_string( json );
}


} // std
