#pragma once

#include "D.h"
#include "Chapter.h"
#include "Impact.h"
#include "Apron.h"
#include "Proviso.h"
#include "LazyList.h"


namespace wquest {


/**
 * @brief Every quest declaration has one or more scenes.
 * @details Don't wrap `Scene` to `Tower` for simplifer using.
 * @warning Always has one scene minimum after created.
 * @see Tower
 */
class Scene : public D {
public:
    WQUEST_DECL_CAST_AND_PTR( Scene )


public:
    /**
     * @brief Use `dm().emitD< Scene >()` instead of.
     */
    Scene() = delete;

    explicit Scene( uid_t, const parent_t );

    virtual Type type() const override {
        return etypeStatic();
    }

    virtual std::string stype() const override {
        return stypeStatic();
    }

    static std::string stypeStatic() {
        return std::to_string( etypeStatic() );
    }

    static Type etypeStatic() {
        return Type::Scene;
    }

    bool operator==( const Scene & ) const;

    Chapter::ptr_t chapter();

    const Impact::ptrs_t &impacts() const;

    Apron::ptr_t apron();


    WQUEST_BUILD_LAZY_LIST_METHODS_WITHOUT_EMIT( Proviso, proviso )

    template< class T >
    Proviso::ptr_t addProvisoFront() {
        const Proviso::ptr_t proviso = dm().emitD< T >( cast< D >() );
        addProvisoFront( proviso );
        return proviso;
    }

    template< class T >
    Proviso::ptr_t addProvisoBack() {
        const Proviso::ptr_t proviso = dm().emitD< T >( cast< D >() );
        addProvisoBack( proviso );
        return proviso;
    }

    template< class T >
    Proviso::ptr_t addProviso( index_t i ) {
        ASSERT( i >= 0 );
        ASSERT( i <= provisoList_->count() );
        const Proviso::ptr_t proviso = dm().emitD< T >( cast< D >() );
        addProviso( i, proviso );
        return proviso;
    }


    virtual json_t toJSON( rapidjson::Document *outer ) const override;

    /**
     * @warning Not uses `DM::emit()`.
     */
    static Scene::ptr_t fromJSON( const JSON &, parent_t );


private:
    void initProvisoList();

    void addProvisoList( const std::vector< uid_t > & );


private:
    Chapter::ptr_t chapter_;
    Impact::ptrs_t impacts_;
    Apron::ptr_t apron_;
    LazyList< Proviso >::ptr_t provisoList_;
};


} // wquest
