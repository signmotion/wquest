#pragma once

#include "dbgMacros.h"
#include <array>
#include <string>


namespace wquest {


static const std::string UNDEFINED_TYPE_PREFIX = "U";


/**
  * \warning Must be accordance with 'strEnumType'.
  * \see is::correct( Type )
  * \see UNDEFINED_TYPE_PREFIX
  */
enum class Type {
    Undefined = 0,
    // common (low level) type
    D,
    // child type from `D`
    Annotation,
    Apron,
    Chapter,
    Impact,
    Proviso,
    QD,
    Scene,
    Title,
    Tower,
    World,
    // count of base types
    count,
    // user types
    lowerBarUserType = 1000
    // define its after `lowerBarUserType`
};


/**
 * \brief For the humanly output.
 * \warning Must be accordance with 'enum Type'.
 */
static const std::array< std::string, ( int )Type::count > strEnumType = {
    "Undefined",
    "D",
    "Annotation",
    "Apron",
    "Chapter",
    "Impact",
    "Proviso",
    "QD",
    "Scene",
    "Title",
    "Tower",
    "World",
};




namespace is {


/**
 * @return `true` when have a correct *base type* and type is defined.
 * @warning For any *user type* we was return `false`.
 */
inline bool correct( Type type ) {
    return (type > Type::Undefined) && (type < Type::count);
}


inline bool undefined( Type type ) {
    return (type == Type::Undefined);
}


inline bool defined( Type type ) {
    return !undefined( type );
}


} // is


} // wquest




namespace std {

std::string to_string( wquest::Type );


/**
 * @warning Case-sensetivity.
 */
template< class T >
T from_string( const std::string & );

} // std
