#pragma once

#include "D.h"


namespace wquest {


/**
 * @see Scene
 */
class Chapter : public D {
public:
    WQUEST_DECL_CAST_AND_PTR( Chapter )

    Chapter() = delete;

    Chapter( uid_t, const std::string &name, const parent_t );

    virtual Type type() const override {
        return etypeStatic();
    }

    virtual std::string stype() const override {
        return stypeStatic();
    }

    static std::string stypeStatic() {
        return std::to_string( etypeStatic() );
    }

    static Type etypeStatic() {
        return Type::Chapter;
    }

    bool operator==( const Chapter & ) const;

    const std::string &name() const {
        return name_;
    }

    void name( const std::string & );

    virtual json_t toJSON( rapidjson::Document *outer ) const override;

    /**
     * @warning Not uses `DM::emit()`.
     */
    static Chapter::ptr_t fromJSON( const json_t &, parent_t );


private:
    std::string name_;
};


} // wquest
