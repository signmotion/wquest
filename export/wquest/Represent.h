#pragma once

#include "Cast.h"


namespace wquest {


/**
 * \brief An abstract representer element to / from any format.
 * \see RepresetJSON
 */
class Represent : public std::enable_shared_from_this< Represent > {
public:
    WQUEST_DECL_CAST_AND_PTR( Represent )


public:
    Represent() {
    }

    virtual ~Represent() {
    }
};


} // wquest
