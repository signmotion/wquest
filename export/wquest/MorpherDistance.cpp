#include "MorpherDistance.h"
#include "common.h"
#include <fmt/core.h>
#include <string>


namespace wquest {


MorpherDistance::MorpherDistance( float distance ) :
    distance_( distance )
{
}




MorpherDistance::operator std::string() const {
    return std::to_string( *this );
}




bool
MorpherDistance::valid() const {
    return (distance_ >= 0.0f) && is::defined( distance_ );
}


} // wquest




namespace std {


std::string
to_string( const wquest::MorpherDistance &m ) {

    const float d = m.distance();
    const std::string r =
            wquest::is::undefined( d ) ?
                "?" :
                fmt::format( "{:.0f}", m.distance() );

    return r;
}


} // std
