#include "Store.h"
#include "D.h"


namespace wquest {


Store::Store() {

    typeConverterToS( nullptr );
}




Store::~Store() {

    flush();
    wait();
}




void
Store::typeConverterToS( typeConverterToS_t typeConverterToS ) {

    typeConverterToS_ = typeConverterToS;
    if ( !typeConverterToS_ ) {
        typeConverterToS_ = [] ( Type type ) {
            return std::to_string( type );
        };
    }
}


} // wquest
