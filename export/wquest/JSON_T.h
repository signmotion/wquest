#pragma once

#include "dbgMacros.h"
#include "common.h"

#include <string>
#include <vector>


namespace wquest {


/**
 * \brief Wrapper for JSON library.
 * \see RapidJSON
 */
template< class Doc, class Val >
class JSON_T {
public:
    explicit JSON_T( const Doc *doc ) :
        doc_( *doc ) {
        ASSERT( doc );
    }

    virtual ~JSON_T() {
    }


    // \todo optimize Replace `string` to `string_view`.
    virtual bool has( const std::string &name ) const = 0;

    /**
     * \brief `has()` and `is*()` in one line.
     * \code
     * bool q = hasInt( "field" );
     * \endcode
     */
    bool hasString( const std::string &name ) const {
        return has( name ) && isString( name );
    }

    bool hasInt( const std::string &name ) const {
        return has( name ) && isInt( name );
    }

    bool hasReal( const std::string &name ) const {
        return has( name ) && isReal( name );
    }


    virtual bool isObject() const = 0;

    virtual bool isArray() const = 0;

    virtual bool isString() const = 0;

    virtual bool isInt() const = 0;

    virtual bool isReal() const = 0;


    // \todo optimize Replace `string` to `string_view`.
    virtual bool isObject( const std::string &name ) const = 0;

    virtual bool isArray( const std::string &name ) const = 0;

    virtual bool isString( const std::string &name ) const = 0;

    virtual bool isInt( const std::string &name ) const = 0;

    virtual bool isReal( const std::string &name ) const = 0;


    /**
     * @brief Get a value from field named `name`.
     */
    virtual std::string getName(
            const std::string &defaultName = "" ) const = 0;

    /**
     * @brief Get a value from field named `type` and verify it
     * with `expectedType` when `expectedType` is present.
     */
    virtual std::string getSType(
            const std::string &expectedType = "" ) const = 0;

    /**
     * @brief Get a value from field named `uid`.
     */
    virtual uid_t getUID(
            uid_t defaultUID = get::undefined< uid_t >() ) const = 0;


    // \todo optimize Replace `string` to `string_view`.
    virtual const Val &getValue(
            const std::string &name ) const = 0;

    virtual std::vector< int > getArrayInt(
            const std::string &name,
            std::vector< int > defaultValue = {} ) const = 0;

    virtual std::string getString(
            const std::string &name,
            const std::string &defaultValue = "" ) const = 0;

    virtual int getInt(
            const std::string &name,
            int defaultValue = get::undefined< int >() ) const = 0;

    virtual float getReal(
            const std::string &name,
            float defaultValue = get::undefined< float >() ) const = 0;


    virtual std::string toPrettyJSON() const = 0;


    const Doc &doc() const {
        return doc_;
    }


private:
    const Doc &doc_;
};


} // wquest




namespace std {


template< class Doc, class Val >
std::string
to_string( const wquest::JSON_T< Doc, Val > &json ) {
    return json.toPrettyJSON();
}


template< class Doc, class Val >
std::ostream &
operator<<( std::ostream &o, const wquest::JSON_T< Doc, Val > &json ) {
    return o << to_string( json );
}


} // std
