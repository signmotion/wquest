#include "Proviso.h"


namespace wquest {


Proviso::Proviso( uid_t uid, const parent_t parent ) :
    D( uid, parent )
{
}




Proviso::~Proviso() {
}




bool
Proviso::operator==( const Proviso &b ) const {
    return declare() == b.declare();
}




int
Proviso::countMorpher() const {

    constexpr char morpherSymbol = '[';
    const std::string d = declare();
    return std::count( d.cbegin(), d.cend(), morpherSymbol );
}




json_t
Proviso::toJSON( rapidjson::Document *outer ) const {

    using namespace rapidjson;

    Document d( kObjectType );
    Document::AllocatorType &a =
            outer ? outer->GetAllocator() : d.GetAllocator();
    d.AddMember( "uid", uid(), a );
    d.AddMember( "type", stype(), a );
    d.AddMember( "declare", declare(), a );
    if ( countDefine() > 0 ) {
        json_t morpher = jsonMorphers( &d );
        d.AddMember( "morpher", morpher, a );
    }

    return d;
}



/*
namespace is {


bool
valid( const Proviso::ptr_t p ) {
    return p && p->valid();
}


}  // is
*/

} // wquest
