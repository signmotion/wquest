#include "Morpher.h"


namespace wquest {


Morpher::Morpher() {
}




std::string
Morpher::stringOrBraces( const Morpher::ptr_t m ) {

    static const std::string undefined = "[]";
    return is::valid( m ) ? std::to_string( *m ) : undefined;
}



namespace is {


bool
valid( const Morpher::ptr_t m ) {
    return m && m->valid();
}


}  // is


} // wquest




namespace std {


std::string
to_string( const wquest::Morpher &m ) {
    return static_cast< std::string >( m );
}


} // std
