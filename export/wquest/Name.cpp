#include "Name.h"
#include "common.h"
#include <cctype>


namespace wquest {


Name::Name( const std::string &s ) :
    name_( s )
{
    // attempt will be made a correct name

    // remove the punctuations and digits
    name_.erase( std::remove_if( name_.begin(), name_.end(), [] ( char ch ) {
            return std::ispunct( ( unsigned char )ch ) ||
                   std::isdigit( ( unsigned char )ch );
        } ), name_.end() );

    make::trim( name_ );

    // capitalize a first letter
    // \todo Not correct work for non-unicode languages. See TestCommon.
    // make::capitalizeFirst( name_ );
}




Name::operator std::string() const {
    return std::to_string( *this );
}




bool
Name::valid() const {

    // not empty
    if ( name_.empty() ) {
        return false;
    }

    // without punctuations and digits
    /* - Was removed and trimmed in constructor.
    for ( const char ch : name_ ) {
        if ( std::ispunct( ( unsigned char )ch ) ||
             std::isdigit( ( unsigned char )ch ) )
        {
            return false;
        }
    }
    */

    // capitalized a first letter
    // \todo Not correct work for non-unicode languages. See TestCommon.
    //if ( !is::capitalizeFirst( name_ ) ) {
    //    return false;
    //}

    return is::defined( name_ );
}




namespace is {


bool
valid( const Name::ptr_t n ) {
    return n && n->valid();
}


}  // is


} // wquest




namespace std {


std::string
to_string( const wquest::Name& n ) {

    return n.name();
}


} // std
