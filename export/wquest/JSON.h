#pragma once

#include "RapidJSON.h"


namespace wquest {


/**
 * @brief Define a class for interchanging the JSON-documents.
 * @see JSON_T
 */
class JSON : public RapidJSON {
public:
    explicit JSON( const rapidjson::Document *doc ) :
        RapidJSON( doc ) {
    }
};


} // wquest
