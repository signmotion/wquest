#include "Impact.h"
#include "common.h"
#include "dbgMacros.h"


namespace wquest {


Impact::Impact(
        uid_t uid,
        const std::string &declaration,
        const parent_t parent
) :
    D( uid, parent ),
    declaration_( make::trimCopy( declaration ) )
{
    //ASSERT( !declaration_.empty() );
}




bool
Impact::operator==( const Impact &b ) const {
    return (uid() == b.uid()) &&
           (declaration_ == b.declaration_);
}





void
Impact::declaration( const std::string &declaration ) {
    declaration_ = make::trimCopy( declaration );
}




json_t
Impact::toJSON( rapidjson::Document *outer ) const {

    using namespace rapidjson;

    Document d( kObjectType );
    Document::AllocatorType &a =
            outer ? outer->GetAllocator() : d.GetAllocator();
    d.AddMember( "uid", uid(), a );
    d.AddMember( "type", stype(), a );
    d.AddMember( "declaration", declaration(), a );

    return d;
}


} // wquest
