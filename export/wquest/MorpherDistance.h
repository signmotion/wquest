#pragma once

#include "Morpher.h"


namespace wquest {


class MorpherDistance : public Morpher {
public:
    WQUEST_DECL_CAST_AND_PTR( MorpherDistance )


public:
    MorpherDistance() = delete;

    explicit MorpherDistance( float );

    virtual ~MorpherDistance() override {
    }

    operator std::string() const override;

    virtual bool valid() const override;

    float distance() const {
        return distance_;
    }


private:
    float distance_;
};


} // wquest




namespace std {

std::string to_string( const wquest::MorpherDistance & );

} // std
