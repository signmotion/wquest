#pragma once

#include "common.h"
#include "Type.h"
#include "RepresentJSON.h"
#include <mutex>
#include <future>


namespace wquest {


class D;


/**
 * @brief Abstract store.
 * @see ownTypeConverterToS()
 */
class Store {
public:
    typedef std::function< std::string( Type ) > typeConverterToS_t;

    mutable std::mutex mutex;


public:
    Store();

    virtual ~Store();

    /**
     * @brief Save data to the store.
     * @warning Use `flush()` and `wait()` for warranty save data.
     */
    virtual void emitD( const std::shared_ptr< D > ) = 0;

    /**
     * @brief Store hasn't any data.
     */
    virtual bool empty() const = 0;

    /**
     * @brief Remove all data from the store.
     */
    virtual void clear() = 0;


    /**
     * @brief Count data in the store.
     * @details Every data has an UID.
     */
    virtual int count() const = 0;

    /**
     * @brief Count data in the store by type.
     * @details Can override this method for better perfomance.
     */
    virtual int count( Type type, const std::shared_ptr< D > parent ) const {
        return get( type, parent ).size();
    }

    template< class T >
    int count( const std::shared_ptr< D > parent ) const {
        return count( T::etypeStatic(), parent );
    }


    /**
     * @brief Returns a content by `path`.
     * @details `path` means `id` to data.
     */
    virtual std::string content( const std::string &path ) const = 0;


    /**
     * @brief Load data like JSON from the store.
     * @todo optimize Returns like `std::future< json_t >`.
     */
    virtual json_t get( Type, const std::shared_ptr< D > parent, uid_t ) = 0;


    /**
     * @brief UIDs data from the store by type.
     * @details Direct depends from emitD().
     */
    virtual uids_t get( Type, const std::shared_ptr< D > parent ) const = 0;

    template< class T >
    uids_t get( const std::shared_ptr< D > parent ) const {
        return get( T::etypeStatic(), parent );
    }


    /**
     * @brief Save all unsaved data to the store.
     * @warning Use `wait()` for warranty save data.
     */
    virtual Store &flush() {
        return *this;
    }

    /**
     * @brief `true` when `flush()` in process.
     */
    virtual bool stillFlushing() const {
        return false;
    }

    virtual const progress_t &progressFlushing() const {
        static const progress_t p{ 1, 1 };
        return p;
    }

    /**
     * @brief Wait a thread operations.
     * @see flush()
     */
    virtual Store &wait() {
        return *this;
    }


    /**
     * @details Call this method if use own data types.
     * @see TestLazyList
     */
    void typeConverterToS( typeConverterToS_t );


protected:
    typeConverterToS_t typeConverterToS_;
};




Store &store();


} // wquest
