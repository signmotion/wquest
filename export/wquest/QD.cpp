#include "QD.h"
#include "DM.h"
#include "dbgMacros.h"


namespace wquest {


QD::QD( uid_t uid, const parent_t parent ) :
    D( uid, parent )
{
}




bool
QD::operator==( const QD &b ) const {

    /* \todo optimize
    return (uid() == b.uid()) &&
           (towers_ == b.towers_);
    */

    return toSJSON( nullptr ) == b.toSJSON( nullptr );
}




Title::ptr_t
QD::title( const std::string &text ) {

    title_ = dm().emitD< Title >( text, cast< D >() );
    return title_;
}




Title::ptr_t
QD::title() {
    if ( !title_ ) {
        title_ = dm().emitD< Title >( "", cast< D >() );
    }
    return title_;
}




Annotation::ptr_t
QD::annotation( const std::string &text ) {

    annotation_ = dm().emitD< Annotation >( text, cast< D >() );
    return annotation_;
}




Annotation::ptr_t
QD::annotation() {
    if ( !annotation_ ) {
        annotation_ = dm().emitD< Annotation >( "", cast< D >() );
    }
    return annotation_;
}




bool
QD::singleInParent( Type type ) const {

    switch ( type ) {
        case Type::Title :
            return true;

        case Type::Annotation :
            return true;

        case Type::Tower :
            return false;
    }

    ASSERT( false && "Type is not part of this class." );
    return true;
}




void
QD::moveScene(
        index_t iTowerFrom,
        index_t iSceneFrom,
        index_t iTowerTo,
        index_t iSceneTo
) {
    initTowerList();

    ASSERT( is::defined( iTowerFrom ) );
    ASSERT( iTowerFrom >= 0 );
    ASSERT( iTowerFrom < towerList_->count() );
    ASSERT( is::defined( iSceneFrom ) );
    ASSERT( iSceneFrom >= 0 );
    ASSERT( is::defined( iTowerTo ) );
    ASSERT( iTowerTo >= 0 );
    ASSERT( iTowerTo < towerList_->count() );
    ASSERT( is::defined( iSceneTo ) );
    ASSERT( iSceneTo >= 0 );

    //DBG_INT( "QD::moveScene() iTowerFrom", iTowerFrom );
    //DBG_INT( "QD::moveScene() iSceneFrom", iSceneFrom );
    //DBG_INT( "QD::moveScene() iTowerTo", iTowerTo );
    //DBG_INT( "QD::moveScene() iSceneTo", iSceneTo );

    // skip if have a same place
    if ( iTowerFrom == iTowerTo && iSceneFrom == iSceneTo ) {
        // do nothing
        return;
    }

    // find a moving scene in a tower
    ASSERT( false && "TODO" );
    /* - TODO
    Tower::ptr_t towerFrom = towerI( iTowerFrom );
    Scene::ptr_t sceneFrom = towerFrom->sceneI( iSceneFrom );
    // move a scene to other or same tower
    towerI( iTowerTo )->addScene( iSceneTo, *itSceneMoving );
    fromScenes.erase( itSceneMoving );
    if ( fromScenes.empty() ) {
        removeTower( iTowerFrom );
    }
    */
}




json_t
QD::toJSON( rapidjson::Document *outer ) const {

    using namespace rapidjson;

    Document d( kObjectType );
    Document::AllocatorType &a =
            outer ? outer->GetAllocator() : d.GetAllocator();
    d.AddMember( "uid", uid(), a );
    d.AddMember( "type", stype(), a );
    if ( towerList_ && !towerList_->empty() ) {
        d.AddMember( "countTower", towerList_->count(), a );
        // not only order, a list also
        Document nd( kArrayType );
        for (const auto &pd : towerList_->dps()) {
            nd.PushBack( pd.first, a );
        }
        d.AddMember( "orderTower", nd, a );
    }

    return d;
}




QD::ptr_t
QD::fromJSON( const JSON &json, parent_t parent ) {

    using namespace rapidjson;

    ASSERT( json.isObject() );

    DBG_STRING( "QD::fromJSON()\n", std::to_string( json ) );

    const std::string stype = json.getSType( stypeStatic() );
    const uid_t uid = json.getUID();
    const QD::ptr_t r = std::make_shared< QD >( uid, parent );
    runWhenHas( json, "Tower", [ &r ] ( const std::vector< uid_t > &uids ) {
        r->addTowerList( uids );
    } );

    return r;
}




void
QD::initTowerList() {

    if ( !towerList_ ) {
        towerList_.reset( new LazyList< Tower >( cast< D >(), nullptr ) );
    }
}




void
QD::addTowerList( const std::vector< uid_t > &uids ) {

    ASSERT( !uids.empty() );

    initTowerList();
    towerList_->add( uids );
}


} // wquest
