#include "common.h"
#include <random>
#include <iterator>
#include <ctype.h>
#include <cctype>
#include <cwctype>
#include <fstream>


namespace wquest::count {

int
indent( const std::string &s, char ch ) {
    int i = 0;
    for ( ; s[ i ] == ch; ++i ) {
    }
    return i;
}

} // wquest::count




namespace wquest::get {


std::string content( const std::string &filePath ) {
    namespace fs = std::filesystem;
    ASSERT( !filePath.empty() );
    ASSERT( fs::exists( filePath ) );
    ASSERT( fs::is_regular_file( filePath ) );
    // \todo optimize See https://stackoverflow.com/a/524843/963948
    std::ifstream ifs( filePath.c_str() );
    return std::string{
        std::istreambuf_iterator< char >( ifs ),
        std::istreambuf_iterator< char >() };
}


std::string fileExtension( const std::string &filePath ) {
    std::string r;
    const std::size_t pos = filePath.find_last_of( '.' );
    if ( pos != std::string::npos ) {
        return filePath.substr( pos + 1, filePath.length());
    }
    return "";
}


std::string fileName( const std::string &filePath ) {
    std::string r;
    const std::size_t pos = filePath.find_last_of( '/' );
    if ( pos != std::string::npos ) {
        return filePath.substr( pos + 1, filePath.length());
    }
    return "";
}


std::string pathWithoutRelative( const std::string &filePath ) {
    std::string::const_iterator it = filePath.cbegin();
    for ( ; it != filePath.cend(); ++it ) {
        if ((*it == '.') || (*it == '/') || (*it == '\\')) {
            continue;
        }
        return std::string( it, filePath.cend());
    }
    return "";
}


// @thanks http://stackoverflow.com/a/237280/963948
std::vector< std::string >
split( const std::string &s ) {

    std::vector< std::string > r;
    std::istringstream iss( s );
    std::copy(
            std::istream_iterator< std::string >( iss ),
            std::istream_iterator< std::string >(),
            std::back_inserter< std::vector< std::string > >( r )
    );

    return r;
}


} // wquest::get




namespace wquest::is {


bool
boolean( const std::string &s ) {
    if ( s.empty()) {
        return false;
    }
    std::string ls;
    std::transform( s.cbegin(), s.cend(), ls.begin(), ::tolower );
    return (ls == "true") || (ls == "+") || (ls == "yes") ||
           (ls == "false") || (ls == "-") || (ls == "no");
}




bool
capitalizeFirst( const std::string &s ) {

    // \todo Don't work for non-unicode languages. See TestCommon.

    // todo optimize Don't convert all input string.
    std::wstring_convert< std::codecvt_utf8_utf16< wchar_t > > converter;
    const std::wstring ws = converter.from_bytes( s );

    return capitalizeFirst( ws );
}




bool
capitalizeFirst( const std::wstring &ws ) {
    return !ws.empty() && std::iswupper( ws.front() );
}




bool
correct( uid_t uid ) {
    return (uid > 0) && is::defined( uid );
}




bool
digit10( char ch ) {
    return (isdigit((unsigned char) ch ) != 0);
}




bool
empty( char a ) {
    return a == 0;
}


bool
empty( float a, float epsilon ) {
    return std::abs( a ) < epsilon;
}


bool
empty( int a ) {
    return a == 0;
}


bool
empty( char *s ) {
    return strlen( s ) == 0;
}


bool
empty( const std::string &s ) {
    return s.empty();
}


bool
empty( const std::string_view s ) {
    return s.empty();
}




bool equals( float a, float b, float p ) {
    const float delta = a - b;
    return (delta < 0) ? (-delta < p) : (delta < p);
}


bool
equals( int a, int b ) {
    return (a == b);
}


bool
equals( float a, int b ) {
    return equals( a, (float) b );
}


bool
equals( int a, float b ) {
    return equals( b, a );
}


bool
name( const std::string &s ) {
    // with upper letter
    if ( s.empty() || !isupper((unsigned char) s.front())) {
        return false;
    }
    // without punctuations
    for ( const char ch : s ) {
        if ( ispunct((unsigned char) ch )) {
            return false;
        }
    }
    return true;
}


template<>
bool
distanceNear< float >( float a, float b, float allowableDistance ) {
    ASSERT( allowableDistance > 0 );
    const float delta = a - b;
    return (delta < 0) ? (-delta < allowableDistance) : (delta < allowableDistance);
}


template<>
bool
distanceFar< float >( float a, float b, float allowableDistance ) {
    ASSERT( allowableDistance > 0 );
    return !distanceNear< float >( a, b, allowableDistance );
}


// @thanks http://stackoverflow.com/a/8095127/963948
bool
startsWith( const std::string &haystack, const std::string &needle ) {
    return (needle.length() <= haystack.length())
           && equal( needle.cbegin(), needle.cend(), haystack.cbegin());
}


bool
undefined( char v ) {
    return v == get::undefined< char >();
}


bool
undefined( float v ) {
    return v == get::undefined< float >();
}


bool
undefined( int v ) {
    return v == get::undefined< int >();
}


bool
undefined( long v ) {
    return v == get::undefined< long >();
}


bool
undefined( unsigned long v ) {
    return v == get::undefined< unsigned long >();
}


bool
undefined( long long v ) {
    return v == get::undefined< long long >();
}


bool
undefined( unsigned long long v ) {
    return v == get::undefined< unsigned long long >();
}


bool
undefined( const std::string &v ) {
    return v == get::undefined< std::string >();
}


bool
undefined( const std::string_view v ) {
    return v == get::undefined< std::string >();
}


bool
undefined( uint32_t v ) {
    return v == get::undefined< uint32_t >();
}


} // wquest::is




namespace wquest::make {

} // wquest::make




namespace wquest::math {

int
random( int a, int b ) {

#ifdef WQUEST_RAND_SEED
    static std::mt19937 generator( WQUEST_RAND_SEED );
#else
    static std::random_device device;
    static std::mt19937 generator( device());
#endif
    std::uniform_int_distribution< int > distribution( a, b );

    return distribution( generator );
}


float
random( float a, float b ) {

#ifdef RAND_SEED
    static std::mt19937 generator( WQUEST_RAND_SEED );
#else
    static std::random_device device;
    static std::mt19937 generator( device());
#endif
    std::uniform_real_distribution< float > distribution( a, b + FLT_EPSILON );

    return distribution( generator );
}


float
random01() {
    return random( 0.0f, 1.0f );
}


coordI_t
coordITo2D( index_t i, int width ) {
    const int y = (int) (i / width);
    // \todo optimize? x = i % width;
    const int x = i - y * width;
    return coordI_t{ x, y };
}


index_t
coord2DToI( const coordI_t &c, int width ) {
    return coord2DToI( c.x, c.y, width );
}


index_t
coord2DToI( int x, int y, int width ) {
    return x + y * width;
}

} // wquest::math




namespace wquest::make {

std::string &
capitalizeFirst( std::string &s ) {

    // \todo Don't work for non-unicode languges. See TestCommon.

    // todo optimize Don't convert all input string.
    std::wstring_convert< std::codecvt_utf8_utf16< wchar_t > > converter;
    std::wstring ws = converter.from_bytes( s );
    capitalizeFirst( ws );
    s = converter.to_bytes( ws );

    return s;
}


std::wstring &
capitalizeFirst( std::wstring &ws ) {

    if ( ws.empty() ) {
        return ws;
    }

    const wchar_t wch = std::towupper( ws.front() );
    ws.front() = wch;

    return ws;
}


std::string
capitalizeFirstCopy( const std::string &s ) {
    std::string ps = s;
    return capitalizeFirst( ps );
}


std::wstring
capitalizeFirstCopy( const std::wstring &ws ) {
    std::wstring pws = ws;
    return capitalizeFirst( pws );
}




std::string &
trim( std::string &s ) {
    return ltrim( rtrim( s ));
}


std::string
trimCopy( const std::string &s ) {
    std::string ps = s;
    return ltrim( rtrim( ps ));
}


std::string &
ltrim( std::string &s ) {
    auto it2 = std::find_if( s.begin(), s.end(), []( char ch ) {
        return !std::isspace< char >( ch, std::locale::classic());
    } );
    s.erase( s.begin(), it2 );
    return s;
}


std::string &
rtrim( std::string &s ) {
    auto it1 = std::find_if( s.rbegin(), s.rend(), []( char ch ) {
        return !std::isspace< char >( ch, std::locale::classic());
    } );
    s.erase( it1.base(), s.end());
    return s;
}




std::string &
shorter( std::string &s, int length, const std::string &tail ) {

    ASSERT( length >= 0 );

    if ( s.length() <= length ) {
        return s;
    }

    const int l = std::min( length, (int)(s.length() - tail.length()) );
    s = s.substr( 0, l );
    if ( s.length() <= tail.length() ) {
        s = tail;
        return s;
    }

    s += tail;
    return s;
}




std::string
shorterCopy( const std::string &s, int length, const std::string &tail ) {
    std::string ps = s;
    return shorter( ps, length, tail );
}




std::string
emptyBraces( const std::string &s ) {

    std::string r = "";
    int countOpenBrace = 0;
    for ( const char ch : s ) {
        if (ch == ']') {
            r += '}';
            --countOpenBrace;
            continue;
        }
        if (ch == '[') {
            r += '{';
            ++countOpenBrace;
            continue;
        }
        if (countOpenBrace == 0) {
            r += ch;
        }
    } // for s

    return (countOpenBrace == 0) ? r : get::undefined< std::string >();
}


} // wquest::make




namespace wquest::parse {


bool
boolean( const std::string &s ) {
    ASSERT( !s.empty());
    std::string ls;
    std::transform( s.cbegin(), s.cend(), ls.begin(), ::tolower );
    return (ls == "true") || (ls == "+") || (ls == "yes");
}




int
numberInteger( const std::string &s ) {
    ASSERT( !s.empty());
    const int r = std::atoi( s.c_str());
    return r;
}




float
numberFloat( const std::string &s ) {
    ASSERT( !s.empty());
    const float r = std::atof( s.c_str());
    return r;
}




uid_t
uid( const std::string &s ) {
    ASSERT( !s.empty());
    const int r = std::atoi( s.c_str());
    return r;
}




std::pair< int, int >
pairInteger( const char *source, char delimiter ) {

    ASSERT( strlen( source ) > 0 );

    std::istringstream ss( source );
    std::string part;
    std::pair< int, int > pv( 0, 0 );
    size_t count = 0;
    while ( std::getline( ss, part, delimiter )) {
        if ((count % 2) == 0 ) {
            pv.first = numberInteger( part );
        } else {
            pv.second = numberInteger( part );
        }
        ++count;
    }
    ASSERT((count == 2) && "Waiting 2 values." );

    return pv;
}




std::pair< float, float >
pairFloat( const char *source, char delimiter ) {

    ASSERT( strlen( source ) > 0 );

    std::istringstream ss( source );
    std::string part;
    std::pair< float, float > pv( 0.0f, 0.0f );
    size_t count = 0;
    while ( std::getline( ss, part, delimiter )) {
        if ((count % 2) == 0 ) {
            pv.first = numberFloat( part );
        } else {
            pv.second = numberFloat( part );
        }
        ++count;
    }
    ASSERT((count == 2) && "Waiting 2 values." );

    return pv;
}


} // wquest::parse
