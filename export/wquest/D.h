#pragma once

#include "common.h"
#include "Cast.h"
#include "RepresentJSON.h"
#include "Type.h"

#include <functional>


namespace wquest {


class DM;


/**
 * \brief Any data for engine.
 */
class D : public RepresentJSON {
public:
    WQUEST_DECL_CAST_AND_PTR( D )

    typedef D::ptr_t parent_t;


private:
    /**
     * @brief Use `dm().emitD< D >()` instead of.
     */
    D() = delete;


public:
    D( uid_t, const parent_t );

    virtual ~D();

    uid_t uid() const {
        return uid_;
    }

    std::string suid() const {
        return std::to_string( uid() );
    }

    parent_t parent() const {
        return parent_.lock();
    }

    bool hasParent() const {
        return !parent_.expired();
    }

    /**
     * @warning
     * Don't declare like `virtual` because want create a `D`-object also.
     */
    virtual Type type() const {
        return etypeStatic( true );
    }

    /**
     * @brief Type like string.
     * @see std::to_string( wquest::Type )
     */
    virtual std::string stype() const {
        return stypeStatic();
    }

    static std::string stypeStatic() {
        return std::to_string( etypeStatic( true ) );
    }

    static Type etypeStatic( bool force = false ) {
        ASSERT( force &&
                "D::etypeStatic() called. Do you forget declare an own etypeStatic() or cast to child?" );
        return Type::D;
    }


    /**
     * @return `true` when `Type` is stored (included) like single char (property).
     * @details Returns `true` by default.
     */
    virtual bool singleInParent( Type ) const {
        return false;
    }

    /**
     * @return `true` when `Type` is stored (included) like multi chars (properties).
     */
    bool multiInParent( Type type ) const {
        return !singleInParent( type );
    }

    /**
     * @brief Path to this data.
     * @see single()
     */
    std::string path() const;


    bool dirt() const {
        return dirt_;
    }

    void setDirt() {
        dirt_ = true;
    }


protected:
    /**
     * @brief Initialize a string field and set a dirt flag
     * when a value is different.
     */
    void initField( std::string *field, const std::string &v );

    /**
     * @brief Helper for `fromJSON()`.
     */
    static void runWhenHas(
            const JSON &,
            const std::string &stype,
            std::function< void( const std::vector< uid_t > & ) > run );


private:
    static void buildPath( std::string *r, const D * );


private:
    uid_t uid_;
    std::weak_ptr< D > parent_;
    bool dirt_;

    friend class DM;
};


} // wquest




namespace std {

// \brief For use in standard sets.
template<>
struct hash< wquest::D > {
    std::size_t operator()( const wquest::D& d ) const {
        return d.uid();
    }
};

} // std
