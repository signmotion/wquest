#include "Annotation.h"
#include "common.h"
#include "dbgMacros.h"


namespace wquest {


Annotation::Annotation(
        uid_t uid,
        const std::string &text,
        const parent_t parent
) :
    D( uid, parent )
{
    //ASSERT( !text_.empty() );

    Annotation::text( text );
}




bool
Annotation::operator==( const Annotation &b ) const {
    return (uid() == b.uid()) &&
           (text_ == b.text_);
}




void
Annotation::text( const std::string &text ) {

    initField( &text_, text );

    // \todo Not correct work for non-unicode languages. See TestCommon.
    //make::capitalizeFirst( text_ );
}




json_t
Annotation::toJSON( rapidjson::Document *outer ) const {

    using namespace rapidjson;

    Document d( kObjectType );
    Document::AllocatorType &a =
            outer ? outer->GetAllocator() : d.GetAllocator();
    d.AddMember( "uid", uid(), a );
    d.AddMember( "type", stype(), a );
    d.AddMember( "text", text(), a );

    return d;
}




Annotation::ptr_t
Annotation::fromJSON( const json_t &json, parent_t ) {

    /* - TODO
    ASSERT( json.IsObject() );

    using namespace rapidjson;

    ASSERT( json.HasMember( "type" ) );
    const std::string type = json[ "type" ].GetString();
    if ( type != stype() ) {
        return nullptr;
    }

    ASSERT( json.HasMember( "uid" ) );
    const uid_t uid = json[ "uid" ].GetInt();
    if ( !is::correct( uid ) ) {
        return nullptr;
    }

    const std::string text =
            json.HasMember( "text" ) ? json[ "text" ].GetString() : "";
    const Annotation::ptr_t annotation = std::make_shared< Annotation >( uid, text );

    return annotation;
    */

    return nullptr;
}


} // wquest
