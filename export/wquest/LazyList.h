#pragma once

#include "D.h"
#include "DM.h"
#include "buildMacros.h"



namespace wquest {


/**
 * @brief Incapsulate a list of data. Work with `DM` for save and load its.
 */
template< class T >
class LazyList : std::enable_shared_from_this< T > {
public:
    WQUEST_DECL_CAST_AND_PTR( LazyList< T > )

    typedef std::shared_ptr< T > d_t;
    typedef std::pair< uid_t, d_t > dp_t;
    typedef std::vector< dp_t > dps_t;


public:
    LazyList() = delete;
    
    LazyList( const D::parent_t parent, DM::ownJSONParser_t ownJSONParser ) :
        parent_( parent ),
        ownJSONParser_( ownJSONParser ) {
    }


    Type etypeStatic() const {
        return T::etypeStatic();
    }

    std::string stype() const {
        return T::stype();
    }

    const dps_t &dps() const {
        return dps_;
    }

    int count() const {
        return dps_.size();
    }

    bool empty() const {
        return dps_.empty();
    }

    /**
     * @brief Add an UID list from array represents like JSON.
     * @return `true` when success.
     */
    [[nodiscard]] bool add( const rapidjson::Value &uids );

    void add( const std::vector< uid_t > & );

    /**
     * @brief Add a data.
     * @return `true` when success.
     * @details When UID is absent then add it.
     */
    [[nodiscard]] bool add( const d_t & );

    [[nodiscard]] bool add( index_t, const d_t & );

    [[nodiscard]] bool remove( index_t );

    void clear();

    /**
     * @brief Search a data by uid. Loads from store if set `load`.
     */
    d_t get( uid_t, bool load = true );

    /**
     * @return A data by index. Loads from store if set `load`.
     */
    d_t getI( index_t, bool load = true );


protected:
    void getFromStore( typename dps_t::iterator );


private:
    D::parent_t parent_;
    DM::ownJSONParser_t ownJSONParser_;
    dps_t dps_;
};




template< class T >
typename LazyList< T >::d_t
LazyList< T >::get( uid_t uid, bool load ) {

    ASSERT( is::correct( uid ) );

    for (auto it = dps_.begin(); it != dps_.end(); ++it) {
        if (uid == it->first) {
            if ( load && !it->second ) {
                getFromStore( it );
            }
            return it->second;
        }
    }

    ASSERT( false && "LazyList::d() Not found a data by UID." );
    return nullptr;
}




template< class T >
typename LazyList< T >::d_t
LazyList< T >::getI( index_t i, bool load ) {

    if ( dps_.empty() ) {
        return nullptr;
    }

    const auto it = std::next( dps_.begin(), i );
    if ( load && !it->second ) {
        getFromStore( it );
    }

    return it->second;
}




template< class T >
bool
LazyList< T >::add( const rapidjson::Value &uids ) {

    using namespace rapidjson;

    if ( !uids.IsArray() ) {
        return false;
    }

    for ( const Value &v : uids.GetArray() ) {
        ASSERT( v.IsInt() );
        const uid_t uid = v.GetInt();
        ASSERT( is::correct( uid ) );
        dps_.emplace_back( std::make_pair( uid, nullptr ) );
    }

    return true;
}




template< class T >
void
LazyList< T >::add( const std::vector< uid_t > &uids ) {

    for ( const uid_t &uid : uids ) {
        ASSERT( is::correct( uid ) );
        dps_.emplace_back( std::make_pair( uid, nullptr ) );
    }
}




template< class T >
bool
LazyList< T >::add( const d_t &d ) {

    if ( !d ) {
        return false;
    }

    const uid_t uid = d->uid();
    if ( !is::correct( uid ) ) {
        return false;
    }

    for (auto it = dps_.begin(); it != dps_.end(); ++it) {
        if (uid == it->first) {
            it->second.reset();
            it->second = d;
            return true;
        }
    }

    dps_.emplace_back( std::make_pair( uid, d ) );
    return true;
}




template< class T >
bool
LazyList< T >::add( index_t i, const d_t &d ) {

    if ( (i < 0) || (i >= dps_.size()) ) {
        return false;
    }

    if ( !d ) {
        return false;
    }

    const uid_t uid = d->uid();
    if ( !is::correct( uid ) ) {
        return false;
    }

    const auto it = std::next( dps_.cbegin(), i );
    dps_.emplace( it, std::make_pair( uid, d ) );
}




template< class T >
bool
LazyList< T >::remove( index_t i ) {

    if ( (i < 0) || (i >= dps_.size()) ) {
        return false;
    }

    const auto it = std::next( dps_.cbegin(), i );
    dps_.erase( it );
}




template< class T >
void
LazyList< T >::clear() {
    dps_.clear();
}




template< class T >
void
LazyList< T >::getFromStore( typename dps_t::iterator it ) {

    const uid_t uid = it->first;
    it->second.reset();
    it->second = dm().get< T >( parent_, uid, ownJSONParser_ );
    ASSERT( it->second );
    ASSERT( uid == it->second->uid() );
}


} // wquest
