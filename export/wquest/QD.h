#pragma once

#include "D.h"
#include "common.h"
#include "Title.h"
#include "Annotation.h"
#include "Tower.h"
#include "Scene.h"
#include "LazyList.h"


namespace wquest {


/**
 * @brief Quest declaration.
 * @warning Always has one tower minimum after created.
 */
class QD : public D {
public:
    WQUEST_DECL_CAST_AND_PTR( QD )


public:
    /**
     * @brief Use `dm().emitD< QD >()` instead of.
     */
    QD() = delete;

    /**
     * @warning Has 1 tower after created.
     */
    QD( uid_t, const parent_t );

    virtual Type type() const override {
        return etypeStatic();
    }

    virtual std::string stype() const override {
        return stypeStatic();
    }

    static std::string stypeStatic() {
        return std::to_string( etypeStatic() );
    }
    
    static Type etypeStatic() {
        return Type::QD;
    }

    bool operator==( const QD & ) const;


    /**
     * @brief Set and returns a title.
     */
    Title::ptr_t title( const std::string &text );

    Title::ptr_t title();

    /**
     * @brief Set and returns an annotation.
     */
    Annotation::ptr_t annotation( const std::string &text );

    Annotation::ptr_t annotation();

    virtual bool singleInParent( Type ) const override;


    WQUEST_BUILD_LAZY_LIST_METHODS( Tower, tower )


    /**
     * @details Remove a tower when it is empty.
     * @see addScene(), removeScene()
     */
    void moveScene(
            index_t iTowerFrom,
            index_t iSceneFrom,
            index_t iTowerTo,
            index_t iSceneTo );


    virtual json_t toJSON( rapidjson::Document *outer ) const override;

    /**
     * @warning Not uses `DM::emit()`.
     */
    static QD::ptr_t fromJSON( const JSON &, parent_t );


private:
    void initTowerList();

    void addTowerList( const std::vector< uid_t > & );


private:
    Title::ptr_t title_;
    Annotation::ptr_t annotation_;
    LazyList< Tower >::ptr_t towerList_;
};


} // wquest
