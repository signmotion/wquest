#pragma once

#include "Cast.h"
#include "dbgMacros.h"


namespace wquest {


/**
 * @brief Class for declare a correct name.
 */
class Name : public std::enable_shared_from_this< Name > {
public:
    WQUEST_DECL_CAST_AND_PTR( Name )


public:
    Name() = delete;

    /**
     * @warning Attempt will be made to convert to correct name.
     * \todo Not correct work for non-unicode languages. See TestCommon.
     */
    explicit Name( const std::string & );

    virtual ~Name() {
    }

    operator std::string() const;

    /**
     * @brief `true` then contains a permissible name.
     */
    bool valid() const;

    bool invalid() const {
        return !valid();
    }

    const std::string &name() const {
        return name_;
    }


private:
    std::string name_;
};




namespace is {


bool valid( const Name::ptr_t );

inline bool invalid( const Name::ptr_t n ) {
    return !valid( n );
}


}  // is


} // wquest




namespace std {

std::string to_string( const wquest::Name & );

} // std
