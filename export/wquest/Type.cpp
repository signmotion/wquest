#include "Type.h"
#include "common.h"


namespace std {


std::string
to_string( wquest::Type type ) {

    using namespace wquest;

    //ASSERT( is::correct( type ) ); - Type can be user default.

    if ( is::undefined( type ) || is::correct( type ) ) {
        // some base type
        return strEnumType[ (int)type ];
    }

    // some user type
    ASSERT( (type > Type::lowerBarUserType) &&
            "Should be define after `Type::lowerBarUserType`." );
    return UNDEFINED_TYPE_PREFIX + std::to_string( (int)type );
}




template<>
wquest::Type
from_string< wquest::Type >( const std::string &s ) {

    using namespace wquest;

    for (int i = 0; i < (int)Type::count; ++i) {
        const std::string &stype = strEnumType[ i ];
        if (s == stype) {
            return (Type)i;
        }
    }

    // detect some user type, see `to_string()`
    if (s.find( UNDEFINED_TYPE_PREFIX ) != 0) {
        return Type::Undefined;
    }

    const std::string strim = s.substr( UNDEFINED_TYPE_PREFIX.length() );
    const int type = parse::numberInteger( strim );
    if (type > (int)Type::lowerBarUserType) {
        // can be a correct user type
        return (Type)type;
    }

    // bad type, definetely
    return Type::Undefined;
}


} // std
