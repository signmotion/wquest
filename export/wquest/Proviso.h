#pragma once

#include "D.h"


namespace wquest {


/**
 * @brief Class for build an empty proviso.
 */
class Proviso : public D {
public:
    WQUEST_DECL_CAST_AND_PTR( Proviso )

    typedef std::string declare_t;


public:
    Proviso() = delete;

    /**
     * @see declare(), caption()
     */
    Proviso( uid_t, const parent_t );

    ~Proviso() override;

    virtual Type type() const override {
        return etypeStatic();
    }

    virtual std::string stype() const override {
        return stypeStatic();
    }

    static std::string stypeStatic() {
        return std::to_string( etypeStatic() );
    }

    static Type etypeStatic() {
        return Type::Proviso;
    }


    /**
     * @brief Morphers like JSON-object.
     */
    virtual json_t jsonMorphers( rapidjson::Document *outer ) const = 0;


    virtual bool define() const = 0;

    virtual int countDefine() const = 0;

    bool allDefine() const {
        return countDefine() == countMorpher();
    }

    bool someDefine() const {
        return (countDefine() > 0) && (countUndefine() > 0);
    }


    bool undefine() const {
        return !define();
    }

    virtual int countUndefine() const {
        return countMorpher() - countDefine();
    }

    bool allUndefine() const {
        return countUndefine() == countMorpher();
    }

    bool someUndefine() const {
        return (countUndefine() > 0) && (countDefine() > 0);
    }


    virtual bool valid() const = 0;

    virtual int countValid() const = 0;

    bool allValid() const {
        return countValid() == countMorpher();
    }

    bool someValid() const {
        return (countValid() > 0) && (countInvalid() > 0);
    }


    bool invalid() const {
        return !valid();
    }

    virtual int countInvalid() const {
        return countMorpher() - countValid();
    }

    bool allInvalid() const {
        return countInvalid() == countMorpher();
    }

    bool someInvalid() const {
        return (countInvalid() > 0) && (countValid() > 0);
    }


    /**
     * @warning Compare by `declare()` only.
     */
    bool operator==( const Proviso & ) const;

    /**
     * @brief Pattern for build a proviso.
     * @details
     * For example:
     * @code
     * When distance between {instanceA} and {instanceB} is {comparison} than {distance} m
     * @endcode
     * @see caption()
     * @see Morpher::stringOrBraces()
     */
    virtual declare_t declare() const = 0;

    /**
     * @brief Calculates a count of morphers by `declare()`.
     */
    int countMorpher() const;

    /**
     * @brief Return the `declare()` with initiated values.
     * @see declare()
     */
    virtual std::string caption() const = 0;

    /**
     * @warning Not uses `DM::emit()`.
     */
    virtual json_t toJSON( rapidjson::Document *outer ) const override;
};




namespace is {


bool valid( const Proviso::ptr_t );

inline bool invalid( const Proviso::ptr_t p ) {
    return !valid( p );
}


}  // is


} // wquest
