#pragma once

#include "common.h"
#include <array>


namespace wquest {


class CartesianCoord;
class CartesianCoordInt;


template< typename T, int D >
class CartesianCoordTD {
public:
    static constexpr int  D = D;

    typedef T  type_t;
    typedef std::array< T, D >  coord_t;

    // \warning 2D only.
    // \TODO For 3D also.
    // \see direction()
    enum class Direction {
        Undefined = 0,
        Absent,
        Top, Right, Down, Left,
        TopRight, DownRight, DownLeft, TopLeft,
        count
    };


public:
    CartesianCoordTD() { for ( int i = 0; i < D; ++i ) { mCoord[ i ] = get::undefined< T >(); } }
    explicit CartesianCoordTD( float x ) { mCoord[ 0 ] = ( T )x; for ( int i = 1; i < D; ++i ) { mCoord[ i ] = T(); } }
    CartesianCoordTD( float x, float y ) { mCoord[ 0 ] = ( T )x; mCoord[ 1 ] = ( T )y; for ( int i = 2; i < D; ++i ) { mCoord[ i ] = T(); } }
    explicit CartesianCoordTD( int x ) { mCoord[ 0 ] = ( T )x; for ( int i = 1; i < D; ++i ) { mCoord[ i ] = T(); } }
    CartesianCoordTD( int x, int y ) { mCoord[ 0 ] = ( T )x; mCoord[ 1 ] = ( T )y; for ( int i = 2; i < D; ++i ) { mCoord[ i ] = T(); } }
    explicit CartesianCoordTD( const coord_t& v ) { mCoord = v; }
    virtual ~CartesianCoordTD() {}

    const T& operator[]( index_t i ) const { return mCoord[ i ]; }
    T& operator[]( index_t i ) { return mCoord[ i ]; }

    const T& x() const { return mCoord[ 0 ]; }
    void x( const float& v ) { mCoord[ 0 ] = ( T )v; }
    void x( const int& v ) { mCoord[ 0 ] = ( T )v; }
    void decX( const int& v = 1 ) { mCoord[ 0 ] -= ( T )v; }
    void incX( const int& v = 1 ) { mCoord[ 0 ] += ( T )v; }

    const T& y() const { return mCoord[ 1 ]; }
    void y( const float& v ) { mCoord[ 1 ] = ( T )v; }
    void y( const int& v ) { mCoord[ 1 ] = ( T )v; }
    void decY( const int& v = 1 ) { mCoord[ 1 ] -= ( T )v; }
    void incY( const int& v = 1 ) { mCoord[ 1 ] += ( T )v; }

    const T* xy() const { return mCoord.data(); }
    void xy( float x, float y ) { mCoord[ 0 ] = ( T )x; mCoord[ 1 ] = ( T )y; }
    void xy( int x, int y ) { mCoord[ 0 ] = ( T )x; mCoord[ 1 ] = ( T )y; }
    void xy( const float* l ) { mCoord.data()[ 0 ] = ( T )l[ 0 ]; mCoord.data()[ 1 ] = ( T )l[ 1 ]; }
    void xy( const int* l ) { mCoord.data()[ 0 ] = ( T )l[ 0 ]; mCoord.data()[ 1 ] = ( T )l[ 1 ]; }

    operator CartesianCoord() const;
    operator CartesianCoordInt() const;

    // \see equalsXY()
    bool operator==(const CartesianCoordTD&) const;

    // \brief Equals by `x` and `y` only.
    bool equalsXY( const CartesianCoordTD& b ) const { return !undefined() && is::equals( x(), b.x() ) && is::equals( y(), b.y() ); }

    bool operator!=(const CartesianCoordTD&) const;
    bool operator<(const CartesianCoordTD&) const;

    CartesianCoordTD& operator-() { mCoord = -mCoord; return *this; }
    CartesianCoordTD& operator+=(const CartesianCoordTD& b) { for ( int i = 0; i < D; ++i ) { mCoord[ i ] += b.mCoord[ i ]; } return *this; }
    CartesianCoordTD& operator-=(const CartesianCoordTD& b) { for ( int i = 0; i < D; ++i ) { mCoord[ i ] -= b.mCoord[ i ]; } return *this; }
    CartesianCoordTD& operator*=(float v) { for ( int i = 0; i < D; ++i ) { mCoord[ i ] *= v; } return *this; }
    CartesianCoordTD& operator/=(float v) { for ( int i = 0; i < D; ++i ) { mCoord[ i ] /= v; } return *this; }
    CartesianCoordTD operator+(const CartesianCoordTD& b) const { coord_t r; for ( int i = 0; i < D; ++i ) { r[ i ] = mCoord[ i ] + b.mCoord[ i ]; } return CartesianCoordTD{ r }; }
    CartesianCoordTD operator-(const CartesianCoordTD& b) const { coord_t r; for ( int i = 0; i < D; ++i ) { r[ i ] = mCoord[ i ] - b.mCoord[ i ]; } return CartesianCoordTD{ r }; }
    CartesianCoordTD operator*(float v) const { coord_t r; for ( int i = 0; i < D; ++i ) { r[ i ] = (( T )(mCoord[ i ] * v)); } return CartesianCoordTD{ r }; }
    CartesianCoordTD operator/(float v) const { coord_t r; for ( int i = 0; i < D; ++i ) { r[ i ] = (( T )(mCoord[ i ] / v)); } return CartesianCoordTD{ r }; }

    Direction direction() const;

    CartesianCoordTD& abs() { for ( int i = 0; i < D; ++i ) { mCoord[ i ] = std::abs( mCoord[ i ] ); } return *this; }
    CartesianCoordTD absCopy() const { coord_t  r; for ( int i = 0; i < D; ++i ) { r[ i ] = std::abs( mCoord[ i ] ); } return r; }

    // \brief 2D
    CartesianCoordTD crossXY( const CartesianCoordTD& b ) const { return CartesianCoordTD{ mCoord[ 1 ], -mCoord[ 0 ] }; }
    float distance( const CartesianCoordTD& b ) const { return std::sqrt( distanceSq( b ) ); }
    T distanceSq( const CartesianCoordTD& b ) const { T r = T(); for ( int i = 0; i < D; ++i ) { const T d = b.mCoord[ i ] - mCoord[ i ]; r += d * d; } return r; }
    // \brief Returns a distance to segment `ab`.
    float distance( const CartesianCoordTD& a, const CartesianCoordTD& b, float* projection = nullptr ) const { return std::sqrt( distanceSq( a, b, projection ) ); }
    // \brief Returns a squared distance to segment `ab`.
    T distanceSq( const CartesianCoordTD& a, const CartesianCoordTD& b, float* projection = nullptr ) const;
    T distanceManhattan( const CartesianCoordTD& b ) const { T r = T(); for ( int i = 0; i < D; ++i ) { r += std::abs( b.mCoord[ i ] - mCoord[ i ] ); } return r; }
    float dot( const CartesianCoordTD& b ) const { return mCoord[ 0 ] * b.mCoord[ 0 ] + mCoord[ 1 ] * b.mCoord[ 1 ]; }
    float length() const { return std::sqrt( lengthSq() ); }
    T lengthSq() const { T r = T(); for ( int i = 0; i < D; ++i ) { r += mCoord[ i ] * mCoord[ i ]; } return r; }

    static const CartesianCoordTD& UNDEFINED();
    static const CartesianCoordTD& ZERO();

    void allAxisDefined() const { for ( int i = 0; i < D; ++i ) { ASSERT( is::defined( mCoord[ i ] ) ); } }


private:
    coord_t  mCoord;
};




template< typename T, int D >
bool
CartesianCoordTD< T, D >::operator==(const CartesianCoordTD& b) const {
    // direct verify, include undefined
    for ( int i = 0; i < D; ++i ) {
        if ( !is::equals( mCoord[ i ], b.mCoord[ i ] )  ) {
            return false;
        }
    }
    return true;
}




template< typename T, int D >
bool
CartesianCoordTD< T, D >::operator!=(const CartesianCoordTD& b) const {
    // direct verify, include undefined
    for ( int i = 0; i < D; ++i ) {
        if ( !is::equals( mCoord[ i ], b.mCoord[ i ] ) ) {
            return true;
        }
    }
    return false;
}




template< typename T, int D >
bool
CartesianCoordTD< T, D >::operator<(const CartesianCoordTD& b) const {

    for ( int i = 0; i < D; ++i ) {
        if ( mCoord[ i ] < b.mCoord[ i ] ) {
            return true;
        }
        else if ( mCoord[ i ] > b.mCoord[ i ] ) {
            return false;
        }
    }
    // are equals
    return false;
}




template< typename T, int D >
typename CartesianCoordTD< T, D >::Direction
CartesianCoordTD< T, D >::direction() const {

    ASSERT( (D == 2) && "Works only for 2D." );

    if ( (x() == 0) && (y() == 1) ) {
        return Direction::Top;
    }
    if ( (x() == 1) && (y() == 0) ) {
        return Direction::Right;
    }
    if ( (x() == 0) && (y() == -1) ) {
        return Direction::Down;
    }
    if ( (x() == -1) && (y() == 0) ) {
        return Direction::Left;
    }
    if ( (x() == 1) && (y() == 1) ) {
        return Direction::TopRight;
    }
    if ( (x() == 1) && (y() == -1) ) {
        return Direction::DownRight;
    }
    if ( (x() == -1) && (y() == -1) ) {
        return Direction::DownLeft;
    }
    if ( (x() == -1) && (y() == 1) ) {
        return Direction::TopLeft;
    }
    if ( (x() == 0) && (y() == 0) ) {
        return Direction::Absent;
    }

    return Direction::Undefined;
}




template< typename T, int D >
T
CartesianCoordTD< T, D >::distanceSq( const CartesianCoordTD& a, const CartesianCoordTD& b, float* projection ) const {

    const CartesianCoordTD  ap = *this - a;
    const CartesianCoordTD  ab = b - a;
    const float  l = ( float )ab.lengthSq();
    const float  pro = is::empty( l ) ? 0.5f : (ap.dot( ab ) / l);
    if ( projection ) {
        *projection = pro;
    }

    if ( pro <= 0.0f ) {
        return distanceSq( a );
    }
    if ( pro >= 1.0f ) {
        return distanceSq( b );
    }
    return distanceSq( a + ab * pro );
}




template< typename T, int D >
const CartesianCoordTD< T, D >&
CartesianCoordTD< T, D >::UNDEFINED() {
    static const CartesianCoordTD  r;
    return r;
}


template< typename T, int D >
const CartesianCoordTD< T, D >&
CartesianCoordTD< T, D >::ZERO() {
    static const CartesianCoordTD  r( 0 );
    return r;
}




class CartesianCoord :
    public CartesianCoordTD< float, 2 >
{
public:
    CartesianCoord() : CartesianCoordTD() {}
    explicit CartesianCoord( float x ) : CartesianCoordTD( x ) {}
    CartesianCoord( float x, float y ) : CartesianCoordTD( x, y ) {}
    explicit CartesianCoord( int x ) : CartesianCoordTD( x ) {}
    CartesianCoord( int x, int y ) : CartesianCoordTD( x, y ) {}
};




template< typename T, int D >
CartesianCoordTD< T, D >::operator CartesianCoord() const {
    CartesianCoord  r;
    for ( int i = 0; i < D; ++i ) {
        r[ i ] = is::undefined( mCoord[ i ] ) ? CartesianCoord::UNDEFINED()[ i ] : (CartesianCoord::type_t)mCoord[ i ];
    }
    return r;
}




class CartesianCoordInt :
    public CartesianCoordTD< int, 3 >
{
public:
    CartesianCoordInt() : CartesianCoordTD() {}
    explicit CartesianCoordInt( float x ) : CartesianCoordTD( x ) {}
    CartesianCoordInt( float x, float y ) : CartesianCoordTD( x, y ) {}
    explicit CartesianCoordInt( int x ) : CartesianCoordTD( x ) {}
    CartesianCoordInt( int x, int y ) : CartesianCoordTD( x, y ) {}
};




template< typename T, int D >
CartesianCoordTD< T, D >::operator CartesianCoordInt() const {
    CartesianCoordInt  r;
    for ( int i = 0; i < D; ++i ) {
        r[ i ] = is::undefined( mCoord[ i ] ) ? CartesianCoordInt::UNDEFINED()[ i ] : (CartesianCoordInt::type_t)mCoord[ i ];
    }
    return r;
}


} // wquest




namespace std {

/** \todo
// \brief For use in standard sets.
template<>
struct hash< wquest::CartesianCoord >
{
    std::size_t operator()( const gen::CartesianCoord& v ) const {
        v.allAxisDefined();
        std::size_t  r = (std::size_t)v[ 0 ];
        for ( int i = 1; i < gen::CartesianCoord::D; ++i ) {
            boost::hash_combine( r, v[ i ] );
        }
        return r;
    }
};


template<>
struct hash< wquest::CartesianCoordInt >
{
    std::size_t operator()( const gen::CartesianCoordInt& v ) const {
        v.allAxisDefined();
        std::size_t  r = (std::size_t)v[ 0 ];
        for ( int i = 1; i < gen::CartesianCoord::D; ++i ) {
            boost::hash_combine( r, v[ i ] );
        }
        return r;
    }
};
*/

} // std
