#include "DM.h"
#include "Store.h"
#include "D.h"
#include "QD.h"
#include "World.h"


namespace wquest {


using namespace rapidjson;


DM::lastUIDs_t DM::lastUIDs_;




DM::DM() :
    progressFlush_( 1, 1 )
{
    ASSERT( progressFlush_.first >= 0 );
    ASSERT( progressFlush_.second > 0 );
    ASSERT( progressFlush_.first <= progressFlush_.second );
}




DM::~DM() {

    //flush();
    //wait();
}




bool
DM::empty() const {
    return memory_.empty() && store().empty();
}




void
DM::clear() {

    clearFromMemory();
    clearFromStore();
    lastUIDs_.clear();
}




void
DM::clearFromMemory() {
    memory_.clear();
}




void
DM::clearFromStore() {
    store().clear();
}




int
DM::count() {

    flush();
    wait();
    return store().count();
}




int
DM::countInMemory() const {
    return memory_.size();
}




int
DM::countInMemory( Type type ) const {

    int n = 0;
    for ( const auto &p : memory_ ) {
        if ( p.second && (p.second->type() == type) ) {
            ++n;
        }
    }

    return n;
}




int
DM::countInStore() const {
    return store().count();
}




int
DM::countInStore( Type type, const std::shared_ptr< D > parent ) const {
    return store().count( type, parent );
}




std::shared_ptr< D >
DM::get(
        Type type,
        const std::shared_ptr< D > parent,
        uid_t uid,
        const ownJSONParser_t ownJSONParser
) {
    //ASSERT( is::correct( type ) ); - Type can be user default.
    ASSERT( is::correct( uid ) );

    const auto handle = [ this, type, parent, uid, ownJSONParser ] () {
        // look at memory, first
        const InnerUID iuid{ type, uid };
        //std::lock_guard lockDM( mutex );
        const auto it = memory_.find( iuid );
        if ( it != memory_.cend() ) {
            return it->second;
        }

        // load from store, otherwise
        return getFromStore( type, parent, uid, ownJSONParser );
    };

    return handle();
}




std::shared_ptr< D >
DM::getFromStore(
        Type type,
        const std::shared_ptr< D > parent,
        uid_t uid,
        const ownJSONParser_t ownJSONParser
) {
    //ASSERT( is::correct( type ) ); - Type can be user default.
    ASSERT( is::correct( uid ) );

    const auto handle = [ this, type, parent, uid, ownJSONParser ] () {
        // clear in memory
        const InnerUID iuid{ type, uid };
        //std::lock_guard lockDM( mutex );
        const auto it = memory_.find( iuid );
        if ( it != memory_.cend() ) {
            memory_.erase( it );
        }

        // load from store
        //std::lock_guard lockStore( store().mutex );
        const json_t json = store().get( type, parent, uid );
        ASSERT( json.IsObject() );
        return emitD( json, parent, ownJSONParser );
    };

    return handle();
}




void DM::emitD( const std::shared_ptr< D > d ) {

    ASSERT( d && "Data should be defined." );

    const InnerUID innerUID{ d->type(), d->uid() };
    memory_.emplace( innerUID, d );
}




std::shared_ptr< D >
DM::emitD(
        const json_t &json,
        const D::parent_t parent,
        const ownJSONParser_t ownJSONParser
) {
    //DBG_STRING( "DM::emitD()\n", std::to_string( json ) );

    // \todo optimize Remove in Release.
    {
        ASSERT( json.HasMember( "uid" ) );
        const uid_t uid = json[ "uid" ].GetInt();
        ASSERT( is::correct( uid ) );
    }

    ASSERT( json.HasMember( "type" ) );
    const std::string stype = json[ "type" ].GetString();
    ASSERT( !stype.empty() );
    const Type type = std::from_string< Type >( stype );
    //ASSERT( is::correct( type ) ); - Type can be user default.

    std::shared_ptr< D > d;
    const JSON jo( &json );
    if (type == Scene::etypeStatic()) {
        d = Scene::fromJSON( jo, parent );
    }
    else if (type == Tower::etypeStatic()) {
        d = Tower::fromJSON( jo, parent );
    }
    else if (type == QD::etypeStatic()) {
        d = QD::fromJSON( jo, parent );
    }
    else if (type == World::etypeStatic()) {
        d = World::fromJSON( jo, parent );
    }
    else if ( ownJSONParser ) {
        // attempt parse with own parser
        d = ownJSONParser( json, parent );
    }
    else {
        ASSERT( d && "DM::emitD() Unrecognized type." );
    }

    emitD( d );

    return d;
}




DM &DM::flush() {

    // \todo optimize fine? Use `this` instead `dm()`. See get().
    const auto task = [ this ] () {
        // emit the data to the store
        std::lock_guard lockDM( mutex );
        const int n = memory_.size();
        progressFlush_ = std::make_pair( 0, n + 1 );
        std::lock_guard lockStore( store().mutex );
        //DBG_INT( "DM::flush() Start for count data", n );
        for ( auto &p : memory_ ) {
            D::ptr_t &d = p.second;
            //DBG( "DM::flush() D %i : '%s'",
            //     p.first, strEnumType[ (int)d->type() ].c_str() );
            if ( d->dirt_ ) {
                store().emitD( d );
                d->dirt_ = false;
            }
            ++progressFlush_.first;
        }
        //DBG( "DM::flush() Stop" );
        // store can be memory cached and async also
        store().flush();
        store().wait();
        progressFlush_.first = (int)progressFlush_.second;
    };

    if ( thFlush_ ) {
        thFlush_->join();
    }
    thFlush_.reset( new std::thread( task ) );

    return *this;
}




bool DM::stillFlushing() const {

    ASSERT( progressFlush_.first <= progressFlush_.second );
    //DBG( "DM::stillFlushing() %i %i",
    //     (int)progressFlush_.first, (int)progressFlush_.second );
    return progressFlush_.first < progressFlush_.second;
}




DM &DM::wait() {

    if ( thFlush_ && thFlush_->joinable() ) {
        thFlush_->join();
    }
    thFlush_.reset();

    return *this;
}




uid_t DM::nextUID( Type type ) {

    //ASSERT( is::correct( type ) ); - Type can be user default.
    ASSERT( is::defined( type ) );

    auto it = lastUIDs_.find( type );
    if (it == lastUIDs_.end()) {
        it = lastUIDs_.emplace( type, 0 ).first;
    }
    ++it->second;
    const uid_t next = it->second;
    //DBG( "DM::nextUID() for %s is %i",
    //     strEnumType[ (int)type ].c_str(), next );

    return next;
}




void DM::nextUIDAfter( const D &d ) const {
    nextUIDAfter( d.type(), d.uid() );
}




void DM::nextUIDAfter( Type type, uid_t uid ) const {

    //ASSERT( is::correct( type ) ); - Type can be user default.
    ASSERT( is::defined( type ) );
    ASSERT( is::correct( uid ) );

    auto it = lastUIDs_.find( type );
    if (it == lastUIDs_.end()) {
        it = lastUIDs_.emplace( type, 0 ).first;
    }
    if ( it->second < uid ) {
        it->second = uid;
    }
}




json_t DM::dump() const {

    using namespace rapidjson;

    Document d( kObjectType );
    Document::AllocatorType &a = d.GetAllocator();
    d.AddMember( "type", "DM", a );

    d.AddMember( "count", countInMemory(), a );

    Document dCounts( kObjectType );
    for ( int i = (int)Type::Undefined + 1; i < (int)Type::count; ++i ) {
        Value stype( strEnumType[ i ], a );
        ASSERT( !strEnumType[ i ].empty() );
        dCounts.AddMember( stype, countInMemory( (Type)i ), a );
    }
    d.AddMember( "counts", dCounts, a );

    return d;
}




DM::InnerUID::InnerUID( Type type, uid_t uid ) :
    inner_( (int)type + uid * (int)Type::count )
{
    //ASSERT( is::correct( type ) ); - Type can be user default.
    ASSERT( is::correct( uid ) );
}




DM &dm() {
    static DM inst;
    return inst;
}


} // wquest




namespace std {

std::string to_string( const wquest::DM& dm ) {
    return wquest::toPrettyJSON( dm.dump() );
}

} // std
