#pragma once

#include "D.h"


namespace wquest {


/**
 * @see Scene
 */
class Impact : public D {
public:
    WQUEST_DECL_CAST_AND_PTR( Impact )

    Impact() = delete;

    Impact( uid_t, const std::string &declaration, const parent_t );

    virtual Type type() const override {
        return etypeStatic();
    }

    virtual std::string stype() const override {
        return stypeStatic();
    }

    static std::string stypeStatic() {
        return std::to_string( etypeStatic() );
    }

    static Type etypeStatic() {
        return Type::Impact;
    }

    bool operator==( const Impact & ) const;

    const std::string &declaration() const {
        return declaration_;
    }

    void declaration( const std::string &declaration );

    /**
     * @warning Not uses `DM::emit()`.
     */
    virtual json_t toJSON( rapidjson::Document *outer ) const override;


private:
    std::string declaration_;
};


} // wquest
