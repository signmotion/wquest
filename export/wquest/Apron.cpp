#include "Apron.h"
#include "common.h"
#include "dbgMacros.h"


namespace wquest {


Apron::Apron(
        uid_t uid,
        const std::string &text,
        const parent_t parent
) :
    D( uid, parent )
{
    //ASSERT( !text_.empty() );

    Apron::text( text );
}




bool
Apron::operator==( const Apron &b ) const {
    return (uid() == b.uid()) &&
           (text_ == b.text_);
}




void
Apron::text( const std::string &text ) {

    initField( &text_, text );

    // \todo Not correct work for non-unicode languages. See TestCommon.
    //make::capitalizeFirst( text_ );
}




json_t
Apron::toJSON( rapidjson::Document *outer ) const {

    using namespace rapidjson;

    Document d( kObjectType );
    Document::AllocatorType &a =
            outer ? outer->GetAllocator() : d.GetAllocator();
    d.AddMember( "uid", uid(), a );
    d.AddMember( "type", stype(), a );
    d.AddMember( "text", text(), a );

    return d;
}




Apron::ptr_t
Apron::fromJSON( const json_t &json, parent_t ) {

    /* - TODO
    ASSERT( json.IsObject() );

    using namespace rapidjson;

    ASSERT( json.HasMember( "type" ) );
    const std::string type = json[ "type" ].GetString();
    if ( type != stype() ) {
        return nullptr;
    }

    ASSERT( json.HasMember( "uid" ) );
    const uid_t uid = json[ "uid" ].GetInt();
    if ( !is::correct( uid ) ) {
        return nullptr;
    }

    const std::string text =
            json.HasMember( "text" ) ? json[ "text" ].GetString() : "";
    const Apron::ptr_t apron = std::make_shared< Apron >( uid, text );

    return apron;
    */

    return nullptr;
}


} // wquest
