#pragma once

#include <list>
#include <memory>


/**
 * \brief Cast will be simpler.
 * \details
 * `is()` returns `true` when cast by hierarchy was OK. \n
 * `cast()` do dynamic cast to type by hierarchy. \
 * \warning
 * Can't create the class Cast< T > for inheritance:
 * problem with pointers and method cast<>() by compile.
 * \see cast<>()
 */
#define WQUEST_DECL_PTR_T( T )  typedef std::shared_ptr< T > ptr_t; \
                                typedef std::weak_ptr< T > wptr_t; \
                                typedef std::list< ptr_t > ptrs_t; \
                                typedef std::list< wptr_t > wptrs_t;

/* - Not sense, not use because not polymorphic.
#define WQUEST_DECL_CAST_T( T ) bool is##T() const { \
                                    return dynamic_cast< const T * >( this ); \
                                } \
                                \
                                std::shared_ptr< T > cast##T() { \
                                    return std::dynamic_pointer_cast< T >( shared_from_this()); \
                                }
*/

#define WQUEST_DECL_CAST  template< class D > \
                          bool is() const { \
                              return dynamic_cast< const D * >( this ); \
                          } \
                          \
                          template< class D > \
                          std::shared_ptr< const D > cast() const { \
                              return std::dynamic_pointer_cast< const D >( shared_from_this()); \
                          }  \
                          template< class D > \
                          std::shared_ptr< D > cast() { \
                              return std::dynamic_pointer_cast< D >( shared_from_this()); \
                          }

#define WQUEST_DECL_CAST_AND_PTR( T )  WQUEST_DECL_PTR_T( T ) \
                                       WQUEST_DECL_CAST
