#pragma once

#include "common.h"
#include "Type.h"
#include "RepresentJSON.h"
#include <map>
#include <mutex>
#include <thread>
#include <future>


namespace wquest {


class D;


/**
 * @brief Data manager.
 */
class DM {
public:
    /**
     * @brief The UID for storage a data.
     * @details Build by `Type` and data UID.
     */
    struct InnerUID {
        InnerUID( Type, uid_t );

        operator uid_t() const {
            return inner_;
        }

        Type type() const {
            return static_cast< Type >(inner_ / (int)Type::count);
        }

        uid_t uid() const {
            return static_cast< uid_t >(inner_ % (int)Type::count);
        }

    private:
        uid_t inner_;
    };

    // \todo optimize Change to std::unordered*.
    typedef std::map< InnerUID, std::shared_ptr< D > > memory_t;

    typedef std::function< std::shared_ptr< D >(
            const json_t &,
            const std::shared_ptr< D > /* parent */ ) > ownJSONParser_t;

    mutable std::mutex mutex;


public:
    /**
     * @brief UID will generate automatically.
     */
    DM();

    ~DM();

    /**
     * \brief Visit by all data with type `T`.
     * \see eachWhile(), update()
     */
    /* - TODO
    template< class T >
    void each( std::function< void( const std::shared_ptr< T > ) > fn ) const {
        for ( const auto &d : data_ ) {
            const auto dd = std::dynamic_pointer_cast< T >( d );
            if ( dd ) {
                fn( dd );
            }
        } // for
    }
    */

    /**
     * \brief Visit by all data with type `T`.
     * \see eachWhile(), update()
     */
    template< class T >
    void each( std::function< void( std::shared_ptr< T > ) > fn );

    /**
     * \brief Visit by all data type `T`.
     * \details Break a loop when `fn` returns `false`.
     * \see each()
     */
    /* - TODO
    template< class T >
    void eachWhile( std::function< bool( const std::shared_ptr< T > ) > fn ) const {
        for ( const ptr_t d : store ) {
            const auto dd = std::dynamic_pointer_cast< T >( d );
            if ( dd ) {
                if ( !fn( dd )) {
                    break;
                }
            }
        } // for
    }
    */

    /**
     * \brief Visit by all data type `T`.
     * \details Break a loop when `fn` returns `false`.
     * \see each()
     */
    /* - TODO
    template< class T >
    void eachWhile( std::function< bool( std::shared_ptr< T > ) > fn ) {
        for ( ptr_t d : store ) {
            auto dd = std::dynamic_pointer_cast< T >( d );
            if ( dd ) {
                if ( !fn( dd )) {
                    break;
                }
            }
        } // for
    }
    */

    /* - TODO
    /// \see each(), eachWhile()
    template< class T >
    void update( std::function< void( std::shared_ptr< T > ) > fn ) {
        each< T >( fn );
    }

    /// \see each(), eachWhile()
    void update( std::function< void( ptr_t ) > fn ) {
        each( fn );
    }
    */

    bool empty() const;

    /**
     * @brief Remove all data from memory and store.
     * @warning UIDs counters also reset.
     */
    void clear();

    void clearFromMemory();

    void clearFromStore();


    /**
     * @brief Real count of data.
     * @warning Flush all data before return count.
     * @see countInMemory(), countInStore()
     * @see wquest::store()
     */
    int count();

    /**
     * @brief Count of data in memory.
     * @see count(), countInStore()
     */
    int countInMemory() const;

    /**
     * @brief Count of data in memory by type.
     * @see count(), countInStore()
     * @todo Do like countInStore( Type )? With parent?
     */
    int countInMemory( Type ) const;

    template< class T >
    int countInMemory() const {
        return countInMemory( T::etypeStatic() );
    }

    /**
     * @brief Count of data in store.
     * @see count(), countInMemory()
     * @see wquest::store()
     */
    int countInStore() const;

    /**
     * @brief Count of data in store by type.
     * @see count(), countInMemory()
     */
    int countInStore( Type, const std::shared_ptr< D > parent ) const;

    template< class T >
    int countInStore( const std::shared_ptr< D > parent ) const {
        return countInStore( T::etypeStatic(), parent );
    }


    /**
     * @brief Get data from memory or from store then data is absent in memory.
     * @details
     * Returns a control immediately to main thread.
     * @todo optimize Returns like `std::future<>`.
     */
    std::shared_ptr< D > get(
            Type,
            const std::shared_ptr< D > parent,
            uid_t,
            const ownJSONParser_t );

    std::shared_ptr< D > get(
            const std::shared_ptr< D > parent,
            const InnerUID &iuid,
            const ownJSONParser_t ownJSONParser ) {
        return get( iuid.type(), parent, iuid.uid(), ownJSONParser );
    }

    template< class T >
    std::shared_ptr< T > get(
            const std::shared_ptr< D > parent,
            uid_t uid,
            const ownJSONParser_t ownJSONParser ) {
        return get( T::etypeStatic(), parent, uid, ownJSONParser )->cast< T >();
    }


    /**
     * @brief Load from store, replace when has in the memory.
     */
    std::shared_ptr< D > getFromStore(
            Type,
            const std::shared_ptr< D > parent,
            uid_t,
            const ownJSONParser_t );

    std::shared_ptr< D > getFromStore(
            const InnerUID &iuid,
            const std::shared_ptr< D > parent,
            const ownJSONParser_t ownJSONParser ) {
        return getFromStore(
                    iuid.type(),
                    parent,
                    iuid.uid(),
                    ownJSONParser );
    }

    template< class T >
    std::shared_ptr< T > getFromStore(
            const std::shared_ptr< D > parent,
            uid_t uid,
            const ownJSONParser_t ownJSONParser) {
        return getFromStore(
                    T::etypeStatic(),
                    parent,
                    uid,
                    ownJSONParser )->cast< T >();
    }


    /**
     * \brief Factory for create and add to the manager the data typed `T`.
     * @details
     * Every class by `type()` has unique UID list.
     * Generates an UID automatically.
     * Always keep the max UID.
     * @warning For warranty store to `Store` call `flush()` and `wait()`.
     * @see nextUID()
     *//* - TODO?
    template< typename T, typename... Params >
    std::shared_ptr< T > emitD( Params &&... params ) {
        if constexpr ( std::is_constructible_v< T, Params... > ) {
            auto r = std::make_shared< T >( std::forward< Params >( params )... );
            emitD( r );
        }
        ASSERT( false && "Can't build a typed data." );
        return nullptr;
    }
    */


    /**
     * @brief Create and add `T` without params to the manager.
     * @details
     * Every class by `type()` has unique UID list.
     * Generates an UID automatically.
     * Always keep the max UID.
     * @warning For warranty store to `Store` call `flush()` and `wait()`.
     * @see nextUID()
     */
    template< class T >
    std::shared_ptr< T > emitD(
            const std::shared_ptr< D > parent );

    /**
     * @brief Create and add `T` with param `param1` to the manager.
     * @todo fine? Rewrite like v-template?
     */
    template< class T >
    std::shared_ptr< T > emitD(
            const std::string& param1,
            const std::shared_ptr< D > parent );

    /**
     * @brief Create from JSON and add `T` to the manager.
     * @warning UID should be undefined or same.
     */
    std::shared_ptr< D > emitD(
            const json_t &,
            const std::shared_ptr< D > parent,
            const ownJSONParser_t );

    /**
     * @brief Add `D` for the manager.
     * @see emitD< T >()
     */
    void emitD( const std::shared_ptr< D > );


    /**
     * @brief Save all unsaved data to the store.
     * @warning Use `wait()` for warranty save data.
     */
    DM &flush();
    
    /**
     * @brief `true` when `flush()` in process.
     */
    bool stillFlushing() const;

    const progress_t &progressFlush() const {
        return progressFlush_;
    }
    
    /**
     * @brief Wait a thread operations.
     * @see flush()
     */
    DM &wait();

    /**
     * @return Next UID for type.
     * @details Set next UID to `newUID` then `newUID` is defined.
     */
    uid_t nextUID( Type );

    /**
     * @brief Set next UID after UID of `D`.
     * @warning Set up UID when it more then was installed early.
     */
    void nextUIDAfter( const D & ) const;

    /**
     * @brief Set next UID for `Type` after `uid_t`.
     * @warning Set up UID when it more then was installed early.
     */
    void nextUIDAfter( Type, uid_t ) const;

    json_t dump() const;


private:
    memory_t memory_;

    std::unique_ptr< std::thread > thFlush_;
    progress_t progressFlush_;

    /// \todo optimize Change to `unordered_map`.
    typedef std::map< Type, uid_t > lastUIDs_t;
    static lastUIDs_t lastUIDs_;
};




template< class T >
std::shared_ptr< T >
DM::emitD(
        const std::shared_ptr< D > parent
) {
    const uid_t uid = dm().nextUID( T::etypeStatic() );
    const auto d = std::make_shared< T >( uid, parent );
    emitD( d );
    return d;
}




template< class T >
std::shared_ptr< T >
DM::emitD(
        const std::string &param1,
        const std::shared_ptr< D > parent
) {
    const uid_t uid = dm().nextUID( T::etypeStatic() );
    const auto d = std::make_shared< T >( uid, param1, parent );
    emitD( d );
    return d;
}




template< class T >
void DM::each( std::function< void( std::shared_ptr< T > ) > fn ) {
    for ( auto &d : memory_ ) {
        if ( !d.second ) {
            // \todo d.second = get< T >( d.first );
        }
        auto dd = std::dynamic_pointer_cast< T >( d.second );
        if ( dd ) {
            fn( dd );
        }
    } // for
}




DM &dm();


} // wquest




namespace std {

std::string to_string( const wquest::DM& );


/// \brief For use in standard sets.
template<>
struct hash< wquest::DM::InnerUID > {
    std::size_t operator()( const wquest::DM::InnerUID& innerUID ) const {
        return static_cast< std::size_t >( innerUID );
    }
};

} // std
