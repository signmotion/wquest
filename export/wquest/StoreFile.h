#pragma once

#include "Store.h"


namespace wquest {


class QD;
class World;


/**
 * @brief File system long term store.
 */
class StoreFile : public Store {
public:
    StoreFile( const std::string &basePath );

    virtual ~StoreFile() override;

    // \todo fine Emit like JSON.
    virtual void emitD( const std::shared_ptr< D > ) override;

    virtual bool empty() const override;

    virtual void clear() override;

    /**
     * @return Count data with type `World`.
     */
    virtual int count() const override;

    virtual std::string content( const std::string &path ) const override;

    virtual json_t get( Type, const std::shared_ptr< D > parent, uid_t ) override;

    virtual uids_t get( Type, const std::shared_ptr< D > parent ) const override;

    bool hasFolder( const std::string & ) const;

    bool hasFile( const std::string & ) const;

    const std::string &basePath() const {
        return basePath_;
    }


protected:
    std::string extension() const {
        return ".i";
    }

    void emitD( const std::shared_ptr< QD > );

    void emitD( const std::shared_ptr< World > );

    uids_t getUIDsFromFolder( const std::string &path ) const;

    uid_t getUIDFromFile( const std::string &file ) const;


private:
    void createBasePath() const;

    /**
     * @brief Returns a path according to base path.
     */
    std::string StoreFile::refreshPath( const std::string &basePath ) const;


private:
    std::string basePath_;
};




#ifndef WQUEST_OWN_STORE
inline Store &store() {
    static const std::string path = "data/my/";
    static StoreFile inst( path );
    return inst;
}
#endif


} // wquest
