#include "ProvisoWhenDistance2.h"
#include <fmt/core.h>


namespace wquest {


bool
ProvisoWhenDistance2::define() const {
    return countDefine() == countMorpher();
}




int
ProvisoWhenDistance2::countDefine() const {

    int n = 0;
    if ( morpherInstanceA() ) {
        ++n;
    }
    if ( morpherInstanceB() ) {
        ++n;
    }
    if ( morpherCompare() ) {
        ++n;
    }
    if ( morpherDistance() ) {
        ++n;
    }

    return n;
}




bool
ProvisoWhenDistance2::valid() const {
    return countValid() == countMorpher();
}




int
ProvisoWhenDistance2::countValid() const {

    int n = 0;
    if ( is::valid( morpherInstanceA() ) ) {
        ++n;
    }
    if ( is::valid( morpherInstanceB() ) ) {
        ++n;
    }
    if ( is::valid( morpherCompare() ) ) {
        ++n;
    }
    if ( is::valid( morpherDistance() ) ) {
        ++n;
    }

    return n;
}




Proviso::declare_t
ProvisoWhenDistance2::declare() const {

    // write on English always
    // other languages use in model
    return "When distance between [instance A] and [instance B]"
           " is [compare] than [distance] m";
}




std::string
ProvisoWhenDistance2::caption() const {

    // \todo Compound like ProvisoWhenDistance2Model::name().
    return declare();
}




json_t
ProvisoWhenDistance2::jsonMorphers( rapidjson::Document *outer ) const {

    using namespace rapidjson;

    Document d( kObjectType );
    Document::AllocatorType &a =
            outer ? outer->GetAllocator() : d.GetAllocator();
    if ( morpherInstanceA() ) {
        d.AddMember( "instance A", std::to_string( *morpherInstanceA() ), a );
    }
    if ( morpherInstanceB() ) {
        d.AddMember( "instance B", std::to_string( *morpherInstanceB() ), a );
    }
    if ( morpherCompare() ) {
        d.AddMember( "compare", std::to_string( *morpherCompare() ), a );
    }
    if ( morpherDistance() ) {
        d.AddMember( "distance", morpherDistance()->distance(), a );
    }

    return d;
}


} // wquest
