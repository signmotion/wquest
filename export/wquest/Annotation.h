#pragma once

#include "D.h"


namespace wquest {


/**
 * @see Scene
 */
class Annotation : public D {
public:
    WQUEST_DECL_CAST_AND_PTR( Annotation )

    Annotation() = delete;

    Annotation( uid_t, const std::string &text, const parent_t );

    virtual Type type() const override {
        return etypeStatic();
    }

    virtual std::string stype() const override {
        return stypeStatic();
    }

    static std::string stypeStatic() {
        return std::to_string( etypeStatic() );
    }

    static Type etypeStatic() {
        return Type::Annotation;
    }

    bool operator==( const Annotation & ) const;

    const std::string &text() const {
        return text_;
    }

    void text( const std::string & );

    virtual json_t toJSON( rapidjson::Document *outer ) const override;

    /**
     * @warning Not uses `DM::emit()`.
     */
    static Annotation::ptr_t fromJSON( const json_t &, parent_t );


private:
    std::string text_;
};


} // wquest
