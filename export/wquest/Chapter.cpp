#include "Chapter.h"
#include "common.h"
#include "dbgMacros.h"
#include <cctype>


namespace wquest {


Chapter::Chapter(
        uid_t uid,
        const std::string &name,
        const parent_t parent
) :
    D( uid, parent )
{
    //ASSERT( !name.empty() );

    Chapter::name( name );
}




bool
Chapter::operator==( const Chapter &b ) const {
    return (uid() == b.uid()) &&
           (name_ == b.name_);
}




void
Chapter::name( const std::string &name ) {

    initField( &name_, name );

    // \todo Not correct work for non-unicode languages. See TestCommon.
    //make::capitalizeFirst( name_ );
}




json_t
Chapter::toJSON( rapidjson::Document *outer ) const {

    using namespace rapidjson;

    Document d( kObjectType );
    Document::AllocatorType &a =
            outer ? outer->GetAllocator() : d.GetAllocator();
    d.AddMember( "uid", uid(), a );
    d.AddMember( "type", stype(), a );
    d.AddMember( "name", StringRef( name().c_str() ), a );

    return d;
}




Chapter::ptr_t
Chapter::fromJSON( const json_t &json, parent_t ) {

    /* - TODO
    ASSERT( json.IsObject() );

    using namespace rapidjson;

    ASSERT( json.HasMember( "type" ) );
    const std::string type = json[ "type" ].GetString();
    if ( type != stype() ) {
        return nullptr;
    }

    ASSERT( json.HasMember( "uid" ) );
    const uid_t uid = json[ "uid" ].GetInt();
    if ( !is::correct( uid ) ) {
        return nullptr;
    }

    const std::string name =
            json.HasMember( "name" ) ? json[ "name" ].GetString() : "";
    const Chapter::ptr_t chapter = std::make_shared< Chapter >( uid, name );

    return chapter;
    */

    return nullptr;
}


} // wquest
